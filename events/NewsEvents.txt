﻿###########################
# Darkest Hour News Events
###########################

add_namespace = DH_News
add_namespace = DH_News_Olympics
add_namespace = efm_news

#########################################################################
#  Berlin-Rome Axis
#########################################################################
news_event = {
	id = DH_News.1
	title = DH_News.1.t
	desc = DH_News.1.d
	picture = GFX_news_event_001	
	major = yes	
	is_triggered_only = yes
	fire_only_once = yes

# Worrying	
	option = {
		name = DH_News.1.A
		trigger = {
			NOT = {
				TAG = GER
				TAG = ITA
			}
		}
	}
# All other countries rotate on the Berlin-Rome Axis
	option = {
		name = DH_News.1.B
		trigger = { 
			OR = {
				TAG = GER
				TAG = ITA
			}
		}
	}
}
#########################################################################
#  Munich Conference
#########################################################################
news_event = {
	id = DH_News.2
	title = DH_News.2.t1
	desc = {
		trigger = {
			original_tag = GER	
		}
		text = DH_News.2.d1
	}
	desc = {
		trigger = {
			original_tag = ENG
		}
		text = DH_News.2.d2
	}
	desc = {
		trigger = {
			NOT = { OR = { tag = ENG tag = GER } }
		}
		text = DH_News.2.d3
	}	
	picture = GFX_news_event_001	
	major = yes	
	is_triggered_only = yes
	fire_only_once = yes

	# Another glorious victory for the Reich!	
	option = {
		name = DH_News.2.A
		trigger = {
			tag = GER
		}
		add_popularity = { ideology = fascist popularity = 0.05 }
	}
	# Peace for our time!
	option = {
		name = DH_News.2.B
		trigger = { 
			tag = ENG
		}
		add_stability = 0.10
		add_war_support = -0.10
	}
	# About us, without us!
	option = {
		name = DH_News.2.C
		trigger = { 
			tag = CZE
		}
		add_stability = -0.10
	}
	# Another batch of maps obsolete
	option = {
		name = DH_News.2.D
		trigger = { 
			NOT = { OR = { tag = ENG tag = GER tag = CZE } }
		}
	}
}

#########################################################################
#  Anti-Comitern Pact Formalized
#########################################################################
news_event = {
	id = DH_News.4
	title = DH_News.4.t
	desc = DH_News.4.d
	picture = GFX_news_event_017	
	major = yes	
	is_triggered_only = yes
	fire_only_once = yes

# Interesting	
	option = {
		name = DH_News.4.A
		trigger = {
			NOT = {
				TAG = SOV
			}
		}
	}
# How dare they!
	option = {
		name = DH_News.4.B
		trigger = { 
			TAG = SOV
		}
	}
}

#########################################################################
#  Pope Pius XI dies
#########################################################################
news_event = {
	id = DH_News.5
	title = DH_News.5.t
	desc = DH_News.5.d
	picture = GFX_news_event_017	
	major = yes	
	fire_only_once = yes
	trigger = {
		date > 1939.2.10
	}
# Pax Christi in Regno Christi.	
	option = {
		name = DH_News.5.A
	}
}
##########################################################################
# 4th august coup
##########################################################################

news_event = {
	id = DH_News.6
	title = DH_News.6.t
	desc = DH_News.6.d
	picture = GFX_4th_of_August_Coup	
	major = yes	
	fire_only_once = yes
	trigger = {
		date > 1936.8.4
	}
# Third hellenic civilizatoin begin
	option = {
		name = DH_News.6.A
	}
}
##########################################################################
# Soviet Occupation of the Baltics
##########################################################################

news_event = {
	id = DH_News.8
	title = DH_News.8.t
	desc = DH_News.8.d
	picture = GFX_news_event_064

	fire_only_once = yes
	trigger = {
		SOV = {
			AND = {
				controls_state = 11
				controls_state = 12
				controls_state = 13
			}
		}
	}
	option = {
		name = DH_News.8.A
	}
}