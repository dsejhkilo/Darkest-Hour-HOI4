﻿division_template = {
	name = "Bùbīng shī"			# Represents three-division infantry corps (generally poorly-equipped Mínbīngs)
	division_names_group = CHI_INF_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
	}
}
division_template = {
	name = "Mínbīng"				
	regiments = {					
		Militia = { x = 0 y = 0 }	
		Militia = { x = 0 y = 1 }
		Militia = { x = 1 y = 0 }
		Militia = { x = 1 y = 1 }
	}

	priority = 0 # garrison
}
division_template = {
	name = "Dì bāshíbā shī"

	regiments = {
			infantry = { x = 0 y = 0 }
			infantry = { x = 0 y = 1 }
			infantry = { x = 0 y = 2 }
			infantry = { x = 1 y = 0 }
			infantry = { x = 1 y = 1 }
			infantry = { x = 1 y = 2 }
			infantry = { x = 2 y = 0 }
			infantry = { x = 2 y = 1 }
			infantry = { x = 2 y = 2 }
	}
}

### OOB ###
units = {
	###### Kuomintang Army (CO: Chang Kai-shek) ######
	##### Beiping-Tianjin Area (CO: ) #####
	### 29 Juntuan (highly-reinforced corps) ###

	##### First War Area (CO: Cheng Qian) #####
	### 2nd Army Group (CO: Liu Shi) ###
	division = {
		name = "3 Bùbīng shī"
		location = 12408
		division_template = "Bùbīng shī"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5

	}

	division = {
		name = "1 Lujun Shi"
		location = 8072
		division_template = "Bùbīng shī"
		start_equipment_factor = 0.3

	}


	##### Second War Area (CO: Yan Xishan) #####

	### 7th Army Group (CO: Fu Zuoyi) ###
	division = {
		name = "4 Mínbīng"
		location = 9958
		division_template = "Mínbīng"		# Reorganized German-trained division (frontline infantry, better equipment and highest experience)
		start_experience_factor = 0.3
		start_equipment_factor = 0.5
	}
	division = {
		name = "89 Mínbīng"
		location = 9958
		division_template = "Mínbīng"		# Reorganized German-trained division (frontline infantry, better equipment and highest experience)
		start_experience_factor = 0.3

	}
	division = {
		name = "17 Bùbīng shī"
		location = 4114
		division_template = "Bùbīng shī"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5

	}
	
	division = {
		name ="Dì bāshíbā shī 1th Jūnduì"
		location = 7222
		division_template = "Dì bāshíbā shī"
		start_experience_factor = 1
		start_equipment_factor = 1

	}	
	
	division = {
		name ="Dì bāshíbā shī 2th Jūnduì"
		location = 7222
		division_template = "Dì bāshíbā shī"
		start_experience_factor = 1
		start_equipment_factor = 1

	}
	
	division = {
		name ="Dì bāshíbā shī 3th Jūnduì"
		location = 7222
		division_template = "Dì bāshíbā shī"
		start_experience_factor = 1
		start_equipment_factor = 1

	}
	
	division = {
		name ="Dì bāshíbā shī 4th Jūnduì"
		location = 7222
		division_template = "Dì bāshíbā shī"
		start_experience_factor = 1
		start_equipment_factor = 1

	}
	
	division = {
		name ="Dì bāshíbā shī 5th Jūnduì"
		location = 7222
		division_template = "Dì bāshíbā shī"
		start_experience_factor = 1
		start_equipment_factor = 1

	}

	division = {
		name ="Dì bāshíbā shī 6th Jūnduì"
		location = 7222
		division_template = "Dì bāshíbā shī"
		start_experience_factor = 1
		start_equipment_factor = 1

	}

	division = {
		name ="Dì bāshíbā shī 262nd Lǚ"
		location = 4325
		division_template = "Dì bāshíbā shī"
		start_experience_factor = 1
		start_equipment_factor = 1

	}
	
	division = {
		name ="Dì bāshíbā shī 263nd Lǚ"
		location = 1069
		division_template = "Dì bāshíbā shī"
		start_experience_factor = 1
		start_equipment_factor = 1

	}
	
	division = {
		name ="Dì bāshíbā shī 264th Lǚ"
		location = 10000
		division_template = "Dì bāshíbā shī"
		start_experience_factor = 1
		start_equipment_factor = 1

	}

	division = {
		name ="Dì bāshíbā shī 265th Lǚ"
		location = 1029
		division_template = "Dì bāshíbā shī"
		start_experience_factor = 1
		start_equipment_factor = 1

	}
	
	##### Third War Area (CO: Gu Zhutong) #####
	division = {
		name = "1 Bùbīng shī"
		location = 7027
		division_template = "Mínbīng"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5

	}
	division = {
		name = "1 Fujian Jingbei"
		location = 9974
		division_template = "Bùbīng shī"		# Garrison unit (poor equipment and training)
		start_equipment_factor = 0.3

	}
	division = {
		name = "2 Fujian Jingbei"
		location = 4169
		division_template = "Bùbīng shī"		# Garrison unit (poor equipment and training)
		start_equipment_factor = 0.3

	}
	division = {
		name = "3 Fujian Jingbei"
		location = 4196
		division_template = "Bùbīng shī"		# Garrison unit (poor equipment and training)
		start_equipment_factor = 0.3

	}
	## Shanghai Defense Force ##
	division = {
		name = "Shanghai Jingbei"
		location = 7014
		division_template = "Bùbīng shī"		# Garrison unit (poor equipment and training)
		start_equipment_factor = 0.3

	}
	division = {
		name = "Lu zhan Shi"
		location = 7014
		division_template = "Bùbīng shī"		# Garrison unit (poor equipment and training)
		start_equipment_factor = 0.3
		start_equipment_factor = 0.5
	}
	## 8th Army (CO: Zhang Fakui) ##
	division = {
		name = "3 Mínbīng"
		location = 10076
		division_template = "Mínbīng"		# German-trained division (frontline infantry, better equipment and experience)
		start_experience_factor = 0.3
		start_equipment_factor = 0.5
	}
	division = {
		name = "Shui Jing Tuan"
		location = 10076
		division_template = "Mínbīng"		# German-trained division (frontline infantry, better equipment and experience)
		start_experience_factor = 0.3
		start_equipment_factor = 0.5
	}
	## 9th Army (CO: Zhang Zhizhong) ##
	division = {
		name = "36 Mínbīng"
		location = 7191
		division_template = "Mínbīng"		# German-trained division (frontline infantry, better equipment and experience)
		start_experience_factor = 0.3
		start_equipment_factor = 0.5
	}
	division = {
		name = "87 Mínbīng"
		location = 7191
		division_template = "Mínbīng"		# German-trained division (frontline infantry, better equipment and experience)
		start_experience_factor = 0.3
		start_equipment_factor = 0.5
	}
	division = {
		name = "88 Mínbīng"
		location = 7191
		division_template = "Mínbīng"		# German-trained division (frontline infantry, better equipment and experience)
		start_experience_factor = 0.3
		start_equipment_factor = 0.5
	}
	## 10th Army (CO: Liu Jiangxu) ##
	division = {
		name = "10 Lujun Shi"
		location = 10076
		division_template = "Bùbīng shī"
		start_equipment_factor = 0.3

	}
	### 15th Army Group (CO: Chen Cheng) ###
	division = {
		name = "11 Mínbīng"
		location = 12076
		division_template = "Mínbīng"		# Reorganized German-trained division (frontline infantry, better equipment and highest experience)
		start_experience_factor = 0.3
		start_equipment_factor = 0.5
	}
	division = {
		name = "14 Mínbīng"
		location = 12076
		division_template = "Mínbīng"		# German-trained division (frontline infantry, better equipment and experience)
		start_experience_factor = 0.3
		start_equipment_factor = 0.5
	}
	division = {
		name = "4 Bùbīng shī"
		location = 12076
		division_template = "Bùbīng shī"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5

	}
	division = {
		name = "18 Bùbīng shī"
		location = 12076
		division_template = "Bùbīng shī"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5

	}
	division = {
		name = "39 Bùbīng shī"
		location = 4042
		division_template = "Bùbīng shī"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5

	}
	division = {
		name = "54 Bùbīng shī"
		location = 4042
		division_template = "Bùbīng shī"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5

	}
	division = {
		name = "74 Bùbīng shī"
		location = 4042
		division_template = "Bùbīng shī"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5

	}
	### 19th Army Group (CO: Xu Yue) ###
	division = {
		name = "6 Mínbīng"
		location = 7058
		division_template = "Mínbīng"		# German-trained division (frontline infantry, better equipment and experience)
		start_experience_factor = 0.3
		start_equipment_factor = 0.5
	}
	division = {
		name = "9 Mínbīng"
		location = 7058
		division_template = "Mínbīng"		# German-trained division (frontline infantry, better equipment and experience)
		start_experience_factor = 0.3

	}
	division = {
		name = "25 Bùbīng shī"
		location = 1096
		division_template = "Bùbīng shī"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5

	}

	##### Yangtze War Area (CO: ) #####
	division = {
		name = "Wuhan Jingbei"
		location = 4619
		division_template = "Bùbīng shī"		# Garrison unit (poor equipment and training)
		start_equipment_factor = 0.3

	}
	division = {
		name = "Shashi Jingbei"
		location = 4130
		division_template = "Bùbīng shī"		# Garrison unit (poor equipment and training)
		start_equipment_factor = 0.3

	}
	division = {
		name = "Di 1 Nanchang Jingbei"
		location = 3992
		division_template = "Bùbīng shī"		# Garrison unit (poor equipment and training)
		start_equipment_factor = 0.3

	}
	division = {
		name = "Di 2 Nanchang Jingbei"
		location = 3992
		division_template = "Bùbīng shī"		# Garrison unit (poor equipment and training)
		start_equipment_factor = 0.3

	}
	division = {
		name = "Di 3 Nanchang Jingbei"
		location = 3992
		division_template = "Bùbīng shī"		# Garrison unit (poor equipment and training)
		start_equipment_factor = 0.3

	}
	division = {
		name = "Huaining Jingbei"
		location = 1083
		division_template = "Bùbīng shī"		# Garrison unit (poor equipment and training)
		start_equipment_factor = 0.3

	}
	### River Defense Force (CO: ) ###
	division = {
		name = "11 Lujun Shi"
		location = 1036
		division_template = "Mínbīng"
		start_equipment_factor = 0.3

	}
	division = {
		name = "12 Lujun Shi"
		location = 7637
		division_template = "Mínbīng"
		start_equipment_factor = 0.3

	}
	division = {
		name = "13 Lujun Shi"
		location = 10377
		division_template = "Mínbīng"
		start_equipment_factor = 0.5

	}
	division = {
		name = "14 Lujun Shi"
		location = 7159
		division_template = "Mínbīng"
		start_equipment_factor = 0.5

	}
	division = {
		name = "15 Lujun Shi"
		location = 7502
		division_template = "Mínbīng"
		start_equipment_factor = 0.3

	}
	division = {
		name = "43 Bùbīng shī"
		location = 9959
		division_template = "Bùbīng shī"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5

	}
	### Capital Garrison Force (CO: ) ###
	division = {
		name = "2 Lujun Shi"
		location = 11913
		division_template = "Bùbīng shī"
		start_equipment_factor = 0.3

	}
	division = {
		name = "Peixun Shi"
		location = 11913
		division_template = "Mínbīng"		# Reorganized German-trained division (frontline infantry, better equipment and highest experience)
		start_experience_factor = 0.3
		start_equipment_factor = 0.5
	}
	division = {
		name = "1 Nanking Jingbei"
		location = 11913
		division_template = "Bùbīng shī"		# Garrison unit (poor equipment and training)
		start_equipment_factor = 0.5
	}
	division = {
		name = "2 Nanking Jingbei"
		location = 11913
		division_template = "Bùbīng shī"		# Garrison unit (poor equipment and training)
	}
	division = {
		name = "3 Nanking Jingbei"
		location = 11913
		division_template = "Bùbīng shī"		# Garrison unit (poor equipment and training)
		start_equipment_factor = 0.5
	}
	division = {
		name = "4 Nanking Jingbei"
		location = 11913
		division_template = "Bùbīng shī"		# Garrison unit (poor equipment and training)
		start_equipment_factor = 0.5
	}



	## qianjun ##
	division = {
		name = "12 Lujun Shi"
		location = 4504
		division_template = "Mínbīng"
		start_equipment_factor = 0.3

	}
	division = {
		name = "13 Lujun Shi"
		location = 7101
		division_template = "Mínbīng"
		start_equipment_factor = 0.3

	}
	division = {
		name = "41 Bùbīng shī"
		location = 7097
		division_template = "Bùbīng shī"
		start_equipment_factor = 0.3

	}
	division = {
		name = "14 Lujun Shi"
		location = 10763
		division_template = "Mínbīng"
		start_equipment_factor = 0.3

	}
	division = {
		name = "15 Lujun Shi"
		location = 10616
		division_template = "Bùbīng shī"
		start_equipment_factor = 0.3

	}
	##### ROCN #####
	navy = {
		name = "Zhōnghuá Mínguó Hǎijūn"
		base = 7014
		location = 7014 #Shanghai (Should be Qingdao)
		# 1st Fleet
		ship = { name = "Ninghai" definition = light_cruiser equipment = { CL_equipment_1922 = { amount = 1 owner = CHI } } }
		ship = { name = "Pinghai" definition = light_cruiser equipment = { CL_equipment_1922 = { amount = 1 owner = CHI } } }
		
		ship = { name = "Datong, Ziqiang" definition = destroyer equipment = { DD_equipment_1885 = {amount = 1 owner = CHI } } }
		
		ship = { name = "Yixian" definition = light_cruiser equipment = { CL_equipment_1912 = { amount = 1 owner = CHI } } }
		ship = { name = "Haichou" definition = light_cruiser equipment = { CL_equipment_1912 = { amount = 1 owner = CHI } } }

		ship = { name = "Yongjian, Yongji, Zhongshan, Jiankang" definition = destroyer equipment = { DD_equipment_1885 = {amount = 1 owner = CHI } } }
	
		ship = { name = "Dingan, Kean" definition = destroyer equipment = { DD_equipment_1885 = {amount = 1 owner = CHI } } }
	
		# 2nd Fleet
		ship = { name = "Minquan, Minsheng, Xianning, Chutong, Chutai" definition = destroyer equipment = { DD_equipment_1885 = {amount = 1 owner = CHI } } }
		
		ship = { name = "Chuyou, Chuguan, Chuqian, Jiangyuan, Jiangzhen" definition = destroyer equipment = { DD_equipment_1885 = {amount = 1 owner = CHI } } }
	
		ship = { name = "Jiangkun, Jiangxi" definition = destroyer equipment = { DD_equipment_1885 = {amount = 1 owner = CHI } } }
	
		ship = { name = "Hupeng, Hue, Huying, Hucui" definition = destroyer equipment = { DD_equipment_1885 = {amount = 1 owner = CHI } } }
	
		# Training Fleet
		ship = { name = "Yingrui" definition = light_cruiser equipment = { CL_equipment_1912 = { amount = 1 owner = CHI } } }
		ship = { name = "Tongji" definition = light_cruiser equipment = { CL_equipment_1912 = { amount = 1 owner = CHI } } }
		
		# 3rd Fleet
		ship = { name = "Yingrui" definition = heavy_cruiser equipment = { CA_equipment_1895 = { amount = 1 owner = CHI } } }

		ship = { name = "Tongan, Yongxiang, Chuyu, Jiangli" definition = destroyer equipment = { DD_equipment_1885 = {amount = 1 owner = CHI } } }

		ship = { name = "Dinghai" definition = destroyer equipment = { DD_equipment_1885 = { amount = 1 owner = CHI } } }
		
		# RoC Naval Academy Fleet
		ship = { name = "Wen-42, Wen-88, Wen-93, Shi-34, Shi-102" definition = destroyer equipment = { DD_equipment_1885 = {amount = 1 owner = CHI } } }
		
		ship = { name = "Shi-181, Yue-22, Yue-253, Yue-371" definition = destroyer equipment = { DD_equipment_1885 = {amount = 1 owner = CHI } } }
		
		ship = { name = "Haijing, Junhe, Cedian, Boxian" definition = destroyer equipment = { DD_equipment_1885 = {amount = 1 owner = CHI } } }

	}
}


### Air Forces ###
air_wings = {
# Roughly 300 fighters, 21 tac bombers, but only 200 aircraft total were serviceable in 1937 due to neglect
	# Beiping
	608 = {
		Fighter_equipment_1933 = { owner = "CHI" amount = 30 }				# 3rd PG - Fiat CR.32
	}
	#Shanghai
	613 = {
		Fighter_equipment_1933 = { owner = "CHI" amount = 70 } 		# 4th PF, 5th PG, 9th PS - Curtiss Hawk
		Tactical_Bomber_equipment_1933 =  { owner = "CHI" amount = 15 }		# Combined BG -- Martin 139WC, Savoia S.72
	}
}



instant_effect = {
	add_equipment_production = {
		equipment = {
			type = Small_Arms_equipment_1900
			creator = "CHI"
		}
		requested_factories = 5
		progress = 0.33
		efficiency = 60
	}
	add_equipment_production = {
		equipment = {
			type = Fighter_equipment_1933
			creator = "CHI"
		}
		requested_factories = 1
		progress = 0.55
		efficiency = 60
	}

#Under Construction in Shanghai
	# Light Cruiser
	add_equipment_production = { 		# "Pinghai"
		equipment = {
			type = light_cruiser_1
			creator = "CHI"
		}
		requested_factories = 1
		progress = 0.33
		amount = 1
	}
}
