﻿units = {
	division= {
		name = "SS-Division (mot.) „Wiking“"
		location = 707 # Heuberg
		division_template = "SS-Infanterie-Division (mot.)"
		start_experience_factor = 0.14
		start_equipment_factor = 1.0 
	}
}