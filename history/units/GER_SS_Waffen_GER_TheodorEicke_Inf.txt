﻿units = {
	division= {
		name = "SS-Division „Theodor Eicke“"
		location = 11620 # Bad Tölz
		division_template = "SS-Infanterie-Division"
		start_experience_factor = 0.14
		start_equipment_factor = 1.0 
	}
}