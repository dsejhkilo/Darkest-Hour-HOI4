﻿division_template = {
	name = "Constabulary District"			# The only local armed force, the Constabulary became the basis of the Army in 1936

	regiments = {
		militia = { x = 0 y = 0 }  # authorized strength in 1925 was just shy of 7k, so two battalions per District is a good approximation.
		militia = { x = 0 y = 1 }
	}
}

units = {
	######## LAND OOB ########
	##### Manila Garrison #####
	division = {
		name = "Constabulary District of Northern Luzon"
		location = 4368 # Vigan?
		division_template = "Constabulary District"		
		start_experience_factor = 0.26 # The Constabulary was considered an "excellent and most efficient organization" with "high standards" and had experience from various insurrection movements.
		start_equipment_factor = 0.78 # Based on the actual 1925 strength (which remained 'substantially the same' for the next decade) of 394 officers and 5829 enlisted.
	}

	division = {
		name = "Constabulary District of Southern Luzon"
		location = 12236 # Lucena?
		division_template = "Constabulary District"		
		start_experience_factor = 0.26
		start_equipment_factor = 0.78
	}
	
	division = {
		name = "Constabulary District of the Visayas"
		location = 12118 # Cebu City?
		division_template = "Constabulary District"		
		start_experience_factor = 0.26
		start_equipment_factor = 0.78
	}
	
	division = {
		name = "Constabulary District of Mindanao and Sulu"
		location = 1421 # Zamboanga?
		division_template = "Constabulary District"		
		start_experience_factor = 0.26
		start_equipment_factor = 0.78
	}
	
	######## No Naval OOB ########
}

### Starting Production ###
instant_effect = {
	add_equipment_production = {
		equipment = {
			type = Small_Arms_equipment_1936
			creator = "PHI"
		}
		requested_factories = 1
		progress = 0.40
		efficiency = 100
	}
}