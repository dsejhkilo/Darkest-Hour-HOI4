﻿division_template = {
    name = "Infanterie Divisie" 
	division_names_group = INS_INF_01

    regiments = {
        infantry = { x = 0 y = 0 }
        infantry = { x = 0 y = 1 }
        infantry = { x = 0 y = 2 }
        infantry = { x = 1 y = 0 }
        infantry = { x = 1 y = 1 }
        infantry = { x = 1 y = 2 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }
		artillery_battalion = { x = 3 y = 0 }
		artillery_battalion = { x = 3 y = 1 }
		artillery_battalion_light = { x = 3 y = 2 }
    }
	support = {
		engineer = { x = 0 y = 0 }  
		field_hospital = { x = 0 y = 1 }
		signal_company = { x = 0 y = 2 }
		recon = { x = 0 y = 3 }
	}
}

division_template = {
    name = "Territoriaal Commando" 
	division_names_group = INS_GAR_01

    regiments = {
        garrison = { x = 0 y = 0 }
        garrison = { x = 0 y = 1 }
        garrison = { x = 1 y = 0 }
		artillery_battalion_light = { x = 3 y = 2 }
    }
 	support = {
		anti_air = { x = 0 y = 0 }  
		field_hospital = { x = 0 y = 1 }
	}
    priority = 0
}

units = {
    ##### Koninklijk Nederlands Indisch Leger #####
    division = {
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}
        location = 7381 # Batavia
        division_template = "Infanterie Divisie"
        start_experience_factor = 0.1
        start_equipment_factor = 0.9 
    }
    division = {
		division_name = {
			is_name_ordered = yes
			name_order = 2
		}
        location = 12277 # Magelang
        division_template = "Infanterie Divisie"
        start_experience_factor = 0.1
        start_equipment_factor = 0.6
    }
    division = {
		division_name = {
			is_name_ordered = yes
			name_order = 3
		}
        location = 10491 # Malang
        division_template = "Infanterie Divisie"
        start_experience_factor = 0.3
        start_equipment_factor = 0.4
    }
    ##### Territoriaal Commando #####
    division = {
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}
        location = 7658 # Koetaradja
        division_template = "Territoriaal Commando"
        start_experience_factor = 0.1
        start_equipment_factor = 0.8
    }
    division = {
		division_name = {
			is_name_ordered = yes
			name_order = 2
		}
        location = 7368 # Padang
        division_template = "Territoriaal Commando"
        start_experience_factor = 0.1
        start_equipment_factor = 0.5
    }
    division = {
		division_name = {
			is_name_ordered = yes
			name_order = 3
		}
        location = 12113 # Tandjoengpinang
        division_template = "Territoriaal Commando"
        start_experience_factor = 0.1
        start_equipment_factor = 0.2
    }
    division = {
		division_name = {
			is_name_ordered = yes
			name_order = 4
		}
        location = 12268 # Palembang
        division_template = "Territoriaal Commando"
        start_experience_factor = 0.1
        start_equipment_factor = 0.4
    }
    division = {
		division_name = {
			is_name_ordered = yes
			name_order = 5
		}
        location = 4279 # Poentianak
        division_template = "Territoriaal Commando"
        start_experience_factor = 0.1
        start_equipment_factor = 0.2
    }
    division = {
		division_name = {
			is_name_ordered = yes
			name_order = 6
		}
        location = 7296 # Bandjarmasin
        division_template = "Territoriaal Commando"
        start_experience_factor = 0.1
        start_equipment_factor = 0.2
    }
    division = {
		division_name = {
			is_name_ordered = yes
			name_order = 7
		}
        location = 10153 # Manado
        division_template = "Territoriaal Commando"
        start_experience_factor = 0.1
        start_equipment_factor = 0.7
    }
    division = {
		division_name = {
			is_name_ordered = yes
			name_order = 8
		}
        location = 1355 # Koepang
        division_template = "Territoriaal Commando"
        start_experience_factor = 0.1
        start_equipment_factor = 0.2
    }
    division = {
		division_name = {
			is_name_ordered = yes
			name_order = 9
		}
        location = 1574 # Amboina
        division_template = "Territoriaal Commando"
        start_experience_factor = 0.1
        start_equipment_factor = 0.25
    }
}




#########################
## STARTING PRODUCTION ##
#########################

instant_effect = {

	add_equipment_production = {
		equipment = {
			type = Small_Arms_equipment_1900
			creator = "INS"
		}
		requested_factories = 2
		progress = 0.1
		efficiency = 50
	}

}

#################################