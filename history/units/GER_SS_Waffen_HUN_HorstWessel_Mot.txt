﻿units = {
	division= {
		name = "SS-Freiwilligen-Grenadier-Division „Horst Wessel“"
		location = 11610 # Actually formed in Zagreb, Croatia
		division_template = "SS-Infanterie-Division (mot.)"
		start_experience_factor = 0.14
		start_equipment_factor = 1.0 
	}
}