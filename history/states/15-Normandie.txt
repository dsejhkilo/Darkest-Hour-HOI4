
state={
	id=15
	name="STATE_15"
	resources={
		steel=21.000
	}

	history={
		owner = FRA
		victory_points = {
			11535 5
		}
		victory_points = {
			6449 3
		}
		buildings = {
			infrastructure = 7
			industrial_complex = 2
			air_base = 6
			6449 = {
				naval_base = 5
				coastal_bunker = 1

			}

		}
		add_core_of = FRA
		1941.6.22 = {
			owner = FRA
			controller = GER
			add_core_of = VIC

		}
		1944.6.20 = {
			FRA = {
				set_province_controller = 3579

			}

		}
		1945.1.1 = {
			owner = FRA
			controller = FRA

		}
		1946.1.1 = {
			remove_core_of = VIC

		}

	}

	provinces={
		539 553 3534 3549 3579 6449 6599 9550 9578 11521 11535 
	}
	manpower=1418559
	buildings_max_level_factor=1.000
	state_category=city
}
