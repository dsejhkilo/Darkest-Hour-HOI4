
state={
	id=54
	name="STATE_54"

	history={
		owner = GER
		buildings = {
			infrastructure = 7
			industrial_complex = 2

		}
		victory_points = {
			11544 10 
		}
		victory_points = {
			9557 2 
		}
		add_core_of = GER
		1939.1.1 = {
			buildings = {
				air_base = 3

			}

		}
		1946.1.1 = {
			owner = USA
			controller = USA

		}
		1950.1.1 = {
			owner = FRG

		}

	}

	provinces={
		561 3474 6421 6594 9416 9557 9572 11404 11417 11529 11544 13116 
	}
	manpower=2708159
	buildings_max_level_factor=1.000
	state_category=city
}
