
state={
	id=361
	name="STATE_361"

	history={
		owner = USA
		victory_points = {
			6984 5 
		}
		victory_points = {
			13209 3
		}
		victory_points = {
			6929 1
		}
		buildings = {
			infrastructure = 7
			industrial_complex = 3
			dockyard = 1
			air_base = 6
			6984 = {
				naval_base = 2

			}
			13209 = {
				naval_base = 1

			}

		}
		add_core_of = USA

	}

	provinces={
		6823 6892 6929 6984 9758 11892 13209 
	}
	manpower=2118394
	buildings_max_level_factor=1.000
	state_category=city
}
