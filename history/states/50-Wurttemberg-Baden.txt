
state={
	id=50
	name="STATE_50"
	resources={
		aluminium=6.000
	}

	history={
		owner = GER
		victory_points = {
			9517 5 
		}
		victory_points = {
			11499 2
		}
		buildings = {
			infrastructure = 7
			arms_factory = 2
			industrial_complex = 2
			air_base = 3

		}
		add_core_of = GER
		1939.1.1 = {
			buildings = {
				arms_factory = 8
				industrial_complex = 3

			}

		}
		1946.1.1 = {
			owner = USA
			controller = USA

		}
		1950.1.1 = {
			owner = FRG

		}

	}

	provinces={
		6555 6581 9517 9545 11499 
	}
	manpower=2152113
	buildings_max_level_factor=1.000
	state_category=city
}
