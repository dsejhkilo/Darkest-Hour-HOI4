state={
	id=882
	name="STATE_882"
	resources={
		oil=12.000
	}
	
	history={
		owner = ENG
		victory_points = {
			1208 3
		}
		victory_points = {
			12283 2
		}
		victory_points = {
			10285 2
		}
		buildings = {
			infrastructure = 3
			1208 = {
				naval_base = 1

			}

		}
		1942.11.22 = {
			owner = ENG
			controller = JAP

		}
		1946.1.1 = {
			owner = ENG
			controller = ENG

		}
		1972.1.1 = {
			owner = MAL
			controller = MAL
			add_core_of = MAL

		}

	}
	
	provinces={
		1208 2117 4216 4396 7371 7443 8091 10269 10285 12283 12905 
	}
	manpower=475000
	buildings_max_level_factor=1.000
	state_category=rural
}
