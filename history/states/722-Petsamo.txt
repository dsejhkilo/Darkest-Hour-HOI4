
state={
	id=722
	name="STATE_722"

	history={
		owner = FIN
		victory_points = {
			9140 1
		}
		buildings = {
			infrastructure = 3
			9140 = {
				naval_base = 1

			}

		}
		add_core_of = FIN
		1945.1.1 = {
		    owner = SOV
			controller = SOV
			add_core_of = SOV
			remove_core_of = FIN
			set_state_name = "Pechenga"
			set_province_name = { id = 9140 name = Pechenga }

		}

	}

	provinces={
		6012 6183 9140 11142 
	}
	manpower=2371
	buildings_max_level_factor=1.000
	state_category=enclave
}
