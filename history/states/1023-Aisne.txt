
state={
	id=1023
	name="STATE_1023"

	history={
		owner = FRA
		victory_points = {
		    3402 1
		}
		buildings = {
			infrastructure = 5
			industrial_complex = 1
			13011 = {
				bunker = 1
			}
		}
		add_core_of = FRA
		1941.6.22 = {
			owner = FRA
			controller = GER
			add_core_of = VIC

		}
		1945.1.1 = {
			owner = FRA
			controller = FRA

		}
		1946.1.1 = {
			remove_core_of = VIC

		}

	}

	provinces={
		3402 3447 13011 
	}
	manpower=471440
	buildings_max_level_factor=1.000
	state_category=rural
}
