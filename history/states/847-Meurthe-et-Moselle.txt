
state={
	id=847
	name="STATE_847"
	resources={
		steel=8.000
	}

	history={
		owner = FRA
		victory_points = {
			11516 5 
		}
		buildings = {
			infrastructure = 7
		}
		add_core_of = FRA
		1941.6.22 = {
			owner = FRA
			controller = GER
			add_core_of = VIC

		}
		1945.1.1 = {
			owner = FRA
			controller = FRA

		}
		1946.1.1 = {
			remove_core_of = VIC

		}

	}

	provinces={
		521 3560 5291 11516 
	}
	manpower=576041
	buildings_max_level_factor=1.000
	state_category=rural
}
