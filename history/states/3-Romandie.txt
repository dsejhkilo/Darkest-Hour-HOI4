
state={
	id=3
	name="STATE_3"
	resources={
		aluminium=28.000
	}

	history={
		owner = SWI
		victory_points = {
			6666 3 
		}
		buildings = {
			infrastructure = 6
			arms_factory = 2
			industrial_complex = 2
			air_base = 4

		}
		add_core_of = SWI

	}

	provinces={
		6666 6683 9622 11590 13124 13490 
	}
	manpower=853944
	buildings_max_level_factor=1.000
	state_category=large_city
}
