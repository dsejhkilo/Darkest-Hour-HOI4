
state={
	id=326
	name="STATE_326"

	history={
		owner = ENG
		victory_points = {
			10062 5
		}
		victory_points = {
			14087 1
		}
		buildings = {
			infrastructure = 5
			air_base = 1
			10062 = {
				naval_base = 6
				coastal_bunker = 1

			}
			14087 = {
				naval_base = 2
				coastal_bunker = 1

			}

		}
		add_core_of = CHI
		add_core_of = PRC
		1942.11.22 = {
			controller = JAP

		}
		1946.1.1 = {
			controller = ENG

		}
		2000.1.1 = {
			owner = PRC
			controller = PRC

		}

	}

	provinces={
		10062 14087 
	}
	manpower=1143516
	buildings_max_level_factor=1.000
	state_category=large_town
}
