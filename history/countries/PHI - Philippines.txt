﻿#########################################################################
# Philippines - 1933
#########################################################################
1933.1.1 = {
	capital = 327
	set_convoys = 5
	oob = "PHI_1933"

	#######################
	# Research
	#######################
	set_technology = {
		### Infantry Tech
		Small_Arms_1900 = 1
		Small_Arms_1916 = 1
		Uniform_1914 = 1
		Uniform_1918 = 1
		Support_Weapons_1914 = 1
		Support_Weapons_1918 = 1
		Heavy_Weapons_1905 = 1
		Heavy_Weapons_1916 = 1
		
		
		### Artillery Tech
		Artillery_1910 = 1
		### Armour Tech
		Armored_Car_1911 = 1
		### Air Tech
		Unarmed_Recon_1910 = 1
        Fighter_1914 = 1
        Fighter_1916 = 1
		### Industry
		Motorized_Plowing = 1
		### Land Doctrines
		Twentieth_Century_Warfare = 1
		Leading_by_Order = 1
		Strongpoint = 1
        Mass_Charge = 1
        Static_Defence = 1
        Counterattack = 1
		Defence_In_Depth = 1
		Separate_Arms_Force = 1
        Combined_Arms_Effort = 1
		Specialized_Branch_Tactical_Experience = 1
	}

	#######################
	# Politics
	#######################
	set_politics = {
		ruling_party = democratic
		last_election = "1929.9.15"
		election_frequency = 72
		elections_allowed = no
	}
	set_popularities = {
		fascist = 0
		authoritarian = 17
		democratic = 83
		socialist = 0
		communist = 0
	}
	add_ideas = {
		industrializing_economy
		isolationism
		police_state
	}
	#######################
	# Leaders
	#######################
	# Democracy
	create_country_leader = {
		name = "Theodore Roosevelt Jr."
		desc = ""
		picture = "P_D_Theodore_Roosevelt_Jr.tga"
		expire = "1935.11.15"
		ideology = social_conservatism
		traits = { POSITION_Governor_General SUBIDEOLOGY_Social_Conservatism HoS_Stern_Imperialist }
	}
	# Authoritarianism
	create_country_leader = {
		name = "Jose P. Laurel"
		desc = ""
		picture = "P_A_Jose_P_Laurel.tga"
		expire = "1945.8.17"
		ideology = authoritarian_democracy
		traits = { POSITION_President SUBIDEOLOGY_Authoritarian_Democracy }
	}
}

##########################################################
# Philippines - 1936
##########################################################
1936.1.1 = {
	oob = "PHI_1936"
	#######################
	# Politics
	#######################
	add_ideas = {
		PHI_HoG_Sergio_Osmena
		industrializing_economy
		isolationism
		police_state
	}
	#######################
	# Leaders
	#######################
	# Democracy
	create_country_leader = {
		name = "Manuel Luis Quezón"
		desc = ""
		picture = "P_D_Manuel_Luis_Quezon.tga"
		expire = "1944.8.1"
		ideology = social_conservatism
		traits = { POSITION_President SUBIDEOLOGY_Social_Conservatism HoG_Silent_Workhorse }
	}
}

##########################################################
# Philippines - 1945
##########################################################
1945.1.1 = {
	#######################
	# Politics
	#######################
	remove_ideas = {
		PHI_HoG_Sergio_Osmena
	}
	#######################
	# Leaders
	#######################
	# Democracy
	create_country_leader = {
		name = "Sergio Osmeña"
		desc = ""
		picture = "P_D_Sergio_Osmena.tga"
		expire = "1961.10.19"
		ideology = social_conservatism
		traits = { POSITION_President SUBIDEOLOGY_Social_Conservatism }
	}
}

