﻿#########################################################################
# Armenia
#########################################################################
capital = 230
oob = "AZR_1936"
set_research_slots = 3
#######################
# Research
#######################
set_technology = {
	Small_Arms_1916 = 1
	Small_Arms_1935 = 1
	tech_recon = 1
	tech_support = 1		
	tech_engineers = 1
	tech_military_police = 1
	tech_mountaineers = 1
	truck_1936 = 1
	paratroopers = 1
	Artillery_1910 = 1	 
	Fighter_1933 = 1
	Fighter_1936 = 1
	Tactical_Bomber_1933 = 1
	Strategic_Bomber_1933 = 1	
	Strategic_Bomber_1936 = 1
	Naval_Bomber_1936 = 1
	transport = 1
	Mass_Assault = 1
	fleet_in_being = 1
}
#######################
# Politics
#######################
set_politics = {
	ruling_party = communist
	last_election = "1932.11.8"
	election_frequency = 48
	elections_allowed = no	
}
set_popularities = {
	fascist = 34
	authoritarian = 0
	democratic = 30
	socialist = 0
	communist = 36
}
#######################
# Leaders
#######################
create_country_leader = {
	name = "Drastamat Kanayan"
	desc = ""
	picture = "gfx/leaders/Europe/Portrait_Europe_Generic_land_5.dds"
	expire = "1953.3.1"
	ideology = fascism
	traits = {}
}
create_country_leader = {
	name = "Hovhannes Kajaznuni"
	desc = ""
	picture = "gfx/leaders/Europe/Portrait_Europe_Generic_2.dds"
	expire = "1953.3.1"
	ideology = social_conservatism
	traits = {}
}
create_country_leader = {
	name = "Grigor Harutyunyan"
	desc = ""
	picture = "gfx/leaders/Europe/Portrait_Europe_Generic_1.dds"
	expire = "1953.3.1"
	ideology = marxism_leninism
	traits = {}
}