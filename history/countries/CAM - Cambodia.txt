﻿capital = 286 #Phnom Penh NOT on the map as VP!

oob = "CAM_1936"

set_research_slots = 3

# Starting tech
# clone of France
set_technology = {
	Small_Arms_1916 = 1
	Small_Arms_1935 = 1
	tech_support = 1
	tech_recon = 1
	tech_mountaineers = 1
	truck_1936 = 1
	Artillery_1910 = 1
	Anti_Aircraft_Gun_1935 = 1
	
	
	
	Fighter_1933 = 1
	Naval_Bomber_1936 = 1
	cv_Fighter_1933 = 1
	Torpedo_bomber_1936 = 1
	Tactical_Bomber_1933 = 1
	Tactical_Bomber_1936 = 1
	CAS_1936 = 1

	transport = 1
	Trench_Warfare = 1
	fleet_in_being = 1
}

set_politics = {
	ruling_party = democratic
	last_election = "1933.2.16"
	election_frequency = 36
	elections_allowed = yes
}
set_popularities = {
	fascist = 0
	authoritarian = 35
	democratic = 40
	socialist = 0
	communist = 25
}
create_country_leader = {
	name = "Pol Pot" 
	desc = ""
	picture = "gfx/leaders/Asia/Portrait_Asia_Generic_warlord1.dds"
	expire = "1953.3.1"
	ideology = socialism #insanity
	traits = {
		
	}
}

#non standard english alphabet character
create_country_leader = {
	name = "Sisowath Youtévong"
	desc = ""
	picture = "gfx/leaders/Asia/Portrait_Asia_Generic_land_4.dds"
	expire = "1953.3.1"
	ideology = social_liberalism
	traits = {
		
	}
}

create_country_leader = {
	name = "Norodom Sihanouk" 
	desc = ""
	picture = "gfx/leaders/Asia/Portrait_Asia_Generic_2.dds"
	expire = "1953.3.1"
	ideology = social_conservatism
	traits = {
		
	}
}

#couldnt find anything historical on fascist

