﻿#########################################################################
# 36th Division 1933
#########################################################################
1933.1.1 = {
	capital = 860
	oob = "TNG_1933"

	#######################
	# Research
	#######################
	set_technology = {
		Small_Arms_1916 = 1
		mass_assault = 1
	}
	#######################
	# Politics
	#######################	
	set_variable = { var = TUN_Ma_Zhongying_Mood value = 0 }
	set_cosmetic_tag = TNG_36TH_DIV
	set_politics = {
		ruling_party = authoritarian
		last_election = "1936.1.1"
		election_frequency = 48
		elections_allowed = no
	}
	set_popularities = {
		fascist = 0
		authoritarian = 100
		democratic = 0
		socialist = 0
		communist = 0
	}
	add_ideas = {
		# Spirits
		TNG_Army_is_the_State
		TNG_Dungans_Armies
		TNG_Overwhelming_Illiterracy
		# Laws and Policies
		one_year_service
		agrarian_economy
		# Cabinet
		TNG_HoG_Ma_Hushan
	}
	#######################
	# Diplomacy
	#######################	
	declare_war_on = {
		target = SIK
		type = annex_everything
	}	
	#######################
	# Leaders
	#######################	
	# Authoritarian
	create_country_leader = {
		name = "Ma Hushan"
		desc = ""
		picture = "P_A_Ma_Hushan.tga"
		expire = "1965.1.1"
		ideology = military_dictatorship
		traits = {
			POSITION_Commander
			SUBIDEOLOGY_Military_Dictatorship
			HoG_Local_Tyrant			
		}
	}
	create_country_leader = {
		name = "Ma Zhongying"
		desc = ""
		picture = "P_A_Ma_Zhongying.tga"
		expire = "1965.1.1"
		ideology = military_dictatorship
		traits = {
			POSITION_Commander
			SUBIDEOLOGY_Military_Dictatorship
			HoS_Little_Commander
		}
	}
	#######################
	# Generals
	#######################	
	create_corps_commander = {
		name = "Ma Hushan"
		picture = "P_A_Ma_Hushan.tga"
		traits = { }
		skill = 2
		attack_skill = 3
		defense_skill = 1
		planning_skill = 2
		logistics_skill = 1
	}
}
#########################################################################
# Tunganistan - 1933
#########################################################################
1936.1.1 = {
	capital = 831
	oob = "TNG_1936"
	set_technology = {
		Small_Arms_1916 = 2	
	}
	drop_cosmetic_tag = yes
	add_ideas = {
		TNG_Uygur_Unrest
	}
	set_politics = {
		ruling_party = authoritarian
		election_frequency = 48
		elections_allowed = no
	}
	set_popularities = {
		fascist = 0
		authoritarian = 100
		democratic = 0
		socialist = 0
		communist = 0
	}
	#######################
	# Diplomacy
	#######################
	diplomatic_relation = { country = SIK relation = war_relation active = no }
	#######################
	# Leaders
	#######################	
	# Authoritarian
	create_country_leader = {
		name = "Ma Hushan"
		desc = ""
		picture = "P_A_Ma_Hushan.tga"
		expire = "1965.1.1"
		ideology = authoritarian_democracy
		traits = { 
			POSITION_Commander
			SUBIDEOLOGY_Military_Dictatorship
			HoG_Local_Tyrant		 
		}
	}
	white_peace = SIK
}