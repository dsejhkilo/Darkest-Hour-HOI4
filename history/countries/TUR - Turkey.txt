#########################################################################
# Turkey - 1933
#########################################################################
1933.1.1 = {
	capital = 49
	set_stability = 0.5
	set_war_support = 0.5
	oob = "TUR_1933"
	add_to_variable = {
		money = 40
	}
	set_research_slots = 3
	set_convoys = 20
	#######################
	# Research
	#######################
	set_technology = {
		### Infantry Tech
		Small_Arms_1916 = 1
		Small_Arms_1900 = 1
		Uniform_1914 = 1
		Uniform_1918 = 1
		Support_Weapons_1914 = 1
		Support_Weapons_1918 = 1
		Heavy_Weapons_1905 = 1
		Heavy_Weapons_1916 = 1
		
		
		### Artillery Tech
		Artillery_1910 = 1
        Artillery_Range_Finding_and_Surveying_Tools = 1
        Artillery_1916 = 1
		### Armour Tech
		Armored_Car_1911 = 1
        Armored_Car_1916 = 1
        Armored_Car_1926 = 1
		Heavy_Tank_1916 = 1
		Heavy_Tank_1917 = 1
		Super_Heavy_Tank_1917 = 1
		Light_Tank_1917 = 1
		Light_Tank_1919 = 1
		### Air Tech
		Unarmed_Recon_1910 = 1
        Fighter_1914 = 1
        Fighter_1916 = 1
        Fighter_1918 = 1
        Fighter_1924 = 1
		Fighter_1933 = 1
		Fighter_Bomber_1916 = 1
        Fighter_Bomber_1918 = 1
        Fighter_Bomber_1924 = 1
		Tactical_Bomber_1910 = 1
        Tactical_Bomber_1914 = 1
        Tactical_Bomber_1916 = 1
        Tactical_Bomber_1918 = 1
        Tactical_Bomber_1925 = 1
		Strategic_Bomber_1916 = 1
		Naval_Bomber_1918 = 1
        Naval_Bomber_1925 = 1
		### Naval Stuff
		DD_1885 = 1
		DD_1900 = 1
		DD_1912 = 1
		DD_1916 = 1
		DD_1922 = 1
		DD_1933 = 1
		CL_1885 = 1
		CL_1900 = 1
		CL_1912 = 1
		CA_1885 = 1
		CA_1895 = 1
		CA_1906 = 1
		SS_1895 = 1
		SS_1912 = 1
		BB_1885 = 1
		BB_1895 = 1
		BB_1900 = 1
		BC_1906 = 1
		BC_1912 = 1
		BC_1916 = 1
		transport = 1
		### Industry
		Industrial_Management = 1
		Factory_Electrification = 1
		Facilities_Design = 1
		Moving_Assembly_Line = 1
		Mass_Production = 1
		Basic_Construction_Machines = 1
		New_Drills = 1
		New_Construction_Standards = 1
		Advanced_Drilling_Method = 1
		Motorized_Plowing = 1
		Farm_Tractors = 1
		Scientific_Agricultural_Processes = 1
		### Land Doctrine Tech
		Twentieth_Century_Warfare = 1
		Leading_by_Task = 1
		Strongpoint = 1
		Mass_Charge = 1
		Static_Defence = 1
		Counterattack = 1
		Schwerpunkt = 1
		Separate_Arms_Force = 1
		Combined_Arms_Effort = 1
		Cross_Branch_Tactical_Experience = 1
	}
	#######################
	# Politics
	#######################
	set_politics = {
		ruling_party = authoritarian
		last_election = "1931.2.8"
		election_frequency = 48
		elections_allowed = no
	}
	set_popularities = {
		fascist = 5
		authoritarian = 75
		democratic = 14
		socialist = 4
		communist = 2
	}
	add_ideas = {
		#Spirits
		TUR_Woes_of_Independence_War
		TUR_Rebellious_Minorities
		TUR_Panturkic_Idealism
		#Hidden Spirits 
		#Cabinet
		TUR_HoG_Mustafa_Ismet_Inonu
		TUR_FM_Tewfik_Rustu_Aras
		TUR_AM_Fuat_Agrali
		TUR_MoS_Sukru_Kaya
		TUR_HoI_Sukru_Ali_Ogel
		#Military Staff
		TUR_CoStaff_Fevzi_Cakmak
		TUR_CoArmy_Ali_Akbaytogan
		TUR_CoNavy_Mehmet_Ali_Ungen
		#Laws
		one_year_service
		neutrality
		limited_exports
		industrializing_economy
		martial_law
	}
    #Variables
		set_variable = { tur_chp_influence_decrease_var = rebelliousness_of_the_minorities_var }
		multiply_variable = { tur_chp_influence_decrease_var = -1 }
		divide_variable = { tur_chp_influence_decrease_var = 100 }    
	#######################
	# Leaders
	#######################
	# Fascism
	create_country_leader = {
		name = "Nihal Atsiz"
		desc = ""
		picture = "P_F_Nihal_Atsiz.tga"
		expire = "1965.1.1"
		ideology = national_socialism
		traits = { POSITION_Leader SUBIDEOLOGY_National_Socialism HoS_Ruthless_Powermonger }
	}
	# Autocracy
	create_country_leader = {
		name = "Mustafa Ismet Inönü"
		desc = ""
		picture = "P_A_Ismet_Inonu.tga"
		expire = "1965.1.1"
		ideology = authoritarian_democracy
		traits = { POSITION_President SUBIDEOLOGY_Authoritarian_Democracy HoG_Political_Protege }
	}
	create_country_leader = {
		name = "Mustafa Kemal Atatürk"
		desc = "Kemal_Ataturk_desc"
		picture = "P_A_Mustafa_Kemal_Ataturk.tga"
		expire = "1965.1.1" #"1938.11.10"
		ideology = authoritarian_democracy
		traits = { POSITION_President SUBIDEOLOGY_Authoritarian_Democracy HoS_Resigned_Generalissimo father_of_turks }
	}
	# Democracy
	create_country_leader = {
		name = "Büyük Millet Meclisi"
		desc = ""
		picture = "P_D_BMM.tga"
		expire = "1965.1.1"
		ideology = social_conservatism
		traits = { POSITION_President SUBIDEOLOGY_Social_Conservatism HoG_Silent_Workhorse }
	}
	# Socialism
	create_country_leader = {
		name = "Mehmet Ali Aybar"
		desc = ""
		picture = "P_S.tga"
		expire = "1965.1.1"
		ideology = socialism
		traits = { POSITION_President SUBIDEOLOGY_Socialism HoG_Old_Lawyer }
	}
	create_country_leader = {
		name = "Kazim Karabekir"
		desc = ""
		picture = "P_S_Kazim_Karabekir.tga"
		expire = "1965.1.1"
		ideology = socialism
		traits = { POSITION_President SUBIDEOLOGY_Socialism HoG_Old_Lawyer }
	}
	# Communism
	create_country_leader = {
		name = "Sefik Hüsnü"
		desc = ""
		picture = "P_C_Sefik_Husnu.tga"
		expire = "1965.1.1"
		ideology = stalinism
		traits = { POSITION_General_Secretary SUBIDEOLOGY_Stalinism HoS_Ruthless_Powermonger }
	}
	#######################
	# Generals
	#######################
	create_field_marshal = {
		name = "Cevat Çobanlı"
		picture = "M_Cevat_Cobanli.tga"
		traits = {   }
		skill = 3
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}
	create_field_marshal = {
		name = "Fevzi Çakmak"
		picture = "M_Fevzi_Cakmak.tga"
		traits = {   }
		skill = 4
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}
	create_field_marshal = {
		name = "Ali Sait Akbaytoğan"
		picture = ""
		traits = {   }
		skill = 3
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}
	create_corps_commander = {
		name = "Muhittin Akyüz"
		picture = ""
		traits = {   }
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}
	create_corps_commander = {
		name = "Nazif Kayacık"
		picture = "M_Nazif_Kayacik.tga"
		traits = {   }
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}
	create_corps_commander = {
		name = "Cemil Conk"
		picture = ""
		traits = {   }
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}
	create_corps_commander = {
		name = "Süleyman Sabri Pasha"
		picture = ""
		traits = {   }
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}
}
#########################################################################
# Turkey - 1936
#########################################################################
1936.1.1 = {
	capital = 49
	set_stability = 0.55
	oob = "TUR_1936"
	set_research_slots = 3
	set_convoys = 20
	#######################
	# Research
	#######################
	set_technology = {
		Small_Arms_1916 = 1
		Small_Arms_1935 = 1
		tech_mountaineers = 1
		Artillery_1910 = 1
		Fighter_1933 = 1
		CAS_1936 = 1
		Strategic_Bomber_1933 = 1
	# Armour Tech
		Heavy_Tank_1916 = 1
		Heavy_Tank_1917 = 1
	#Naval Stuff
		DD_1885 = 1
		DD_1900 = 1
		DD_1912 = 1
		DD_1916 = 1
		DD_1922 = 1
		DD_1933 = 1

		CL_1885 = 1
		CL_1900 = 1
		CL_1912 = 1

		CA_1885 = 1
		CA_1895 = 1
		CA_1906 = 1

		SS_1895 = 1
		SS_1912 = 1
		SS_1916 = 1
		SS_1922 = 1

		BB_1885 = 1
		BB_1895 = 1
		BB_1900 = 1

		BC_1906 = 1
		BC_1912 = 1
		BC_1916 = 1

		transport = 1
	#
	}
}
#########################################################################
# Turkey - 1946
#########################################################################
#1946.1.1 = {
	#######################
	# Politics
	#######################
	#set_politics = {
	#	ruling_party = democratic
	#	last_election = "1935.2.8"
	#	election_frequency = 48
	#	elections_allowed = no
	#}
	#set_popularities = {
	#	fascist = 0
	#	authoritarian = 20
	#	democratic = 80
	#	socialist = 0
	#	communist = 0
	#}
	#######################
	# Leaders
	#######################
	# Democracy
	#create_country_leader = {
	#	name = "Mustafa Ismet Inönü"
	#	desc = ""
	#	picture = "P_A_Ismet_Inonu.tga"
	#	expire = "1965.1.1"
	#	ideology = social_conservatism
	#	traits = { POSITION_President SUBIDEOLOGY_Social_Conservatism HoS_Pig_Headed_Isolationist }
	#}
#}
