﻿#########################################################################
# El Salvador - 1933
#########################################################################
1933.1.1 = {
	capital = 314
	set_convoys = 5
	oob = "ELS_1933"
	
	#######################
	# Research
	#######################
	set_technology = {
		### Infantry Tech
		Small_Arms_1916 = 1
		Small_Arms_1900 = 1
		Uniform_1914 = 1
		Uniform_1918 = 1
		Support_Weapons_1914 = 1
		Support_Weapons_1918 = 1
		Heavy_Weapons_1905 = 1
		Heavy_Weapons_1916 = 1
		
		
		### Artillery Tech
		Artillery_1910 = 1
        Artillery_Range_Finding_and_Surveying_Tools = 1
        Artillery_1916 = 1
		### Air Tech
		Unarmed_Recon_1910 = 1
        Fighter_1914 = 1
        Fighter_1916 = 1
        Fighter_1918 = 1
        Fighter_1924 = 1
		### Industry
		Basic_Construction_Machines = 1
		Motorized_Plowing = 1
		Farm_Tractors = 1
		Scientific_Agricultural_Processes = 1
		### Land Doctrines
		Twentieth_Century_Warfare = 1
		Leading_by_Order = 1
		Strongpoint = 1
        Mass_Charge = 1
        Static_Defence = 1
        Counterattack = 1
		Defence_In_Depth = 1
		Separate_Arms_Force = 1
        Combined_Arms_Effort = 1
		Specialized_Branch_Tactical_Experience = 1
	}
	#######################
	# Politics
	#######################
	set_country_flag = monroe_doctrine
	set_politics = {
		ruling_party = authoritarian
		last_election = "1935.1.15"
		election_frequency = 48
		elections_allowed = yes
	}
	set_popularities = {
		fascist = 7
		authoritarian = 49
		democratic = 38
		socialist = 6
		communist = 0
	}
	add_ideas = {
		agrarian_economy
		neutrality
		police_state
	}
	#######################
	# Leaders
	#######################
	# Autocracy
	create_country_leader = {
		name = "Maximiliano Hernández Martínez"
		desc = "POLITICS_MAXIMILIANO_HERNANDEZ_MARTINEZ_DESC"
		picture = "P_A_Maximiliano_Martinez.tga"
		expire = "1965.1.1"
		ideology = military_dictatorship
		traits = { POSITION_President SUBIDEOLOGY_Military_Dictatorship HoS_Resigned_Generalissimo }
	}
}
#########################################################################
# El Salvador - 1936
#########################################################################
1936.1.1 = {
	oob = "ELS_1936"
	#######################
	# Research
	#######################	
	set_technology = {
		Artillery_1910 = 1
		Anti_Aircraft_Gun_1935 = 1
	}
	#######################
	# Politics
	#######################
	add_ideas = {
		agrarian_economy
		neutrality
		police_state
	}
}
#########################################################################
# El Salvador - 1944
#########################################################################
1944.6.20 = {
	#######################
	# Politics
	#######################	
	set_party_name = {
		ideology = authoritarian 
		name = ELS_authoritarian_military_dictatorship_party 
		long_name = ELS_authoritarian_military_dictatorship_party_long 
	}	
	#######################
	# Leaders
	#######################
	# Autocracy
	create_country_leader = {
		name = "Andrés Ignacio Menéndez"
		desc = ""
		picture = "P_A_Andres_Ignacio_Menendez.tga"
		expire = "1965.1.1"
		ideology = military_dictatorship
		traits = { POSITION_President SUBIDEOLOGY_Military_Dictatorship HoS_Benevolent_Gentleman }
	}
}