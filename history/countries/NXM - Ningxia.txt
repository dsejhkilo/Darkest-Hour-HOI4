﻿capital = 283

oob = "NXM_1936"

# Starting tech
set_technology = {
	Small_Arms_1916 = 1
	Mass_Assault = 1
}

set_politics = {
	ruling_party = authoritarian
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	fascist = 0
	authoritarian = 100
	democratic = 0
	socialist = 0
	communist = 0
}
add_ideas = {
	CXB_HoG_Ma_Hongbin
	agrarian_economy
}
create_country_leader = {
	name = "Ma Hongkui"
	desc = ""
	picture = "Xibei_PA_Ma_Hongkui.tga"
	expire = "1965.1.1"
	ideology = despotism
	traits = {
		POSITION_Civil_Governor
		SUBIDEOLOGY_Despotism
		HoS_Barking_Buffoon
	}
}

create_field_marshal = {
	name = "Ma Hongkui"
	picture = "Xibei_PA_Ma_Hongkui.tga"
	traits = { offensive_doctrine trait_reckless }
	skill = 4
	attack_skill = 4
	defense_skill = 2
	planning_skill = 3
	logistics_skill = 1
}
create_field_marshal = {
	name = "Ma Hongbin"
	picture = "P_A_Ma_Hongbin.tga"
	traits = { defensive_doctrine } 
	skill = 3
	attack_skill = 2
	defense_skill = 2
	planning_skill = 3
	logistics_skill = 2
}