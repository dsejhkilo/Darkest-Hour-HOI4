﻿#########################################################################
# Belgium - 1933
#########################################################################
1933.1.1 = {
	capital = 6
	oob = "BEL_1933"
	set_research_slots = 3
	set_stability = 0.75
	set_convoys = 80
	#######################
	# Research
	#######################
	set_technology = {
		### Infantry Tech
		Small_Arms_1900 = 1
		Small_Arms_1916 = 1
		Uniform_1914 = 1
		Uniform_1918 = 1
		Support_Weapons_1914 = 1
		Support_Weapons_1918 = 1
		Heavy_Weapons_1905 = 1
		Heavy_Weapons_1916 = 1
		
		
		### Support Company Tech
		tech_support = 1
		tech_recon = 1
		tech_engineers = 1
		### Arty Tech
		Artillery_1910 = 1
		Artillery_Range_Finding_and_Surveying_Tools = 1
		Artillery_1916 = 1	
		### Armor Tech
		truck_1936 = 1
		Armored_Car_1911 = 1
		Armored_Car_1916 = 1
		Armored_Car_1926 = 1
		Heavy_Tank_1916 = 1
		Heavy_Tank_1917 = 1
		Light_Tank_1917 = 1
		Light_Tank_1919 = 1
		Light_Tank_1926 = 1
		### Light Air Tech
		Unarmed_Recon_1910 = 1
		Fighter_1914 = 1
		Fighter_1916 = 1
		Fighter_1918 = 1
		Fighter_1924 = 1
		Fighter_1933 = 1
		### Heavy Air Tech
		Tactical_Bomber_1910 = 1
		Tactical_Bomber_1914 = 1
		Tactical_Bomber_1916 = 1
		Tactical_Bomber_1918 = 1
		Tactical_Bomber_1925 = 1
		### Land Doctrine Tech
		Twentieth_Century_Warfare = 1
		Leading_by_Order = 1
		Strongpoint = 1
		Mass_Charge = 1
		Static_Defence = 1
		Counterattack = 1
		Defence_In_Depth = 1
		Separate_Arms_Force = 1
		Combined_Arms_Effort = 1
		Specialized_Branch_Tactical_Experience = 1
		Planning_Focus = 1
		### Industry
		Industrial_Management = 1
		Factory_Electrification = 1
		Facilities_Design = 1
		Moving_Assembly_Line = 1
		Skilled_Workforce = 1
		Concentrated_Industries = 1
		Basic_Construction_Machines = 1
		New_Drills = 1
		New_Construction_Standards = 1
		Advanced_Drilling_Method = 1
		Construction_Site_Management = 1
		Motorized_Plowing = 1
		Farm_Tractors = 1
		Scientific_Agricultural_Processes = 1
		#Naval stuff
		DD_1885 = 1
		#
	}
	#######################
	# Politics
	#######################
	set_politics = {
		ruling_party = democratic
		last_election = "1932.11.27"
		election_frequency = 48
		elections_allowed = yes
	}
	set_popularities = {
		fascist = 5
		authoritarian = 1
		democratic = 90
		socialist = 1
		communist = 3
	}
	add_ideas = {
		# Cabinet
		BEL_HoG_Albert_I
		BEL_FM_Paul_Van_Zeeland
		BEL_AM_Albert_Deveze
		BEL_MoS_Eugene_Soudan
		neutrality
		full_civil_liberties
		# Military Staff
		BEL_CoStaff_Oscar_Cumont
		BEL_CoArmy_Victor_van_Strydonck_de_Burkel
		BEL_CoNavy_G_Timmermans
		BEL_CoAir_LFE_Wouters
	}
	#######################
	# Leaders
	#######################
	# Fascism
	create_country_leader = {
		name = "Léon Degrelle"
		desc = ""
		picture = "P_F_Leon_Degrelle.tga"
		expire = "1965.1.1"
		ideology = rexism
		traits = {
			POSITION_Prime_Minister
			SUBIDEOLOGY_Rexism
			HoS_Barking_Buffoon
		}
	}
	# Paternal Autocracy
	create_country_leader = {
		name = "Leopold III"
		desc = ""
		picture = "P_A_Leopold_III.tga"
		expire = "1965.1.1"
		ideology = monarchism
		traits = {
			POSITION_King
			SUBIDEOLOGY_Monarchism
			HoS_Benevolent_Gentleman
		}
	}
	# Democracy
	create_country_leader = {
		name = "Charles de Broqueville"
		desc = ""
		picture = "P_D_Charles_de_Broqueville.tga"
		expire = "1965.1.1"
		ideology = social_conservatism
		traits = {
			POSITION_Prime_Minister
			SUBIDEOLOGY_Social_Conservatism
			HoG_Silent_Workhorse
		}
	}
	# Socialism
	create_country_leader = {
		name = "Joseph Jacquemotte"
		desc = ""
		picture = "P_S_Joseph_Jacquemotte.tga"
		expire = "1965.1.1"
		ideology = syndicalism
		traits = {
			POSITION_Prime_Minister
			SUBIDEOLOGY_Syndicalism
			HoG_Political_Protege
		}
	}
	# Communism
	create_country_leader = {
		name = "Julien Lahaut"
		desc = ""
		picture = "P_C_Julien_Lahaut.tga"
		expire = "1965.1.1"
		ideology = marxism_leninism
		traits = {
			POSITION_Chairman
			SUBIDEOLOGY_Marxism_Leninism
			HoS_Ruthless_Powermonger
		}
	}
	#######################
	# Generals
	#######################
	create_corps_commander = {
		name = "Lambert Auguste Raymond Chardome"
		picture = "M_Lambert_Auguste_Raymond_Chardome.tga"
		traits = {
			old_guard
		}
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}
	
	create_corps_commander = {
		name = "Oscar Michiels"
		picture = "M_Oscar_Michiels.tga"
		traits = {

		}
		skill = 2
		attack_skill = 3
		defense_skill = 2
		planning_skill = 1
		logistics_skill = 2
	}
	
	create_corps_commander = {
		name = "Edouard van den Bergen"
		picture = "M_Edouard_van_den_Bergen.tga"
		traits = {

		}
		skill = 2
		attack_skill = 3
		defense_skill = 1
		planning_skill = 2
		logistics_skill = 1
	}
	
	create_corps_commander = {
		name = "Victor van Strydonck de Burkel"
		picture = "M_Victor_van_Strydonck_de_Burkel.tga"
		traits = {

		}
		skill = 3
		attack_skill = 2
		defense_skill = 3
		planning_skill = 2
		logistics_skill = 1
	}
	
	create_corps_commander = {
		name = "Maximilien Alfred Theodore de Neve de Roden"
		picture = "M_Maximilien_Alfred_Theodore_de_Neve_de_Roden.tga"
		traits = {

		}
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 2
		logistics_skill = 3
	}
	#######################
	# Admirals
	#######################
	create_navy_leader = {
		name = "Georges Timmermans"
		portrait_path = "gfx/leaders/Europe/Portrait_Europe_Generic_navy_3.dds"
		traits = {
			bold
		}
		skill = 4
		attack_skill = 4
		defense_skill = 2
		maneuvering_skill = 3
		coordination_skill = 4
	}
}

#########################################################################
# Belgium - 1936
#########################################################################
1936.1.1 = {
	oob = "BEL_1936"
	#######################
	# Research
	#######################
	set_technology = {
		Small_Arms_1916 = 1
		Small_Arms_1935 = 1
		tech_support = 1
		tech_recon = 1
		tech_engineers = 1
		Artillery_1910 = 1
		truck_1936 = 1
		Fighter_1933 = 1
		Trench_Warfare = 1
		# Armour Tech
		Heavy_Tank_1916 = 1
		Heavy_Tank_1917 = 1
		Light_Tank_1926 = 1
		#Naval stuff
		DD_1885 = 1
		#
	}
	#######################
	# Politics
	#######################
	set_politics = {
		ruling_party = democratic
		last_election = "1932.11.27"
		election_frequency = 48
		elections_allowed = yes
	}
	set_popularities = {
		fascist = 5
		authoritarian = 1
		democratic = 90
		socialist = 1
		communist = 3
	}
	add_ideas = {
		# Cabinet
		BEL_HoS_Leopold_III
		BEL_FM_Paul_Van_Zeeland
		BEL_AM_Albert_Deveze
		BEL_MoS_Eugene_Soudan
		neutrality
		full_civil_liberties
		# Military Staff
		BEL_CoStaff_Oscar_Cumont
		BEL_CoArmy_Victor_van_Strydonck_de_Burkel
		BEL_CoNavy_G_Timmermans
		BEL_CoAir_LFE_Wouters
	}
	#######################
	# Leaders
	#######################
	# Democracy
	create_country_leader = {
		name = "Paul van Zeeland"
		desc = ""
		picture = "P_D_Paul_Van_Zeeland.tga"
		expire = "1965.1.1"
		ideology = social_conservatism
		traits = {
			POSITION_Prime_Minister
			SUBIDEOLOGY_Social_Conservatism
			HoG_Silent_Workhorse
		}
	}
}

############################################
# Belgium - 1940
############################################
1940.5.10 = {
	oob = "BEL_1940"
}

############################################
# Belgium - 1944
############################################
1944.6.20 = {
	oob = "BEL_1944"
}
