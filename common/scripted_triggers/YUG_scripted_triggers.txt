#scripted trigger for Yugoslavia

is_available_fighter_YUG = {
	OR = {
		AND = {
			has_tech = Fighter_1936
			YUG = { 
				NOT = { has_tech = Fighter_1936}
			}
		}
		AND = {
			has_tech = Fighter_1940
			YUG = { 
				NOT = { has_tech = Fighter_1940}
			}
		}
		AND = {
			has_tech = Fighter_1943
			YUG = { 
				NOT = { has_tech = Fighter_1943}
			}
		}
	}
	NOT = {
		has_war_with = YUG
	}
}

is_available_heavy_fighter_YUG = {
	OR = {
		AND = {
			has_tech = Heavy_Fighter_1936
			YUG = { 
				NOT = { has_tech = Heavy_Fighter_1936}
			}
		}
		AND = {
			has_tech = Heavy_Fighter_1940
			YUG = { 
				NOT = { has_tech = Heavy_Fighter_1940}
			}
		}
		AND = {
			has_tech = Heavy_Fighter_1943
			YUG = { 
				NOT = { has_tech = Heavy_Fighter_1943}
			}
		}
	}
	NOT = {
		has_war_with = YUG
	}
}

is_available_cas_YUG = {
	OR = {
		AND = {
			has_tech = CAS_1936
			YUG = { 
				NOT = { has_tech = CAS_1936}
			}
		}
		AND = {
			has_tech = CAS_1940
			YUG = { 
				NOT = { has_tech = CAS_1940}
			}
		}
		AND = {
			has_tech = CAS_1943
			YUG = { 
				NOT = { has_tech = CAS_1943}
			}
		}
	}
	NOT = {
		has_war_with = YUG
	}
}

is_available_tac_YUG = {
	OR = {
		AND = {
			has_tech = Tactical_Bomber_1936
			YUG = { 
				NOT = { has_tech = Tactical_Bomber_1936}
			}
		}
		AND = {
			has_tech = Tactical_Bomber_1940
			YUG = { 
				NOT = { has_tech = Tactical_Bomber_1940}
			}
		}
		AND = {
			has_tech = Tactical_Bomber_1943
			YUG = { 
				NOT = { has_tech = Tactical_Bomber_1943}
			}
		}
	}
	NOT = {
		has_war_with = YUG
	}
}