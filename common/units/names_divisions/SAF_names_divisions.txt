﻿# Division template historical names system. Is a new method of naming the divisions based on the names-group assigned to it's template.
# If we run out of all historical names, the names will be assigned in the old way.
#
# Each group has a made up tag. Use it if you want to append more, or replace existing names with the extra txt files (modding).
#
# for_countries - lists all countries that SAF use it. If empty, or entire tag is missing, all countries in the world SAF use it.
#
# can_use - is a trigger that locks/unlocks the group under specific circumstances. The trigger is in a country scope.
#
# division_types - is a list of tokens to corresponding unit types. A player SAF in fact use any group of names for a div.template
#                  however this tag is a helper for an automated choice (for AI, or if the group must switch on it's own, because
#                  for example the current one is no longer available due to the can_use trigger saying so).
#                  In automated choice, the division template must have at leSAF 1 of the following types for it to be chosen.
#
# fallback_name - Is going to be used if we run out of the scripted historical names. If you want to use the old division naming
#                 mechanics to be used for fallbacks, then just skip this option.
#
# unordered - It's a list of historical division names that did not have a number. Regardless if such names happened in history or not
#             this option is available here mainly for a mods.
#
# ordered - Is a list of all historical names. 
#           Numbers must start from 1 and up. 
#           Numbers can't repeat in one scope.
#           If you script the name for this group, at the same number (for example in a mod in another file), the name will be override.
#           All arguments between the brackets must be separated by spaces. Each argument is wrapped in "quotas".
#           1st argument = The name. It must contain either: 
#                          %d (for decimal number placement)
#                          %s (for string number placement - ROMAN numbers like XIV).
#           2nd argument is optional = A localizable text describing this historical division. The text will be visible in the tooltip
#                                      where you choose the historical division name.
#           3rd argument is optional = An URL preferably pointing to the WIKI. It's a future feature that is not currently working in
#                                      the current game version.

SAF_INF_01 = 
{
	name = "Infantry Divisions"

	for_countries = { SAF }

	can_use = { always = yes }

	division_types = { "infantry" "light_infantry" "shock_infantry" "bicycle_battalion" "motorized" "mechanized" "heavy_mechanized" "mountaineers" "marine" }
	# Currently South African battalions are called South African Infantry whether they are air assault, seaborne assault, light infantry, mechanized, or motorized.
	# Number reservation system will tie to another group.
	#link_numbering_with = { GER_Gar_01 }

	fallback_name = "%d South African Infantry Division"

	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered =
	{
		1 = { "%dst South African Infantry Division" }
		2 = { "%dnd South African Infantry Division" }
		3 = { "%drd South African Infantry Division" }
		4 = { "%dth South African Infantry Division" }
		5 = { "%dth South African Infantry Division" }
		6 = { "%dth South African Infantry Division" }
		7 = { "%dth South African Infantry Division" }
		8 = { "%dth South African Infantry Division" }
		9 = { "%dth South African Infantry Division" }
		10 = { "%dth South African Infantry Division" }
		11 = { "%dth South African Infantry Division" }
		12 = { "%dth South African Infantry Division" }
		13 = { "%dth South African Infantry Division" }
		14 = { "%dth South African Infantry Division" }
		15 = { "%dth South African Infantry Division" }
		16 = { "%dth South African Infantry Division" }
		17 = { "%dth South African Infantry Division" }
		18 = { "%dth South African Infantry Division" }
		19 = { "%dth South African Infantry Division" }
		20 = { "%dth South African Infantry Division" }
		21 = { "%dst South African Infantry Division" }
		22 = { "%dnd South African Infantry Division" }
		23 = { "%drd South African Infantry Division" }
		24 = { "%dth South African Infantry Division" }
		25 = { "%dth South African Infantry Division" }
		26 = { "%dth South African Infantry Division" }
		27 = { "%dth South African Infantry Division" }
		28 = { "%dth South African Infantry Division" }
		29 = { "%dth South African Infantry Division" }
		30 = { "%dth South African Infantry Division" }
		31 = { "%dst South African Infantry Division" }
		32 = { "%dnd South African Infantry Division" }
		33 = { "%drd South African Infantry Division" }
		34 = { "%dth South African Infantry Division" }
		35 = { "%dth South African Infantry Division" }
		36 = { "%dth South African Infantry Division" }
		37 = { "%dth South African Infantry Division" }
		38 = { "%dth South African Infantry Division" }
		39 = { "%dth South African Infantry Division" }
		40 = { "%dth South African Infantry Division" }
		41 = { "%dst South African Infantry Division" }
		42 = { "%dnd South African Infantry Division" }
		43 = { "%drd South African Infantry Division" }
		44 = { "%dth South African Infantry Division" }
		45 = { "%dth South African Infantry Division" }
		46 = { "%dth South African Infantry Division" }
		47 = { "%dth South African Infantry Division" }
		48 = { "%dth South African Infantry Division" }
		49 = { "%dth South African Infantry Division" }
		50 = { "%dth South African Infantry Division" }
		51 = { "%dst South African Infantry Division" }
		52 = { "%dnd South African Infantry Division" }
		53 = { "%drd South African Infantry Division" }
		54 = { "%dth South African Infantry Division" }
		55 = { "%dth South African Infantry Division" }
		56 = { "%dth South African Infantry Division" }
		57 = { "%dth South African Infantry Division" }
		58 = { "%dth South African Infantry Division" }
		59 = { "%dth South African Infantry Division" }
		60 = { "%dth South African Infantry Division" }
		61 = { "%dst South African Infantry Division" }
		62 = { "%dnd South African Infantry Division" }
		63 = { "%drd South African Infantry Division" }
		64 = { "%dth South African Infantry Division" }
		65 = { "%dth South African Infantry Division" }
		66 = { "%dth South African Infantry Division" }
		67 = { "%dth South African Infantry Division" }
		68 = { "%dth South African Infantry Division" }
		69 = { "%dth South African Infantry Division" }
		70 = { "%dth South African Infantry Division" }
		71 = { "%dst South African Infantry Division" }
		72 = { "%dnd South African Infantry Division" }
		73 = { "%drd South African Infantry Division" }
		74 = { "%dth South African Infantry Division" }
		75 = { "%dth South African Infantry Division" }
		76 = { "%dth South African Infantry Division" }
		77 = { "%dth South African Infantry Division" }
		78 = { "%dth South African Infantry Division" }
		79 = { "%dth South African Infantry Division" }
		80 = { "%dth South African Infantry Division" }
		81 = { "%dst South African Infantry Division" }
		82 = { "%dnd South African Infantry Division" }
		83 = { "%drd South African Infantry Division" }
		84 = { "%dth South African Infantry Division" }
		85 = { "%dth South African Infantry Division" }
		86 = { "%dth South African Infantry Division" }
		87 = { "%dth South African Infantry Division" }
		88 = { "%dth South African Infantry Division" }
		89 = { "%dth South African Infantry Division" }
		90 = { "%dth South African Infantry Division" }
		91 = { "%dst South African Infantry Division" }
		92 = { "%dnd South African Infantry Division" }
		93 = { "%drd South African Infantry Division" }
		94 = { "%dth South African Infantry Division" }
		95 = { "%dth South African Infantry Division" }
		96 = { "%dth South African Infantry Division" }
		97 = { "%dth South African Infantry Division" }
		98 = { "%dth South African Infantry Division" }
		99 = { "%dth South African Infantry Division" }

		# SAF only had three historically, but just in case!
	}
}

SAF_CAV_01 = 
{
	name = "Cavalry Divisions"

	for_countries = { SAF }

	can_use = { always = yes }

	division_types = { "cavalry" }

	# Number reservation system will tie to another group.
	#link_numbering_with = { GER_Gar_01 }

	fallback_name = "%d South African Mounted Division"

	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered =
	{
		1 = { "%dst South African Mounted Division" }
		2 = { "%dnd South African Mounted Division" }
		3 = { "%drd South African Mounted Division" }
		4 = { "%dth South African Mounted Division" }
		5 = { "%dth South African Mounted Division" }
		6 = { "%dth South African Mounted Division" }
		7 = { "%dth South African Mounted Division" }
		8 = { "%dth South African Mounted Division" }
		9 = { "%dth South African Mounted Division" }
		10 = { "%dth South African Mounted Division" }
		11 = { "%dth South African Mounted Division" }
		12 = { "%dth South African Mounted Division" }
		13 = { "%dth South African Mounted Division" }
		14 = { "%dth South African Mounted Division" }
		15 = { "%dth South African Mounted Division" }
		16 = { "%dth South African Mounted Division" }
		17 = { "%dth South African Mounted Division" }
		18 = { "%dth South African Mounted Division" }
		19 = { "%dth South African Mounted Division" }
		20 = { "%dth South African Mounted Division" }
		21 = { "%dst South African Mounted Division" }
		22 = { "%dnd South African Mounted Division" }
		23 = { "%drd South African Mounted Division" }
		24 = { "%dth South African Mounted Division" }
		25 = { "%dth South African Mounted Division" }
		26 = { "%dth South African Mounted Division" }
		27 = { "%dth South African Mounted Division" }
		28 = { "%dth South African Mounted Division" }
		29 = { "%dth South African Mounted Division" }
		30 = { "%dth South African Mounted Division" }
		31 = { "%dst South African Mounted Division" }
		32 = { "%dnd South African Mounted Division" }
		33 = { "%drd South African Mounted Division" }
		34 = { "%dth South African Mounted Division" }
		35 = { "%dth South African Mounted Division" }
		36 = { "%dth South African Mounted Division" }
		37 = { "%dth South African Mounted Division" }
		38 = { "%dth South African Mounted Division" }
		39 = { "%dth South African Mounted Division" }
		40 = { "%dth South African Mounted Division" }
		41 = { "%dst South African Mounted Division" }
		42 = { "%dnd South African Mounted Division" }
		43 = { "%drd South African Mounted Division" }
		44 = { "%dth South African Mounted Division" }
		45 = { "%dth South African Mounted Division" }
		46 = { "%dth South African Mounted Division" }
		47 = { "%dth South African Mounted Division" }
		48 = { "%dth South African Mounted Division" }
		49 = { "%dth South African Mounted Division" }
		50 = { "%dth South African Mounted Division" }
		51 = { "%dst South African Mounted Division" }
		52 = { "%dnd South African Mounted Division" }
		53 = { "%drd South African Mounted Division" }

		# Historically, there's no way SAF would have had 53 Cav divisions the list taken this far because of cav's suppression role in-game.
	}
}

SAF_AC_01 = 
{
	name = "Armoured Car Divisions"

	for_countries = { SAF }

	can_use = { always = yes }

	division_types = { "armored_car" }

	# Number reservation system will tie to another group.
	#link_numbering_with = { GER_Gar_01 }

	fallback_name = "%d South African Special Service Division"

	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered =
	{
		1 = { "%dst South African Special Service Division" }
		2 = { "%dnd South African Special Service Division" }
		3 = { "%drd South African Special Service Division" }
		4 = { "%dth South African Special Service Division" }
		5 = { "%dth South African Special Service Division" }
		6 = { "%dth South African Special Service Division" }
		7 = { "%dth South African Special Service Division" }
		8 = { "%dth South African Special Service Division" }
		9 = { "%dth South African Special Service Division" }
		10 = { "%dth South African Special Service Division" }
		11 = { "%dth South African Special Service Division" }
		12 = { "%dth South African Special Service Division" }
		13 = { "%dth South African Special Service Division" }
		14 = { "%dth South African Special Service Division" }
		15 = { "%dth South African Special Service Division" }
		16 = { "%dth South African Special Service Division" }
		17 = { "%dth South African Special Service Division" }
		18 = { "%dth South African Special Service Division" }
		19 = { "%dth South African Special Service Division" }
		20 = { "%dth South African Special Service Division" }
		21 = { "%dst South African Special Service Division" }
		22 = { "%dnd South African Special Service Division" }
		23 = { "%drd South African Special Service Division" }
		24 = { "%dth South African Special Service Division" }
		25 = { "%dth South African Special Service Division" }
		26 = { "%dth South African Special Service Division" }
		27 = { "%dth South African Special Service Division" }
		28 = { "%dth South African Special Service Division" }
		29 = { "%dth South African Special Service Division" }
		30 = { "%dth South African Special Service Division" }
		31 = { "%dst South African Special Service Division" }
		32 = { "%dnd South African Special Service Division" }
		33 = { "%drd South African Special Service Division" }
		34 = { "%dth South African Special Service Division" }
		35 = { "%dth South African Special Service Division" }
		36 = { "%dth South African Special Service Division" }
		37 = { "%dth South African Special Service Division" }
		38 = { "%dth South African Special Service Division" }
		39 = { "%dth South African Special Service Division" }
		40 = { "%dth South African Special Service Division" }
		41 = { "%dst South African Special Service Division" }
		42 = { "%dnd South African Special Service Division" }
		43 = { "%drd South African Special Service Division" }
		44 = { "%dth South African Special Service Division" }
		45 = { "%dth South African Special Service Division" }
		46 = { "%dth South African Special Service Division" }
		47 = { "%dth South African Special Service Division" }
		48 = { "%dth South African Special Service Division" }
		49 = { "%dth South African Special Service Division" }
		50 = { "%dth South African Special Service Division" }
		51 = { "%dst South African Special Service Division" }
		52 = { "%dnd South African Special Service Division" }
		53 = { "%drd South African Special Service Division" }

		# Historically, there's no way SAF would have had 53 Cav divisions the list taken this far because of cav's suppression role in-game.
	}
}

SAF_ARM_01 = 
{
	name = "Armoured Divisions"

	for_countries = { SAF }

	can_use = { always = yes }

	division_types = { "light_armor" "medium_armor" "heavy_armor" }

	# Number reservation system will tie to another group.
	link_numbering_with = { SAF_INF_01 }

	fallback_name = "%d South African Armoured Division"

	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered =
	{
		6 = { "%dth South African Armoured Division" }
		7 = { "%dth South African Armoured Division" }
		8 = { "%dth South African Armoured Division" }
		9 = { "%dth South African Armoured Division" }
		10 = { "%dth South African Armoured Division" }
		11 = { "%dth South African Armoured Division" }
		12 = { "%dth South African Armoured Division" }
		13 = { "%dth South African Armoured Division" }
		14 = { "%dth South African Armoured Division" }
		15 = { "%dth South African Armoured Division" }
		16 = { "%dth South African Armoured Division" }
		17 = { "%dth South African Armoured Division" }
		18 = { "%dth South African Armoured Division" }
		19 = { "%dth South African Armoured Division" }
		20 = { "%dth South African Armoured Division" }
		21 = { "%dst South African Armoured Division" }
		22 = { "%dnd South African Armoured Division" }
		23 = { "%drd South African Armoured Division" }
		24 = { "%dth South African Armoured Division" }
		25 = { "%dth South African Armoured Division" }
		26 = { "%dth South African Armoured Division" }
		27 = { "%dth South African Armoured Division" }
		28 = { "%dth South African Armoured Division" }
		29 = { "%dth South African Armoured Division" }
		30 = { "%dth South African Armoured Division" }
		31 = { "%dst South African Armoured Division" }
		32 = { "%dnd South African Armoured Division" }
		33 = { "%drd South African Armoured Division" }

		# Historically, only the 6th was armoured, but suspect they intended to have a linked number system (as per the Canadians) but started their armoured units a bit later in the sequence - this is a guess though.
	}
}

SAF_GAR_01 = 
{
	name = "Garrison Divisions"

	for_countries = { SAF }

	can_use = { always = yes }

	division_types = { "infantry" }

	# Number reservation system will tie to another group.
	#link_numbering_with = { SAF_INF_01 }

	fallback_name = "%d District Force"

	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered =
	{
		1 = { "Cape Command" }
		2 = { "Natal Command" }
		3 = { "Orange Free State Command" }
		4 = { "Eastern Province Command" }
		5 = { "Robert's Heights and Transvaal Command" }
		6 = { "Witwatersrand Command" }
		7 = { "%dth District Force" }
		8 = { "%dth District Force" }
		9 = { "%dth District Force" }
		10 = { "%dth District Force" }
		11 = { "%dth District Force" }
		12 = { "%dth District Force" }
		13 = { "%dth District Force" }
		14 = { "%dth District Force" }
		15 = { "%dth District Force" }
		16 = { "%dth District Force" }
		17 = { "%dth District Force" }
		18 = { "%dth District Force" }
		19 = { "%dth District Force" }
		20 = { "%dth District Force" }
		21 = { "%dst District Force" }
		22 = { "%dnd District Force" }
		23 = { "%drd District Force" }
		24 = { "%dth District Force" }
		25 = { "%dth District Force" }
		26 = { "%dth District Force" }
		27 = { "%dth District Force" }
		28 = { "%dth District Force" }
		29 = { "%dth District Force" }
		30 = { "%dth District Force" }
		31 = { "%dst District Force" }
		32 = { "%dnd District Force" }
		33 = { "%drd District Force" }
		34 = { "%dth District Force" }
		35 = { "%dth District Force" }
		36 = { "%dth District Force" }
		37 = { "%dth District Force" }
		38 = { "%dth District Force" }
		39 = { "%dth District Force" }
		40 = { "%dth District Force" }
		41 = { "%dst District Force" }
		42 = { "%dnd District Force" }
		43 = { "%drd District Force" }
		44 = { "%dth District Force" }
		45 = { "%dth District Force" }
		46 = { "%dth District Force" }
		47 = { "%dth District Force" }
		48 = { "%dth District Force" }
		49 = { "%dth District Force" }
		51 = { "%dst District Force" }
		52 = { "%dnd District Force" }
		53 = { "%drd District Force" }

		# SAF has six named 'commands' that were more-or-less garrison forces. After that, have used a generic 'xth District Force' structure.
	}
}

SAF_PAR_01 = 
{
	name = "Airborne Divisions"

	for_countries = { SAF }

	can_use = { always = yes }

	division_types = { "paratrooper" }

	# Number reservation system will tie to another group.

	fallback_name = "%d South African Parachute Division"

	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered =
	{
		1 = { "%dst South African Parachute Division" }
		2 = { "%dnd South African Parachute Division" }
		3 = { "%drd South African Parachute Division" }
		4 = { "%dth South African Parachute Division" }
		5 = { "%dth South African Parachute Division" }
		6 = { "%dth South African Parachute Division" }
		7 = { "%dth South African Parachute Division" }
		8 = { "%dth South African Parachute Division" }
		9 = { "%dth South African Parachute Division" }
		10 = { "%dth South African Parachute Division" }
		11 = { "%dth South African Parachute Division" }
		12 = { "%dth South African Parachute Division" }
		13 = { "%dth South African Parachute Division" }
		14 = { "%dth South African Parachute Division" }
		15 = { "%dth South African Parachute Division" }
		16 = { "%dth South African Parachute Division" }
		17 = { "%dth South African Parachute Division" }
		18 = { "%dth South African Parachute Division" }
		19 = { "%dth South African Parachute Division" }
		20 = { "%dth South African Parachute Division" }
		21 = { "%dst South African Parachute Division" }
		22 = { "%dnd South African Parachute Division" }
		23 = { "%drd South African Parachute Division" }
		24 = { "%dth South African Parachute Division" }
		25 = { "%dth South African Parachute Division" }
		26 = { "%dth South African Parachute Division" }
		27 = { "%dth South African Parachute Division" }
		28 = { "%dth South African Parachute Division" }
		29 = { "%dth South African Parachute Division" }
		30 = { "%dth South African Parachute Division" }

		# SAF didn't have any Airborne divisions.  Have independent sequence.
	}
}