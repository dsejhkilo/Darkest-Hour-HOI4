ideas = {
#################################################
### Head of Government
#################################################
	Head_of_Government = {
	# Carlos Alberto Ucles
		HON_HoG_Carlos_Alberto_Ucles = {
			picture = Carlos_Alberto_Ucles
			allowed = { tag = HON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Carlos_Alberto_Ucles_unavailable }
			}
			
			
			traits = { ideology_A HoG_Silent_Workhorse }
		}
	# Oswaldo López Arellano
		HON_HoG_Oswaldo_Lopez_Arellano = {
			picture = Oswaldo_Lopez_Arellano
			allowed = { tag = HON }
			available = {
				date > 1973.1.1
				date < 2011.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Oswaldo_Lopez_Arellano_unavailable }
			}
			
			
			traits = { ideology_A HoG_Happy_Amateur }
		}
	# Juan Alberto Melgar Castro
		HON_HoG_Juan_Alberto_Melgar_Castro = {
			picture = Juan_Alberto_Melgar_Castro
			allowed = { tag = HON }
			available = {
				date > 1975.1.1
				date < 1988.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Juan_Alberto_Melgar_Castro_unavailable }
			}
			
			
			traits = { ideology_A HoG_Flamboyant_Tough_Guy }
		}
	# Policarpo Paz Garcia
		HON_HoG_Policarpo_Paz_Garcia = {
			picture = Policarpo_Paz_Garcia
			allowed = { tag = HON }
			available = {
				date > 1978.1.1
				date < 2001.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Policarpo_Paz_Garcia_unavailable }
			}
			
			
			traits = { ideology_A HoG_Backroom_Backstabber }
		}
	# Carlos Alberto Ucles
		HON_HoG_Carlos_Alberto_Ucles_2 = {
			picture = Carlos_Alberto_Ucles_2
			allowed = { tag = HON }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Carlos_Alberto_Ucles_unavailable }
			}
			
			
			traits = { ideology_D HoG_Silent_Workhorse }
		}
	# Tiburcio Carias Andina
		HON_HoG_Tiburcio_Carias_Andina = {
			picture = Tiburcio_Carias_Andina
			allowed = { tag = HON }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Tiburcio_Carias_Andina_unavailable }
			}
			
			
			traits = { ideology_D HoG_Old_General }
		}
	# Abraham Williams Calderon
		HON_HoG_Abraham_Williams_Calderon = {
			picture = Abraham_Williams_Calderon
			allowed = { tag = HON }
			available = {
				date > 1943.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Abraham_Williams_Calderon_unavailable }
			}
			
			
			traits = { ideology_D HoG_Political_Protege }
		}
	# Juan Manuel Galvez
		HON_HoG_Juan_Manuel_Galvez = {
			picture = Juan_Manuel_Galvez
			allowed = { tag = HON }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Juan_Manuel_Galvez_unavailable }
			}
			
			
			traits = { ideology_D HoG_Old_General }
		}
	# Hernan Fernandez Silva
		HON_HoG_Hernan_Fernandez_Silva_2 = {
			picture = Hernan_Fernandez_Silva_2
			allowed = { tag = HON }
			available = {
				date > 1949.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Hernan_Fernandez_Silva_unavailable }
			}
			
			
			traits = { ideology_D HoG_Corporate_Suit }
		}
	# René Bendana Meza
		HON_HoG_Rene_Bendana_Meza = {
			picture = Rene_Bendana_Meza
			allowed = { tag = HON }
			available = {
				date > 1971.1.1
				date < 1980.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Rene_Bendana_Meza_unavailable }
			}
			
			
			traits = { ideology_D HoG_Naive_Optimist }
		}
	# Jacobo Hernández Cruz
		HON_HoG_Jacobo_Hernandez_Cruz = {
			picture = Jacobo_Hernandez_Cruz
			allowed = { tag = HON }
			available = {
				date > 1990.1.1
				date < 2010.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jacobo_Hernandez_Cruz_unavailable }
			}
			
			
			traits = { ideology_D HoG_Happy_Amateur }
		}
	# Francisco Bertrand
		HON_HoG_Francisco_Bertrand = {
			picture = Francisco_Bertrand
			allowed = { tag = HON }
			available = {
				date > 1913.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Francisco_Bertrand_unavailable }
			}
			
			
			traits = { ideology_D HoG_Old_General }
		}
	# Tiburcio Carías Andino
		HON_HoG_Tiburcio_Carias_Andino = {
			picture = Tiburcio_Carias_Andino
			allowed = { tag = HON }
			available = {
				date > 1924.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Tiburcio_Carias_Andino_unavailable }
			}
			
			
			traits = { ideology_D HoG_Old_General }
		}
	# Rafael López Gutiérrez
		HON_HoG_Rafael_Lopez_Gutierrez = {
			picture = Rafael_Lopez_Gutierrez
			allowed = { tag = HON }
			available = {
				date > 1919.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Rafael_Lopez_Gutierrez_unavailable }
			}
			
			
			traits = { ideology_D HoG_Old_General }
		}
	# Juan B. Valladares
		HON_HoG_Juan_B_Valladares = {
			picture = Juan_B_Valladares
			allowed = { tag = HON }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Juan_B_Valladares_unavailable }
			}
			
			
			traits = { ideology_D HoG_Smiling_Oilman }
		}
	# Julio Lozano Díaz
		HON_HoG_Julio_Lozano_Diaz = {
			picture = Julio_Lozano_Diaz
			allowed = { tag = HON }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Julio_Lozano_Diaz_unavailable }
			}
			
			
			traits = { ideology_D HoG_Ambitious_Union_Boss }
		}
	# Roberto Suazo Córdova
		HON_HoG_Roberto_Suazo_Cordova = {
			picture = Roberto_Suazo_Cordova
			allowed = { tag = HON }
			available = {
				date > 1982.1.1
				date < 2020.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Roberto_Suazo_Cordova_unavailable }
			}
			
			
			traits = { ideology_D HoG_Happy_Amateur }
		}
	# Francisco S. Tapio Toro
		HON_HoG_Francisco_S_Tapio_Toro = {
			picture = Francisco_S_Tapio_Toro
			allowed = { tag = HON }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Francisco_S_Tapio_Toro_unavailable }
			}
			
			
			traits = { ideology_D HoG_Flamboyant_Tough_Guy }
		}
	# Ricardo Zuniga Agustinus
		HON_HoG_Ricardo_Zuniga_Agustinus = {
			picture = Ricardo_Zuniga_Agustinus
			allowed = { tag = HON }
			available = {
				date > 1963.1.1
				date < 1965.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Ricardo_Zuniga_Agustinus_unavailable }
			}
			
			
			traits = { ideology_D HoG_Backroom_Backstabber }
		}
	# Marcelino Ponce Martinez
		HON_HoG_Marcelino_Ponce_Martinez = {
			picture = Marcelino_Ponce_Martinez
			allowed = { tag = HON }
			available = {
				date > 1982.1.1
				date < 1990.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Marcelino_Ponce_Martinez_unavailable }
			}
			
			
			traits = { ideology_D HoG_Happy_Amateur }
		}
	# Alfredo Fortin
		HON_HoG_Alfredo_Fortin = {
			picture = Alfredo_Fortin
			allowed = { tag = HON }
			available = {
				date > 1986.1.1
				date < 2010.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Alfredo_Fortin_unavailable }
			}
			
			
			traits = { ideology_D HoG_Happy_Amateur }
		}
	# Hernan Fernandez Silva
		HON_HoG_Hernan_Fernandez_Silva = {
			picture = Hernan_Fernandez_Silva
			allowed = { tag = HON }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Hernan_Fernandez_Silva_unavailable }
			}
			
			
			traits = { ideology_S HoG_Silent_Workhorse }
		}
	# Juan Codigo Ramirez
		HON_HoG_Juan_Codigo_Ramirez = {
			picture = Juan_Codigo_Ramirez
			allowed = { tag = HON }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Juan_Codigo_Ramirez_unavailable }
			}
			
			
			traits = { ideology_S HoG_Happy_Amateur }
		}
	# José Mejia Arellano
		HON_HoG_Jose_Mejia_Arellano = {
			picture = Jose_Mejia_Arellano
			allowed = { tag = HON }
			available = {
				date > 1957.1.1
				date < 1965.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Jose_Mejia_Arellano_unavailable }
			}
			
			
			traits = { ideology_S HoG_Old_Admiral }
		}
}
#################################################
### Foreign Minister
#################################################
	Foreign_Minister = {
	# Tiburcio Carias Andina
		HON_FM_Tiburcio_Carias_Andina = {
			picture = Tiburcio_Carias_Andina
			allowed = { tag = HON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Tiburcio_Carias_Andina_unavailable }
			}
			
			
			traits = { ideology_A FM_Iron_Fisted_Brute }
		}
	# Antonio Bermúdez Meza
		HON_FM_Antonio_Bermudez_Meza = {
			picture = Antonio_Bermudez_Meza
			allowed = { tag = HON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Antonio_Bermudez_Meza_unavailable }
			}
			
			
			traits = { ideology_A FM_Ideological_Crusader }
		}
	# Rafael L. Callejas Romero
		HON_FM_Rafael_L_Callejas_Romero = {
			picture = Rafael_L_Callejas_Romero
			allowed = { tag = HON }
			available = {
				date > 1985.1.1
				date < 2020.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Rafael_L_Callejas_Romero_unavailable }
			}
			
			
			traits = { ideology_A FM_Great_Compromiser }
		}
	# Laureano Laínez Espinoza
		HON_FM_Laureano_Lainez_Espinoza = {
			picture = Laureano_Lainez_Espinoza
			allowed = { tag = HON }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Laureano_Lainez_Espinoza_unavailable }
			}
			
			
			traits = { ideology_D FM_Great_Compromiser }
		}
	# Julio Lozano Díaz
		HON_FM_Julio_Lozano_Diaz = {
			picture = Julio_Lozano_Diaz
			allowed = { tag = HON }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Julio_Lozano_Diaz_unavailable }
			}
			
			
			traits = { ideology_D FM_Ideological_Crusader }
		}
	# Salvador Aguirre
		HON_FM_Salvador_Aguirre = {
			picture = Salvador_Aguirre
			allowed = { tag = HON }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Salvador_Aguirre_unavailable }
			}
			
			
			traits = { ideology_D FM_The_Cloak_N_Dagger_Schemer }
		}
	# Silverio Laínez Villamin
		HON_FM_Silverio_Lainez_Villamin = {
			picture = Silverio_Lainez_Villamin
			allowed = { tag = HON }
			available = {
				date > 1944.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Silverio_Lainez_Villamin_unavailable }
			}
			
			
			traits = { ideology_D FM_Apologetic_Clerk }
		}
	# Juan Manuel Galvez
		HON_FM_Juan_Manuel_Galvez = {
			picture = Juan_Manuel_Galvez
			allowed = { tag = HON }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Juan_Manuel_Galvez_unavailable }
			}
			
			
			traits = { ideology_D FM_General_Staffer }
		}
	# Jesús Bendaña
		HON_FM_Jesus_Bendana = {
			picture = Jesus_Bendana
			allowed = { tag = HON }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jesus_Bendana_unavailable }
			}
			
			
			traits = { ideology_D FM_Great_Compromiser }
		}
	# Salvador Aguirre Osana
		HON_FM_Salvador_Aguirre_Osana = {
			picture = Salvador_Aguirre_Osana
			allowed = { tag = HON }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Salvador_Aguirre_Osana_unavailable }
			}
			
			
			traits = { ideology_D FM_The_Cloak_N_Dagger_Schemer }
		}
	# Roberto Michelleti Bain
		HON_FM_Roberto_Michelleti_Bain = {
			picture = Roberto_Michelleti_Bain
			allowed = { tag = HON }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Roberto_Michelleti_Bain_unavailable }
			}
			
			
			traits = { ideology_D FM_Great_Compromiser }
		}
	# Hernan Fernandez Silva
		HON_FM_Hernan_Fernandez_Silva = {
			picture = Hernan_Fernandez_Silva
			allowed = { tag = HON }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Hernan_Fernandez_Silva_unavailable }
			}
			
			
			traits = { ideology_D FM_Great_Compromiser }
		}
	# Orlando Rodriguez Jimino
		HON_FM_Orlando_Rodriguez_Jimino = {
			picture = Orlando_Rodriguez_Jimino
			allowed = { tag = HON }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Orlando_Rodriguez_Jimino_unavailable }
			}
			
			
			traits = { ideology_D FM_Ideological_Crusader }
		}
	# Jacinto Escudero Oycandadel
		HON_FM_Jacinto_Escudero_Oycandadel = {
			picture = Jacinto_Escudero_Oycandadel
			allowed = { tag = HON }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Jacinto_Escudero_Oycandadel_unavailable }
			}
			
			
			traits = { ideology_S FM_Iron_Fisted_Brute }
		}
}
#################################################
### Minister of Security
#################################################
	Minister_of_Security = {
	# Carlos Alberto Ucles
		HON_MoS_Carlos_Alberto_Ucles = {
			picture = Carlos_Alberto_Ucles
			allowed = { tag = HON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Carlos_Alberto_Ucles_unavailable }
			}
			
			
			traits = { ideology_A MoS_Silent_Lawyer }
		}
	# Oswaldo López Arellano
		HON_MoS_Oswaldo_Lopez_Arellano = {
			picture = Oswaldo_Lopez_Arellano
			allowed = { tag = HON }
			available = {
				date > 1973.1.1
				date < 2011.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Oswaldo_Lopez_Arellano_unavailable }
			}
			
			
			traits = { ideology_A MoS_Crime_Fighter }
		}
	# Miguel A. Navarro
		HON_MoS_Miguel_A_Navarro = {
			picture = Miguel_A_Navarro
			allowed = { tag = HON }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Miguel_A_Navarro_unavailable }
			}
			
			
			traits = { ideology_D MoS_Crooked_Kleptocrat }
		}
	# Rafael Alvarado Guerrero
		HON_MoS_Rafael_Alvarado_Guerrero = {
			picture = Rafael_Alvarado_Guerrero
			allowed = { tag = HON }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Rafael_Alvarado_Guerrero_unavailable }
			}
			
			
			traits = { ideology_D MoS_Back_Stabber }
		}
	# Abraham Williams Calderon
		HON_MoS_Abraham_Williams_Calderon = {
			picture = Abraham_Williams_Calderon
			allowed = { tag = HON }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Abraham_Williams_Calderon_unavailable }
			}
			
			
			traits = { ideology_D MoS_Prince_Of_Terror }
		}
	# Salvador Aguirre
		HON_MoS_Salvador_Aguirre = {
			picture = Salvador_Aguirre
			allowed = { tag = HON }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Salvador_Aguirre_unavailable }
			}
			
			
			traits = { ideology_D MoS_Crooked_Kleptocrat }
		}
	# Roberto Ortíz Almendares
		HON_MoS_Roberto_Ortiz_Almendares = {
			picture = Roberto_Ortiz_Almendares
			allowed = { tag = HON }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Roberto_Ortiz_Almendares_unavailable }
			}
			
			
			traits = { ideology_D MoS_Compassionate_Gentleman }
		}
	# Augustin Espinoza Reyes
		HON_MoS_Augustin_Espinoza_Reyes = {
			picture = Augustin_Espinoza_Reyes
			allowed = { tag = HON }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Augustin_Espinoza_Reyes_unavailable }
			}
			
			
			traits = { ideology_D MoS_Back_Stabber }
		}
	# Jaime Herda Garcia
		HON_MoS_Jaime_Herda_Garcia = {
			picture = Jaime_Herda_Garcia
			allowed = { tag = HON }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jaime_Herda_Garcia_unavailable }
			}
			
			
			traits = { ideology_D MoS_Efficient_Sociopath }
		}
	# Carlos Molina Dejonje
		HON_MoS_Carlos_Molina_Dejonje = {
			picture = Carlos_Molina_Dejonje
			allowed = { tag = HON }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Carlos_Molina_Dejonje_unavailable }
			}
			
			
			traits = { ideology_D MoS_Man_Of_The_People }
		}
	# Juan Codigo Ramirez
		HON_MoS_Juan_Codigo_Ramirez = {
			picture = Juan_Codigo_Ramirez
			allowed = { tag = HON }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Juan_Codigo_Ramirez_unavailable }
			}
			
			
			traits = { ideology_S MoS_Prince_Of_Terror }
		}
}
#################################################
### Armaments Minister
#################################################
	Armaments_Minister = {
	# Jacobo Hernández Cruz
		HON_AM_Jacobo_Hernandez_Cruz = {
			picture = Jacobo_Hernandez_Cruz
			allowed = { tag = HON }
			available = {
				date > 1990.1.1
				date < 2010.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Jacobo_Hernandez_Cruz_unavailable }
			}
			
			
			traits = { ideology_A AM_Reformer }
		}
	# Roberto Michelleti Bain
		HON_AM_Roberto_Michelleti_Bain = {
			picture = Roberto_Michelleti_Bain
			allowed = { tag = HON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Roberto_Michelleti_Bain_unavailable }
			}
			
			
			traits = { ideology_D AM_Laissez-faire_Capitalist }
		}
	# Miguel A. Navarro
		HON_AM_Miguel_A_Navarro = {
			picture = Miguel_A_Navarro
			allowed = { tag = HON }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Miguel_A_Navarro_unavailable }
			}
			
			
			traits = { ideology_D AM_Infantry_Proponent }
		}
	# Carlos Alberto Ucles
		HON_AM_Carlos_Alberto_Ucles = {
			picture = Carlos_Alberto_Ucles
			allowed = { tag = HON }
			available = {
				date > 1939.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Carlos_Alberto_Ucles_unavailable }
			}
			
			
			traits = { ideology_D AM_Military_Entrepreneur }
		}
	# Rafael Alvarado Guerrero
		HON_AM_Rafael_Alvarado_Guerrero = {
			picture = Rafael_Alvarado_Guerrero
			allowed = { tag = HON }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Rafael_Alvarado_Guerrero_unavailable }
			}
			
			
			traits = { ideology_D AM_Corrupt_Kleptocrat }
		}
	# Didier Masson
		HON_AM_Didier_Masson = {
			picture = Didier_Masson
			allowed = { tag = HON }
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Didier_Masson_unavailable }
			}
			
			
			traits = { ideology_D AM_Air_Superiority_Proponent }
		}
	# Fausto Davilá
		HON_AM_Fausto_Davila = {
			picture = Fausto_Davila
			allowed = { tag = HON }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Fausto_Davila_unavailable }
			}
			
			
			traits = { ideology_D AM_Resource_Industrialist }
		}
	# Roberto Ortíz Almendares
		HON_AM_Roberto_Ortiz_Almendares = {
			picture = Roberto_Ortiz_Almendares
			allowed = { tag = HON }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Roberto_Ortiz_Almendares_unavailable }
			}
			
			
			traits = { ideology_D AM_Administrative_Genius }
		}
	# Juan Manuel Galvez
		HON_AM_Juan_Manuel_Galvez = {
			picture = Juan_Manuel_Galvez
			allowed = { tag = HON }
			available = {
				date > 1947.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Juan_Manuel_Galvez_unavailable }
			}
			
			
			traits = { ideology_D AM_Theoretical_Scientist }
		}
	# Leopoldo Córdova
		HON_AM_Leopoldo_Cordova = {
			picture = Leopoldo_Cordova
			allowed = { tag = HON }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Leopoldo_Cordova_unavailable }
			}
			
			
			traits = { ideology_D AM_Laissez-faire_Capitalist }
		}
	# Roberto Michelleti Bain
		HON_AM_Roberto_Michelleti_Bain_2 = {
			picture = Roberto_Michelleti_Bain_2
			allowed = { tag = HON }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Roberto_Michelleti_Bain_unavailable }
			}
			
			
			traits = { ideology_D AM_Resource_Industrialist }
		}
	# Oswaldo López Arellano
		HON_AM_Oswaldo_Lopez_Arellano = {
			picture = Oswaldo_Lopez_Arellano
			allowed = { tag = HON }
			available = {
				date > 1939.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Oswaldo_Lopez_Arellano_unavailable }
			}
			
			
			traits = { ideology_D AM_Battle_Fleet_Proponent }
		}
	# Juan B. Valladares
		HON_AM_Juan_B_Valladares = {
			picture = Juan_B_Valladares
			allowed = { tag = HON }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Juan_B_Valladares_unavailable }
			}
			
			
			traits = { ideology_D AM_Tank_Proponent }
		}
	# Dr. Ramon Villeda Morales
		HON_AM_Dr_Ramon_Villeda_Morales = {
			picture = Dr_Ramon_Villeda_Morales
			allowed = { tag = HON }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Dr_Ramon_Villeda_Morales_unavailable }
			}
			
			
			traits = { ideology_D AM_Laissez-faire_Capitalist }
		}
	# Misael Becker Villarroel
		HON_AM_Misael_Becker_Villarroel = {
			picture = Misael_Becker_Villarroel
			allowed = { tag = HON }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Misael_Becker_Villarroel_unavailable }
			}
			
			
			traits = { ideology_D AM_Military_Entrepreneur }
		}
	# Carlos Molina Dejonje
		HON_AM_Carlos_Molina_Dejonje = {
			picture = Carlos_Molina_Dejonje
			allowed = { tag = HON }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Carlos_Molina_Dejonje_unavailable }
			}
			
			
			traits = { ideology_D AM_Submarine_Proponent }
		}
	# Jaime Herda Garcia
		HON_AM_Jaime_Herda_Garcia = {
			picture = Jaime_Herda_Garcia
			allowed = { tag = HON }
			available = {
				date > 1946.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jaime_Herda_Garcia_unavailable }
			}
			
			
			traits = { ideology_D AM_Laissez-faire_Capitalist }
		}
	# Augustin Espinoza Reyes
		HON_AM_Augustin_Espinoza_Reyes = {
			picture = Augustin_Espinoza_Reyes
			allowed = { tag = HON }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Augustin_Espinoza_Reyes_unavailable }
			}
			
			
			traits = { ideology_S AM_Administrative_Genius }
		}
}
#################################################
### Head of Intelligence
#################################################
	Head_of_Intelligence = {
	# Raimundo Stochler Garcia
		HON_HoI_Raimundo_Stochler_Garcia = {
			picture = Raimundo_Stochler_Garcia
			allowed = { tag = HON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Raimundo_Stochler_Garcia_unavailable }
			}
			
			
			traits = { ideology_A HoI_Logistics_Specialist }
		}
	# Oswaldo López Arellano
		HON_HoI_Oswaldo_Lopez_Arellano = {
			picture = Oswaldo_Lopez_Arellano
			allowed = { tag = HON }
			available = {
				date > 1973.1.1
				date < 2011.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Oswaldo_Lopez_Arellano_unavailable }
			}
			
			
			traits = { ideology_A HoI_Logistics_Specialist }
		}
	# Roberto Ortíz Almendares
		HON_HoI_Roberto_Ortiz_Almendares = {
			picture = Roberto_Ortiz_Almendares
			allowed = { tag = HON }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Roberto_Ortiz_Almendares_unavailable }
			}
			
			
			traits = { ideology_D HoI_Dismal_Enigma }
		}
	# Tiburcio Carias Andina
		HON_HoI_Tiburcio_Carias_Andina = {
			picture = Tiburcio_Carias_Andina
			allowed = { tag = HON }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Tiburcio_Carias_Andina_unavailable }
			}
			
			
			traits = { ideology_D HoI_Political_Specialist }
		}
	# Vicente Tosta Carrasco
		HON_HoI_Vicente_Tosta_Carrasco = {
			picture = Vicente_Tosta_Carrasco
			allowed = { tag = HON }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Vicente_Tosta_Carrasco_unavailable }
			}
			
			
			traits = { ideology_D HoI_Dismal_Enigma }
		}
	# José Zuñiga Huete
		HON_HoI_Jose_Zuniga_Huete = {
			picture = Jose_Zuniga_Huete
			allowed = { tag = HON }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jose_Zuniga_Huete_unavailable }
			}
			
			
			traits = { ideology_D HoI_Naval_Intelligence_Specialist }
		}
	# Julio Lozano Díaz
		HON_HoI_Julio_Lozano_Diaz = {
			picture = Julio_Lozano_Diaz
			allowed = { tag = HON }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Julio_Lozano_Diaz_unavailable }
			}
			
			
			traits = { ideology_D HoI_Dismal_Enigma }
		}
	# Hernan Fernandez Silva
		HON_HoI_Hernan_Fernandez_Silva = {
			picture = Hernan_Fernandez_Silva
			allowed = { tag = HON }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Hernan_Fernandez_Silva_unavailable }
			}
			
			
			traits = { ideology_D HoI_Industrial_Specialist }
		}
	# Raul Sanchez Banados
		HON_HoI_Raul_Sanchez_Banados = {
			picture = Raul_Sanchez_Banados
			allowed = { tag = HON }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Raul_Sanchez_Banados_unavailable }
			}
			
			
			traits = { ideology_D HoI_Industrial_Specialist }
		}
	# Vicente Tapia Lobo
		HON_HoI_Vicente_Tapia_Lobo = {
			picture = Vicente_Tapia_Lobo
			allowed = { tag = HON }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Vicente_Tapia_Lobo_unavailable }
			}
			
			
			traits = { ideology_S HoI_Naval_Intelligence_Specialist }
		}
	# Enrique Doll Rojas
		HON_HoI_Enrique_Doll_Rojas = {
			picture = Enrique_Doll_Rojas
			allowed = { tag = HON }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Enrique_Doll_Rojas_unavailable }
			}
			
			
			traits = { ideology_S HoI_Technical_Specialist }
		}
}
#################################################
### Chief of Staff
#################################################
	Chief_of_Staff = {
	# Tiburcio Carias Andina
		HON_CoStaff_Tiburcio_Carias_Andina = {
			picture = Tiburcio_Carias_Andina
			allowed = { tag = HON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Tiburcio_Carias_Andina_unavailable }
			}
			
			
			traits = { ideology_A CoStaff_School_Of_Psychology }
		}
	# Policarpo Paz Garcia
		HON_CoStaff_Policarpo_Paz_Garcia = {
			picture = Policarpo_Paz_Garcia
			allowed = { tag = HON }
			available = {
				date > 1978.1.1
				date < 2001.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Policarpo_Paz_Garcia_unavailable }
			}
			
			
			traits = { ideology_A CoStaff_School_Of_Defence }
		}
	# Juan Manuel Galvez
		HON_CoStaff_Juan_Manuel_Galvez = {
			picture = Juan_Manuel_Galvez
			allowed = { tag = HON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Juan_Manuel_Galvez_unavailable }
			}
			
			
			traits = { ideology_D CoStaff_School_Of_Manoeuvre }
		}
	# Raimundo Stochler Garcia
		HON_CoStaff_Raimundo_Stochler_Garcia = {
			picture = Raimundo_Stochler_Garcia
			allowed = { tag = HON }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Raimundo_Stochler_Garcia_unavailable }
			}
			
			
			traits = { ideology_D CoStaff_School_Of_Defence }
		}
	# Santiago Meza Calix
		HON_CoStaff_Santiago_Meza_Calix = {
			picture = Santiago_Meza_Calix
			allowed = { tag = HON }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Santiago_Meza_Calix_unavailable }
			}
			
			
			traits = { ideology_D CoStaff_School_Of_Defence }
		}
	# Oswaldo López Arellano
		HON_CoStaff_Oswaldo_Lopez_Arellano = {
			picture = Oswaldo_Lopez_Arellano
			allowed = { tag = HON }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Oswaldo_Lopez_Arellano_unavailable }
			}
			
			
			traits = { ideology_D CoStaff_School_Of_Manoeuvre }
		}
	# Enrique Ortiz Pinel
		HON_CoStaff_Enrique_Ortiz_Pinel = {
			picture = Enrique_Ortiz_Pinel
			allowed = { tag = HON }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Enrique_Ortiz_Pinel_unavailable }
			}
			
			
			traits = { ideology_D CoStaff_School_Of_Fire_Support }
		}
	# Enrique Doll Rojas
		HON_CoStaff_Enrique_Doll_Rojas = {
			picture = Enrique_Doll_Rojas
			allowed = { tag = HON }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Enrique_Doll_Rojas_unavailable }
			}
			
			
			traits = { ideology_S CoStaff_School_Of_Psychology }
		}
	# Vicente Tapia Lobo
		HON_CoStaff_Vicente_Tapia_Lobo = {
			picture = Vicente_Tapia_Lobo
			allowed = { tag = HON }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Vicente_Tapia_Lobo_unavailable }
			}
			
			
			traits = { ideology_S CoStaff_School_Of_Manoeuvre }
		}
}
#################################################
### Chief of Army
#################################################
	Chief_of_Army = {
	# Raimundo Stochler Garcia
		HON_CoArmy_Raimundo_Stochler_Garcia = {
			picture = Raimundo_Stochler_Garcia
			allowed = { tag = HON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Raimundo_Stochler_Garcia_unavailable }
			}
			
			
			traits = { ideology_A CoArmy_Static_Defence_Doctrine }
		}
	# Policarpo Paz Garcia
		HON_CoArmy_Policarpo_Paz_Garcia = {
			picture = Policarpo_Paz_Garcia
			allowed = { tag = HON }
			available = {
				date > 1978.1.1
				date < 2001.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Policarpo_Paz_Garcia_unavailable }
			}
			
			
			traits = { ideology_A CoArmy_Elastic_Defence_Doctrine }
		}
	# Juan Manuel Galvez
		HON_CoArmy_Juan_Manuel_Galvez = {
			picture = Juan_Manuel_Galvez
			allowed = { tag = HON }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Juan_Manuel_Galvez_unavailable }
			}
			
			
			traits = { ideology_D CoArmy_Armoured_Spearhead_Doctrine }
		}
	# Vicente Tosta Carrasco
		HON_CoArmy_Vicente_Tosta_Carrasco = {
			picture = Vicente_Tosta_Carrasco
			allowed = { tag = HON }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Vicente_Tosta_Carrasco_unavailable }
			}
			
			
			traits = { ideology_D CoArmy_Armoured_Spearhead_Doctrine }
		}
	# José Zuñiga Huete
		HON_CoArmy_Jose_Zuniga_Huete = {
			picture = Jose_Zuniga_Huete
			allowed = { tag = HON }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jose_Zuniga_Huete_unavailable }
			}
			
			
			traits = { ideology_D CoArmy_Decisive_Battle_Doctrine }
		}
	# Enrique Ortiz Pinel
		HON_CoArmy_Enrique_Ortiz_Pinel = {
			picture = Enrique_Ortiz_Pinel
			allowed = { tag = HON }
			available = {
				date > 1941.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Enrique_Ortiz_Pinel_unavailable }
			}
			
			
			traits = { ideology_D CoArmy_Armoured_Spearhead_Doctrine }
		}
	# Francisco S. Tapio Toro
		HON_CoArmy_Francisco_S_Tapio_Toro = {
			picture = Francisco_S_Tapio_Toro
			allowed = { tag = HON }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Francisco_S_Tapio_Toro_unavailable }
			}
			
			
			traits = { ideology_D CoArmy_Armoured_Spearhead_Doctrine }
		}
	# Enrique Doll Rojas
		HON_CoArmy_Enrique_Doll_Rojas = {
			picture = Enrique_Doll_Rojas
			allowed = { tag = HON }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Enrique_Doll_Rojas_unavailable }
			}
			
			
			traits = { ideology_S CoArmy_Static_Defence_Doctrine }
		}
}
#################################################
### Chief of Navy
#################################################
	Chief_of_Navy = {
	# Policarpo Paz Garcia
		HON_CoNavy_Policarpo_Paz_Garcia = {
			picture = Policarpo_Paz_Garcia
			allowed = { tag = HON }
			available = {
				date > 1978.1.1
				date < 2001.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Policarpo_Paz_Garcia_unavailable }
			}
			
			
			traits = { ideology_A CoNavy_Base_Control_Doctrine }
		}
	# Carlos Vaupel Comeza
		HON_CoNavy_Carlos_Vaupel_Comeza = {
			picture = Carlos_Vaupel_Comeza
			allowed = { tag = HON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Carlos_Vaupel_Comeza_unavailable }
			}
			
			
			traits = { ideology_D CoNavy_Indirect_Approach_Doctrine }
		}
	# Carlos Vaupel Comeza
		HON_CoNavy_Carlos_Vaupel_Comeza_2 = {
			picture = Carlos_Vaupel_Comeza_2
			allowed = { tag = HON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Carlos_Vaupel_Comeza_unavailable }
			}
			
			
			traits = { ideology_D CoNavy_Indirect_Approach_Doctrine }
		}
	# Santiago Meza Calix
		HON_CoNavy_Santiago_Meza_Calix = {
			picture = Santiago_Meza_Calix
			allowed = { tag = HON }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Santiago_Meza_Calix_unavailable }
			}
			
			
			traits = { ideology_D CoNavy_Indirect_Approach_Doctrine }
		}
	# Oswaldo López Arellano
		HON_CoNavy_Oswaldo_Lopez_Arellano = {
			picture = Oswaldo_Lopez_Arellano
			allowed = { tag = HON }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Oswaldo_Lopez_Arellano_unavailable }
			}
			
			
			traits = { ideology_D CoNavy_Decisive_Naval_Battle_Doctrine }
		}
	# Francisco S. Tapio Toro
		HON_CoNavy_Francisco_S_Tapio_Toro = {
			picture = Francisco_S_Tapio_Toro
			allowed = { tag = HON }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Francisco_S_Tapio_Toro_unavailable }
			}
			
			
			traits = { ideology_D CoNavy_Base_Control_Doctrine }
		}
	# Carlos Vaupel Comeza
		HON_CoNavy_Carlos_Vaupel_Comeza_3 = {
			picture = Carlos_Vaupel_Comeza_3
			allowed = { tag = HON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Carlos_Vaupel_Comeza_unavailable }
			}
			
			
			traits = { ideology_S CoNavy_Indirect_Approach_Doctrine }
		}
	# Vicente Tapia Lobo
		HON_CoNavy_Vicente_Tapia_Lobo = {
			picture = Vicente_Tapia_Lobo
			allowed = { tag = HON }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				S_Minister_Allowed = yes
				NOT = { has_country_flag = Vicente_Tapia_Lobo_unavailable }
			}
			
			
			traits = { ideology_S CoNavy_Decisive_Naval_Battle_Doctrine }
		}
}
#################################################
### Chief of Airforce
#################################################
	Chief_of_Airforce = {
	# Policarpo Paz Garcia
		HON_CoAir_Policarpo_Paz_Garcia = {
			picture = Policarpo_Paz_Garcia
			allowed = { tag = HON }
			available = {
				date > 1978.1.1
				date < 2001.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Policarpo_Paz_Garcia_unavailable }
			}
			
			
			traits = { ideology_A CoAir_Army_Aviation_Doctrine }
		}
	# Gustavo Schmidt
		HON_CoAir_Gustavo_Schmidt = {
			picture = Gustavo_Schmidt
			allowed = { tag = HON }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Gustavo_Schmidt_unavailable }
			}
			
			
			traits = { ideology_D CoAir_Air_Superiority_Doctrine }
		}
	# Didier Masson
		HON_CoAir_Didier_Masson = {
			picture = Didier_Masson
			allowed = { tag = HON }
			available = {
				date > 1937.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Didier_Masson_unavailable }
			}
			
			
			traits = { ideology_D CoAir_Air_Superiority_Doctrine }
		}
	# Miguel Ángel Sevilla
		HON_CoAir_Miguel_Angel_Sevilla = {
			picture = Miguel_Angel_Sevilla
			allowed = { tag = HON }
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Miguel_Angel_Sevilla_unavailable }
			}
			
			
			traits = { ideology_D CoAir_Naval_Aviation_Doctrine }
		}
	}
}
