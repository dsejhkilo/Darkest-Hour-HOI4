ideas = {
#########################################################################
#  National Spirits
#########################################################################
	country = {
		#### Aftermath of the Ryutin Affair
		SOV_Aftermath_of_the_Ryutin_Affair = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			picture = SOV_Ryutin_Affair

			modifier = {
				stability_factor = -0.075
				political_power_gain = -0.15
			}
		}
		#### The Great Soviet Famine
		SOV_The_Great_Soviet_Famine = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			picture = SOV_Soviet_Famine

			modifier = {
				stability_weekly = -0.002
				MONTHLY_POPULATION = -0.20
			}
		}
		#### Deficient Economy.
		SOV_Deficient_Economy = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			picture = chi_hyper_inflation

			modifier = {
				stability_factor = -0.05
				consumer_goods_factor = -0.05
				research_speed_factor = -0.05
				production_speed_industrial_complex_factor = -0.10
				production_speed_arms_factory_factor = 0.10
				trade_opinion_factor = -0.25
			}
		}
		#### Final Phase of the Decossackization
		SOV_Final_Phase_of_the_Decossackization = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			picture = SOV_Decossackization

			modifier = {
				conscription_factor = -0.05
				political_power_gain = 0.25
				war_support_factor = -0.05
			}
		}

		SOV_Collectivization_Process = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = ast_all_in

			modifier = {
				production_speed_industrial_complex_factor = -0.10
				production_speed_arms_factory_factor = 0.10
				consumer_goods_factor = 0.05
				war_support_factor = -0.10
				stability_factor = -0.10
			}
		}
		
		#### Soviet Collectivization
		SOV_Soviet_Collectivization = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			picture = SOV_Soviet_Collectivization

			modifier = {
				MONTHLY_POPULATION = -0.20
				conscription_factor = -0.10
				war_support_factor = -0.1
				stability_factor = -0.1
				production_speed_buildings_factor = -0.15
				money_income_factor = -0.1
				industrial_capacity_factory = -0.05
				industrial_capacity_dockyard = -0.05
			}
		}
		#### Soviet Collectivization - church widespread raids
		SOV_Soviet_CollectivizationC1 = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			#picture = SOV_Decossackization

			modifier = {
				political_power_gain = 0.05
				communist_drift = 0.2
				MONTHLY_POPULATION = -0.20
				conscription_factor = -0.10
				war_support_factor = -0.05
				stability_factor = -0.1
				production_speed_buildings_factor = -0.10
				money_income_factor = -0.1
				industrial_capacity_factory = -0.05
				industrial_capacity_dockyard = -0.05
			}
		}
		#### Soviet Collectivization - church secret slow
		SOV_Soviet_CollectivizationC2 = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			#picture = SOV_Decossackization

			modifier = {
				political_power_gain = 0.03
				MONTHLY_POPULATION = -0.20
				communist_drift = 0.1
				conscription_factor = -0.10
				war_support_factor = -0.07
				stability_factor = -0.1
				production_speed_buildings_factor = -0.12
				money_income_factor = -0.1
				industrial_capacity_factory = -0.05
				industrial_capacity_dockyard = -0.05
			}
		}
		#### Soviet Collectivization - working class invigorate
		SOV_Soviet_CollectivizationI1 = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			#picture = SOV_Decossackization

			modifier = {
				political_power_gain = 0.05
				communist_drift = 0.2
				MONTHLY_POPULATION = -0.20
				conscription_factor = -0.10
				war_support_factor = -0.05
				stability_factor = -0.1
				production_speed_buildings_factor = -0.05
				money_income_factor = -0.1
				industrial_capacity_factory = -0.03
				industrial_capacity_dockyard = -0.03
			}
		}
		SOV_Soviet_CollectivizationI2 = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			#picture = SOV_Decossackization

			modifier = {
				political_power_gain = 0.03
				MONTHLY_POPULATION = -0.20
				communist_drift = 0.1
				conscription_factor = -0.10
				war_support_factor = -0.07
				stability_factor = -0.1
				production_speed_buildings_factor = -0.07
				money_income_factor = -0.1
				industrial_capacity_factory = -0.03
				industrial_capacity_dockyard = -0.03
			}
		}
		#### Soviet Collectivization - Production consumer
		SOV_Soviet_CollectivizationP1 = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			#picture = SOV_Decossackization

			modifier = {
				production_factory_max_efficiency_factor = 0.05
				consumer_goods_factor = 0.05
				political_power_gain = 0.05
				communist_drift = 0.2
				MONTHLY_POPULATION = -0.20
				conscription_factor = -0.10
				war_support_factor = -0.05
				stability_factor = -0.1
				production_speed_buildings_factor = -0.05
				money_income_factor = -0.1
				industrial_capacity_factory = -0.03
				industrial_capacity_dockyard = -0.03
			}
		}
		SOV_Soviet_CollectivizationP2 = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			#picture = SOV_Decossackization

			modifier = {
				production_factory_max_efficiency_factor = 0.05
				consumer_goods_factor = 0.05
				political_power_gain = 0.03
				MONTHLY_POPULATION = -0.20
				communist_drift = 0.1
				conscription_factor = -0.10
				war_support_factor = -0.07
				stability_factor = -0.1
				production_speed_buildings_factor = -0.07
				money_income_factor = -0.1
				industrial_capacity_factory = -0.03
				industrial_capacity_dockyard = -0.03
			}
		}
		#### Soviet Collectivization - Tolkachi
		SOV_Soviet_CollectivizationT1 = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			#picture = SOV_Decossackization

			modifier = {
				production_factory_max_efficiency_factor = 0.05
				consumer_goods_factor = 0.05
				political_power_gain = 0.1
				communist_drift = 0.2
				MONTHLY_POPULATION = -0.20
				conscription_factor = -0.10
				war_support_factor = -0.05
				stability_factor = -0.13
				production_speed_buildings_factor = -0.05
				money_income_factor = -0.1
				industrial_capacity_dockyard = -0.03
			}
		}
		SOV_Soviet_CollectivizationT2 = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			#picture = SOV_Decossackization

			modifier = {
				production_factory_max_efficiency_factor = 0.05
				consumer_goods_factor = 0.05
				political_power_gain = 0.08
				MONTHLY_POPULATION = -0.20
				communist_drift = 0.1
				conscription_factor = -0.10
				war_support_factor = -0.07
				stability_factor = -0.13
				production_speed_buildings_factor = -0.07
				money_income_factor = -0.1
				industrial_capacity_dockyard = -0.03
			}
		}

		#Ultimate Loyalty to the Motherland
		SOV_Loyalty_to_the_Motherland = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			#picture = SOV_Decossackization

			modifier = {
				army_core_defence_factor = 0.05
				war_support_factor = 0.05
				MONTHLY_POPULATION = 0.05
			}
		}
		SOV_Loyalty_to_the_Motherland2 = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			#picture = SOV_Decossackization

			modifier = {
				army_core_defence_factor = 0.08
				war_support_factor = 0.05
				stability_factor = 0.05
				MONTHLY_POPULATION = 0.05
				money_income_factor = 0.1
				hidden_modifier = { 
					money_income_factor_from_ideas = 0.1
				}
			}
		}
		########################################
		#### High Agricultural Employment
		SOV_High_Agricultural_Employment = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			picture = SOV_Soviet_Collectivization

			modifier = {
				global_building_slots_factor = -0.30
			}
		}
		
		#### Soviet Collectivization modifiers
		SOV_Soviet_Collectivization1 = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			#picture = SOV_Decossackization

			modifier = {
				political_power_gain = 0.1
				MONTHLY_POPULATION = -0.20
				conscription_factor = -0.15
				war_support_factor = -0.05
				stability_factor = -0.1
				production_speed_buildings_factor = -0.1
				money_income_factor = -0.1
			}
		}
		SOV_Soviet_Collectivization2 = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			#picture = SOV_Decossackization

			modifier = {
				political_power_gain = 0.1
				MONTHLY_POPULATION = -0.10
				conscription_factor = -0.15
				stability_factor = -0.05
				production_speed_buildings_factor = -0.1
				money_income_factor = -0.1
			}
		}
		SOV_Soviet_Collectivization3 = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			#picture = SOV_Decossackization

			modifier = {
				political_power_gain = 0.1
				conscription_factor = -0.15
				production_speed_buildings_factor = -0.1
				money_income_factor = -0.1
			}
		}
		
		#### High Agricultural Employment modifiers
		SOV_High_Agricultural_Employment1 = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			#picture = SOV_Decossackization

			modifier = {
				global_building_slots_factor = -0.25
			}
		}
		
		#### Industrial Ethos
		SOV_Industrial_Ethos = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			#picture = generic_production_bonus

			modifier = {
				industrial_capacity_factory = 0.05
				industrial_capacity_dockyard = 0.05
			}
		}
		
		SOV_Industrial_Ethos1 = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			#picture = generic_production_bonus

			modifier = {
				industrial_capacity_factory = 0.1
				industrial_capacity_dockyard = 0.1
				production_factory_efficiency_gain_factor = -0.07
				line_change_production_efficiency_factor = -0.05
			}
		}
		
		SOV_Industrial_Ethos2 = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			#picture = generic_production_bonus

			modifier = {
			    industrial_capacity_factory = 0.1
				industrial_capacity_dockyard = 0.1
				production_factory_efficiency_gain_factor = -0.07
				line_change_production_efficiency_factor = -0.05
				production_speed_arms_factory_factor = 0.05
			}

			research_bonus = {
				industry = 0.10
				infantry_weapons = 0.10
			}
		}


		SOV_Patronage_Networks = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = generic_communism_drift_bonus

			modifier = {
				political_power_gain = -0.15
				stability_factor = -0.10
			}
		}

		SOV_Ethnic_Nationalism = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = generic_morale_bonus

			modifier = {
				stability_factor = -0.10
				political_power_gain = -0.20
			}
		}

		SOV_Ethnic_Nationalism_1 = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = generic_morale_bonus

			modifier = {
				stability_factor = -0.10
			}
		}
	#### Production Quotas
		SOV_Production_Quotas = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			picture = generic_production_bonus

			modifier = {
				local_resources_factor = -0.15
				production_factory_max_efficiency_factor = -0.10
				production_speed_infrastructure_factor = 0.15
			}
		}

		SOV_Production_Quotas_1 = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			picture = generic_goods_red_bonus

			modifier = {
				local_resources_factor = -0.08
				production_speed_infrastructure_factor = 0.15
			}
		}

		SOV_Production_Quotas_2 = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			picture = generic_goods_red_bonus

			modifier = {
				local_resources_factor = -0.08
				production_speed_infrastructure_factor = 0.15
				war_support_factor = 0.10
			}
		}

		SOV_Production_Quotas_3 = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			picture = generic_goods_red_bonus

			modifier = {
				local_resources_factor = -0.08
				production_speed_infrastructure_factor = 0.15
				production_speed_buildings_factor = 0.05
				war_support_factor = 0.10
				stability_factor = 0.05
				consumer_goods_factor = 0.05
			}
		}

		SOV_Stakhanovite_Movement = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = generic_production_bonus

			modifier = {
				industrial_capacity_factory = 0.05
				production_factory_efficiency_gain_factor = 0.05
				local_resources_factor = 0.10
			}
		}
	#### Repurposed Guns
		SOV_repurpose_guns = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = generic_artillery_regiments

			equipment_bonus = {
				artillery_equipment = {
					build_cost_ic = -0.10 instant = yes
				}
			}
		}
	#### Repurposed Hulls
		SOV_repurpose_hulls = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = generic_coastal_defense_ships2

			equipment_bonus = {
				CA_equipment = {
					build_cost_ic = -0.05 instant = yes
				}
				CL_equipment = {
					build_cost_ic = -0.05 instant = yes
				}
				DD_equipment = {
					build_cost_ic = -0.10 instant = yes
				}
			}
		}
	#### Unstable Democracy
		SOV_Unstable_Democracy = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			picture = GENERIC_Unstable_Democracy
			modifier = {
				stability_factor = -0.12
				political_power_gain = -0.2
			}
		}
	#### Soviet Democracy
		SOV_Soviet_Democracy = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			picture = generic_democratic_drift_bonus
			modifier = {
				stability_factor = 0.10
				political_power_gain = -0.1
			}
		}
	#### New Economic Policy
		SOV_New_Economic_Policy = {
			picture = SOV_NEP
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			modifier = {
				MONTHLY_POPULATION = 1.07
				political_power_gain = 0.20
				consumer_goods_factor = 0.05
				research_speed_factor = -0.05
			}
		}

		SOV_positive_heroism = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = generic_license_production

			modifier = {
				war_support_factor = 0.10
				experience_gain_army_factor = 0.20
			}
		}

		SOV_militarized_schools = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = mex_politicised_army_2

			modifier = {
				experience_gain_army_factor = 0.05
				conscription_factor = 0.05
			}

			research_bonus = {
				land_doctrine = 0.05
			}
		}

		SOV_workers_culture = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = production_bonus

			modifier = {
				production_speed_arms_factory_factor = 0.1
				production_speed_dockyard_factor = 0.1
				production_speed_naval_base_factor = 0.1
				production_speed_coastal_bunker_factor = 0.1
				production_speed_bunker_factor = 0.1
				production_speed_air_base_factor = 0.1
				production_speed_anti_air_building_factor = 0.1
				production_speed_radar_station_factor = 0.1
				production_speed_synthetic_refinery_factor = 0.1
			}
		}

		mvd = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = generic_communism_drift_bonus

			modifier = {
				#stuff
			}
		}

		SOV_mass_mechanization = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = FRA_motorized_focus

			equipment_bonus = {
				motorized_equipment = {
					build_cost_ic = -0.1
					instant = yes
				}
				APC_equipment = {
					build_cost_ic = -0.1
					instant = yes
				}
				IFV_equipment = {
					build_cost_ic = -0.1
					instant = yes
				}
				heavy_tank_equipment = {
					build_cost_ic = -0.1
					instant = yes
				}
				light_armor_equipment = {
					build_cost_ic = -0.1
					instant = yes
				}
				armor_equipment = {
					build_cost_ic = -0.1
					instant = yes
				}
			}
		}

		SOV_red_guards = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = generic_communist_army

			modifier = {
				training_time_army_factor = -0.15
				army_org_factor = -0.15
			}
		}

		SOV_comecon_group = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = home_of_the_revolution

			modifier = {
				consumer_goods_factor = -0.05
				political_power_gain = -0.10
			}
		}

		SOV_Promote_Kolkoz_farms_1 = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = GENERIC_Agrarian_Society

			modifier = {
				production_speed_arms_factory_factor = -0.10
				consumer_goods_factor = -0.04
				war_support_factor = -0.10
			}
		}

		SOV_Promote_Kolkoz_farms_2 = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = GENERIC_Agrarian_Society

			modifier = {
				production_speed_arms_factory_factor = -0.10
				consumer_goods_factor = 0.08
				war_support_factor = -0.10
				stability_factor = 0.05
				production_speed_industrial_complex_factor = 0.10
				conscription_factor = 0.08
			}
		}

		SOV_Propagate_an_Industrial_Ethos_idea = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = generic_production_bonus

			modifier = {
				industrial_capacity_factory = 0.05
				political_power_gain = 0.10
			}
		}

		SOV_Focalize_on_Sovkhoz_Farms_1 = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = GENERIC_Agrarian_Society_2

			modifier = {
				production_speed_arms_factory_factor = 0.05
				consumer_goods_factor = 0.08
				political_power_gain = 0.05
			}
		}

		SOV_Focalize_on_Sovkhoz_Farms_2 = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = GENERIC_Agrarian_Society_2

			modifier = {
				production_speed_arms_factory_factor = 0.10
				production_speed_industrial_complex_factor = 0.05
				consumer_goods_factor = -0.05
				political_power_gain = 0.10
				local_resources_factor = 0.05
			}
		}

		SOV_Focalize_on_Sovkhoz_Farms_3 = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = GENERIC_Agrarian_Society_2

			modifier = {
				production_speed_arms_factory_factor = 0.10
				production_speed_industrial_complex_factor = 0.05
				consumer_goods_factor = -0.05
				political_power_gain = 0.10
				local_resources_factor = 0.05
				research_speed_factor = -0.02
			}
		}

		SOV_Pragmatic_Command_Economy_idea = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = chi_hyper_inflation2

			modifier = {
				production_speed_buildings_factor = 0.08
				research_speed_factor = 0.1
				local_resources_factor = 0.05
				political_power_gain = 0.10
				democratic_acceptance = -20
			}
		}

		SOV_Liquidate_The_Church_idea = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = GENERIC_Crisis

			modifier = {
				stability_factor = -0.10
				war_support_factor = -0.10
				political_power_gain = 0.75
				industrial_capacity_factory = 0.05
			}
		}

		SOV_Introduce_Childcare_idea = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = hol_the_foundations_of_defense

			modifier = {
				industrial_capacity_factory = 0.05
				consumer_goods_factor = -0.05
			}
		}

		SOV_Reverse_Korenizatsiya_idea = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = jap_the_unthinkable_option

			modifier = {
				stability_factor = -0.05
				political_power_gain = -0.10
			}
		}

		SOV_Stamp_down_Tolkachi_on_Industries_idea = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = eng_tackle_capitalism

			modifier = {
				political_power_gain = -0.05
				stability_factor = -0.05
			}
		}

		SOV_Design_Contemporary_Equipment_Standards_idea = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = SOV_scientist_defect

			modifier = {
				production_speed_buildings_factor = 0.08
				research_speed_factor = 0.1
				local_resources_factor = 0.05
				political_power_gain = 0.10
				democratic_acceptance = -20
				production_speed_arms_factory_factor = 0.05
			}

			research_bonus = {
				industry = 0.10
				infantry_weapons = 0.10
			}
		}

		SOV_Adopt_New_Russificaion_Policies_idea = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = generic_communism_drift_bonus

			modifier = {
				war_support_factor = 0.05
				political_power_gain = 0.20
				research_speed_factor = -0.02
				stability_factor = -0.08
			}
		}

		SOV_Congress_Policies_on_Improvement = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = jap_duplicate_research

			modifier = {
			}
		}

		SOV_NKVD_1 = {
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			allowed_civil_war = {
				has_government = communist
			}
			
			modifier = {
				subversive_activites_upkeep = -0.5
			}
		}

		SOV_NKVD_2 = {
			removal_cost = -1
			
			allowed = {
				always = no # Added via focus
			}

			allowed_civil_war = {
				has_government = communist
			}
			
			modifier = {
				subversive_activites_upkeep = -0.5
				land_reinforce_rate = 0.05
				stability_factor = -0.10
			}
		}

		SOV_Congress_Disjointed_Politburo = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = generic_communism_drift_bonus

			modifier = {
			}
		}

		SOV_Congress_End_Rabkrin_System = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = generic_communism_drift_bonus

			modifier = {
				stability_weekly = 0.005
			}
		}

		SOV_Congress_Reform_Rabkrin_System = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = generic_communism_drift_bonus

			modifier = {
				political_power_gain = 0.5
			}
		}
		
		SOV_Reign_of_Terror = {
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			
			removal_cost = -1
			
			#picture = 
			
			modifier = {
				stability_factor = 0.1
				political_power_factor = -0.5
			}
		}
		
		SOV_Active_Opposition = {
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			
			removal_cost = -1
			
			#picture = 
			
			modifier = {
			}
		}
		
		SOV_Socialism_Achieved = {
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			
			removal_cost = -1
			
			#picture = 
			
			modifier = {
				stability_factor = 0.1
				political_power_factor = 0.25
			}
		}
		
		SOV_Gulag_Based_Economy_Projects = {
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			
			removal_cost = -1
			
			#picture = 
			
			modifier = {
				stability_factor = -0.06
				production_speed_buildings_factor = 0.1
			}
		}
		
		SOV_Zhdanov_Doctrine = {
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			
			removal_cost = -1
			
			#picture = 
			
			modifier = {
			}
		}
		
		SOV_Stalin_Mass_Repression = {
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			
			removal_cost = -1
			
			#picture = 
			
			modifier = {
			}
		}
		
		SOV_Voroshiloctchina = {
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			
			removal_cost = -1
			
			#picture = 
			
			modifier = {
			political_power_factor = 0.15
			training_time_army_factor = -0.1
			army_org_factor = -0.25
			experience_gain_army_factor = -0.25
			command_power_gain_mult = -0.25
			}
		}

		SOV_NEP = {
			removal_cost = -1
			
			picture = generic_communism_drift_bonus

			allowed = {
				always = no # Added via focus
			}

			allowed_civil_war = {
				has_government = communist
			}
			
			modifier = {
				stability_factor = 0.10
				research_speed_factor = -0.05
				consumer_goods_factor = -0.05
			}
		}		
	}
}
