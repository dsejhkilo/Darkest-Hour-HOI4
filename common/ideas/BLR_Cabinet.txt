ideas = {
#################################################
### Head of Government
#################################################
	Head_of_Government = {
	# Vasil Zacharka
		BLR_HoG_Vasil_Zacharka = {
			picture = Vasil_Zacharka
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Vasil_Zacharka_unavailable }
			}
			
			
			traits = { ideology_F HoG_Corporate_Suit }
		}
	# Wiaczaslau Adamowicz
		BLR_HoG_Wiaczaslau_Adamowicz = {
			picture = Wiaczaslau_Adamowicz
			allowed = { tag = BLR }
			available = {
				date > 1919.1.1
				date < 1933.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Wiaczaslau_Adamowicz_unavailable }
			}
			
			
			traits = { ideology_A HoG_Naive_Optimist }
		}
	# Venedict Bobkovski
		BLR_HoG_Venedict_Bobkovski = {
			picture = Venedict_Bobkovski
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Venedict_Bobkovski_unavailable }
			}
			
			
			traits = { ideology_D HoG_Political_Protege }
		}
	# Vikentsiy Zhuk-Hryshkyevich
		BLR_HoG_Vikentsiy_ZhukHryshkyevich = {
			picture = Vikentsiy_ZhukHryshkyevich
			allowed = { tag = BLR }
			available = {
				date > 1970.1.1
				date < 1990.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Vikentsiy_ZhukHryshkyevich_unavailable }
			}
			
			
			traits = { ideology_D HoG_Smiling_Oilman }
		}
	# Jazep Sazhych
		BLR_HoG_Jazep_Sazhych = {
			picture = Jazep_Sazhych
			allowed = { tag = BLR }
			available = {
				date > 1982.1.1
				date < 2008.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jazep_Sazhych_unavailable }
			}
			
			
			traits = { ideology_D HoG_Old_General }
		}
	# Anton Luckiewicz
		BLR_HoG_Anton_Luckiewicz = {
			picture = Anton_Luckiewicz
			allowed = { tag = BLR }
			available = {
				date > 1914.1.1
				date < 1943.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Anton_Luckiewicz_unavailable }
			}
			
			
			traits = { ideology_D HoG_Naive_Optimist }
		}
	# Waclaw Lastouski
		BLR_HoG_Waclaw_Lastouski = {
			picture = Waclaw_Lastouski
			allowed = { tag = BLR }
			available = {
				date > 1917.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Waclaw_Lastouski_unavailable }
			}
			
			
			traits = { ideology_D HoG_Naive_Optimist }
		}
	# Mikhail Chigir
		BLR_HoG_Mikhail_Chigir = {
			picture = Mikhail_Chigir
			allowed = { tag = BLR }
			available = {
				date > 1985.1.1
				date < 2013.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mikhail_Chigir_unavailable }
			}
			
			
			traits = { ideology_D HoG_Corporate_Suit }
		}
	# Pyotr Krecheuski
		BLR_HoG_Pyotr_Krecheuski = {
			picture = Pyotr_Krecheuski
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Pyotr_Krecheuski_unavailable }
			}
			
			
			traits = { ideology_D HoG_Happy_Amateur }
		}
	# Vyachaslau Kebich
		BLR_HoG_Vyachaslau_Kebich = {
			picture = Vyachaslau_Kebich
			allowed = { tag = BLR }
			available = {
				date > 1990.1.1
				date < 2020.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Vyachaslau_Kebich_unavailable }
			}
			
			
			traits = { ideology_D HoG_Old_Airmarshal }
		}
	# Aleksiy A. Volkov
		BLR_HoG_Aleksiy_A_Volkov = {
			picture = Aleksiy_A_Volkov
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Aleksiy_A_Volkov_unavailable }
			}
			
			
			traits = { ideology_C HoG_Naive_Optimist }
		}
	# Nikolay S. Patolichev
		BLR_HoG_Nikolay_S_Patolichev_2 = {
			picture = Nikolay_S_Patolichev_2
			allowed = { tag = BLR }
			available = {
				date > 1953.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Nikolay_S_Patolichev_unavailable }
			}
			
			
			traits = { ideology_C HoG_Backroom_Backstabber }
		}
	# Kirill T. Mazurov
		BLR_HoG_Kirill_T_Mazurov = {
			picture = Kirill_T_Mazurov
			allowed = { tag = BLR }
			available = {
				date > 1956.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Kirill_T_Mazurov_unavailable }
			}
			
			
			traits = { ideology_C HoG_Happy_Amateur }
		}
	# Tikhon Kiselyov
		BLR_HoG_Tikhon_Kiselyov = {
			picture = Tikhon_Kiselyov
			allowed = { tag = BLR }
			available = {
				date > 1965.1.1
				date < 1984.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Tikhon_Kiselyov_unavailable }
			}
			
			
			traits = { ideology_C HoG_Old_General }
		}
	# Aleksandr Aksyonov
		BLR_HoG_Aleksandr_Aksyonov = {
			picture = Aleksandr_Aksyonov
			allowed = { tag = BLR }
			available = {
				date > 1978.1.1
				date < 2010.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Aleksandr_Aksyonov_unavailable }
			}
			
			
			traits = { ideology_C HoG_Old_General }
		}
	# Vladimir Brovikov
		BLR_HoG_Vladimir_Brovikov = {
			picture = Vladimir_Brovikov
			allowed = { tag = BLR }
			available = {
				date > 1983.1.1
				date < 2020.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Vladimir_Brovikov_unavailable }
			}
			
			
			traits = { ideology_C HoG_Old_General }
		}
	# Mikhail Kovalyov
		BLR_HoG_Mikhail_Kovalyov = {
			picture = Mikhail_Kovalyov
			allowed = { tag = BLR }
			available = {
				date > 1986.1.1
				date < 2007.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Mikhail_Kovalyov_unavailable }
			}
			
			
			traits = { ideology_C HoG_Flamboyant_Tough_Guy }
		}
	# Zmicier Zylunowicz
		BLR_HoG_Zmicier_Zylunowicz = {
			picture = Zmicier_Zylunowicz
			allowed = { tag = BLR }
			available = {
				date > 1917.1.1
				date < 1933.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Zmicier_Zylunowicz_unavailable }
			}
			
			
			traits = { ideology_C HoG_Naive_Optimist }
		}
	# Mikola Abramchyk
		BLR_HoG_Mikola_Abramchyk = {
			picture = Mikola_Abramchyk
			allowed = { tag = BLR }
			available = {
				date > 1936.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Mikola_Abramchyk_unavailable }
			}
			
			
			traits = { ideology_C HoG_Naive_Optimist }
		}
	# Nikolay F. Gikalo
		BLR_HoG_Nikolay_F_Gikalo = {
			picture = Nikolay_F_Gikalo
			allowed = { tag = BLR }
			available = {
				date > 1936.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Nikolay_F_Gikalo_unavailable }
			}
			
			
			traits = { ideology_C HoG_Ambitious_Union_Boss }
		}
	# Panteleion Ponomarenko
		BLR_HoG_Panteleion_Ponomarenko = {
			picture = Panteleion_Ponomarenko
			allowed = { tag = BLR }
			available = {
				date > 1938.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Panteleion_Ponomarenko_unavailable }
			}
			
			
			traits = { ideology_C HoG_Smiling_Oilman }
		}
	# Vasil Zacharka
		BLR_HoG_Vasil_Zacharka_2 = {
			picture = Vasil_Zacharka_2
			allowed = { tag = BLR }
			available = {
				date > 1943.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Vasil_Zacharka_unavailable }
			}
			
			
			traits = { ideology_C HoG_Old_Admiral }
		}
	# Nikolay I. Gusarov
		BLR_HoG_Nikolay_I_Gusarov = {
			picture = Nikolay_I_Gusarov
			allowed = { tag = BLR }
			available = {
				date > 1947.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Nikolay_I_Gusarov_unavailable }
			}
			
			
			traits = { ideology_C HoG_Smiling_Oilman }
		}
	# Nikolay S. Patolichev
		BLR_HoG_Nikolay_S_Patolichev = {
			picture = Nikolay_S_Patolichev
			allowed = { tag = BLR }
			available = {
				date > 1950.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Nikolay_S_Patolichev_unavailable }
			}
			
			
			traits = { ideology_C HoG_Silent_Workhorse }
		}
	# Ivan F. Klimov
		BLR_HoG_Ivan_F_Klimov = {
			picture = Ivan_F_Klimov
			allowed = { tag = BLR }
			available = {
				date > 1952.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Ivan_F_Klimov_unavailable }
			}
			
			
			traits = { ideology_C HoG_Old_General }
		}
}
#################################################
### Foreign Minister
#################################################
	Foreign_Minister = {
	# Iuri Shurevich
		BLR_FM_Iuri_Shurevich = {
			picture = Iuri_Shurevich
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Iuri_Shurevich_unavailable }
			}
			
			
			traits = { ideology_F FM_Biased_Intellectual }
		}
	# Wilhelm Kube
		BLR_FM_Wilhelm_Kube = {
			picture = Wilhelm_Kube
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Wilhelm_Kube_unavailable }
			}
			
			
			traits = { ideology_F FM_The_Cloak_N_Dagger_Schemer }
		}
	# Jazep Waronka
		BLR_FM_Jazep_Waronka = {
			picture = Jazep_Waronka
			allowed = { tag = BLR }
			available = {
				date > 1914.1.1
				date < 1953.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Jazep_Waronka_unavailable }
			}
			
			
			traits = { ideology_A FM_Ideological_Crusader }
		}
	# Vasil Zacharka
		BLR_FM_Vasil_Zacharka = {
			picture = Vasil_Zacharka
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Vasil_Zacharka_unavailable }
			}
			
			
			traits = { ideology_D FM_Great_Compromiser }
		}
	# Veniamin Novitski
		BLR_FM_Veniamin_Novitski = {
			picture = Veniamin_Novitski
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Veniamin_Novitski_unavailable }
			}
			
			
			traits = { ideology_D FM_Apologetic_Clerk }
		}
	# Ivonka Survilla
		BLR_FM_Ivonka_Survilla = {
			picture = Ivonka_Survilla
			allowed = { tag = BLR }
			available = {
				date > 1985.1.1
				date < 2020.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Ivonka_Survilla_unavailable }
			}
			
			
			traits = { ideology_D FM_Great_Compromiser }
		}
	# Anton Luckiewicz
		BLR_FM_Anton_Luckiewicz = {
			picture = Anton_Luckiewicz
			allowed = { tag = BLR }
			available = {
				date > 1914.1.1
				date < 1943.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Anton_Luckiewicz_unavailable }
			}
			
			
			traits = { ideology_D FM_Great_Compromiser }
		}
	# Jan Makreje
		BLR_FM_Jan_Makreje = {
			picture = Jan_Makreje
			allowed = { tag = BLR }
			available = {
				date > 1918.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jan_Makreje_unavailable }
			}
			
			
			traits = { ideology_D FM_Great_Compromiser }
		}
	# A. Ladnou
		BLR_FM_A_Ladnou = {
			picture = A_Ladnou
			allowed = { tag = BLR }
			available = {
				date > 1919.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = A_Ladnou_unavailable }
			}
			
			
			traits = { ideology_D FM_Great_Compromiser }
		}
	# Andrei Sannikov
		BLR_FM_Andrei_Sannikov = {
			picture = Andrei_Sannikov
			allowed = { tag = BLR }
			available = {
				date > 1985.1.1
				date < 2013.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Andrei_Sannikov_unavailable }
			}
			
			
			traits = { ideology_D FM_Great_Compromiser }
		}
	# Panteleimon Rozhnovsky
		BLR_FM_Panteleimon_Rozhnovsky = {
			picture = Panteleimon_Rozhnovsky
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Panteleimon_Rozhnovsky_unavailable }
			}
			
			
			traits = { ideology_D FM_Apologetic_Clerk }
		}
	# Pyotr A. Krechevsky
		BLR_FM_Pyotr_A_Krechevsky = {
			picture = Pyotr_A_Krechevsky
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Pyotr_A_Krechevsky_unavailable }
			}
			
			
			traits = { ideology_D FM_Ideological_Crusader }
		}
	# Nikolay I. Gusarov
		BLR_FM_Nikolay_I_Gusarov = {
			picture = Nikolay_I_Gusarov
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Nikolay_I_Gusarov_unavailable }
			}
			
			
			traits = { ideology_C FM_Great_Compromiser }
		}
	# Karl Lander
		BLR_FM_Karl_Lander = {
			picture = Karl_Lander
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Karl_Lander_unavailable }
			}
			
			
			traits = { ideology_C FM_Ideological_Crusader }
		}
}
#################################################
### Minister of Security
#################################################
	Minister_of_Security = {
	# Curt von Gottberg
		BLR_MoS_Curt_von_Gottberg = {
			picture = Curt_von_Gottberg
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Curt_von_Gottberg_unavailable }
			}
			
			
			traits = { ideology_F MoS_Prince_Of_Terror }
		}
	# Constantine Voskoboinikov
		BLR_MoS_Constantine_Voskoboinikov = {
			picture = Constantine_Voskoboinikov
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Constantine_Voskoboinikov_unavailable }
			}
			
			
			traits = { ideology_F MoS_Silent_Lawyer }
		}
	# Yafim Yafimavich Byalyevich
		BLR_MoS_Yafim_Yafimavich_Byalyevich = {
			picture = Yafim_Yafimavich_Byalyevich
			allowed = { tag = BLR }
			available = {
				date > 1914.1.1
				date < 1943.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Yafim_Yafimavich_Byalyevich_unavailable }
			}
			
			
			traits = { ideology_A MoS_Silent_Lawyer }
		}
	# Pyotra Krechewski
		BLR_MoS_Pyotra_Krechewski = {
			picture = Pyotra_Krechewski
			allowed = { tag = BLR }
			available = {
				date > 1918.1.1
				date < 1929.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Pyotra_Krechewski_unavailable }
			}
			
			
			traits = { ideology_A MoS_Crime_Fighter }
		}
	# Larysa Hienijush
		BLR_MoS_Larysa_Hienijush = {
			picture = Larysa_Hienijush
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1983.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Larysa_Hienijush_unavailable }
			}
			
			
			traits = { ideology_D MoS_Efficient_Sociopath }
		}
	# Zianon Pazniak
		BLR_MoS_Zianon_Pazniak = {
			picture = Zianon_Pazniak
			allowed = { tag = BLR }
			available = {
				date > 1985.1.1
				date < 2020.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Zianon_Pazniak_unavailable }
			}
			
			
			traits = { ideology_D MoS_Crime_Fighter }
		}
	# Alaksandar Cwikiewicz
		BLR_MoS_Alaksandar_Cwikiewicz = {
			picture = Alaksandar_Cwikiewicz
			allowed = { tag = BLR }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Alaksandar_Cwikiewicz_unavailable }
			}
			
			
			traits = { ideology_D MoS_Prince_Of_Terror }
		}
	# Alaksandar Cwikiewicz
		BLR_MoS_Alaksandar_Cwikiewicz_2 = {
			picture = Alaksandar_Cwikiewicz_2
			allowed = { tag = BLR }
			available = {
				date > 1918.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Alaksandar_Cwikiewicz_unavailable }
			}
			
			
			traits = { ideology_D MoS_Prince_Of_Terror }
		}
	# Pawel Aleksiuk
		BLR_MoS_Pawel_Aleksiuk = {
			picture = Pawel_Aleksiuk
			allowed = { tag = BLR }
			available = {
				date > 1919.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Pawel_Aleksiuk_unavailable }
			}
			
			
			traits = { ideology_D MoS_Prince_Of_Terror }
		}
	# Tamas Hryb
		BLR_MoS_Tamas_Hryb = {
			picture = Tamas_Hryb
			allowed = { tag = BLR }
			available = {
				date > 1919.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Tamas_Hryb_unavailable }
			}
			
			
			traits = { ideology_D MoS_Prince_Of_Terror }
		}
	# Jury Zacharanka
		BLR_MoS_Jury_Zacharanka = {
			picture = Jury_Zacharanka
			allowed = { tag = BLR }
			available = {
				date > 1985.1.1
				date < 2013.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jury_Zacharanka_unavailable }
			}
			
			
			traits = { ideology_D MoS_Crime_Fighter }
		}
	# Aleksander Osatkin-Vladimirsky
		BLR_MoS_Aleksander_OsatkinVladimirsky = {
			picture = Aleksander_OsatkinVladimirsky
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Aleksander_OsatkinVladimirsky_unavailable }
			}
			
			
			traits = { ideology_D MoS_Crime_Fighter }
		}
	# Yurij B. Gamarnik
		BLR_MoS_Yurij_B_Gamarnik = {
			picture = Yurij_B_Gamarnik
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Yurij_B_Gamarnik_unavailable }
			}
			
			
			traits = { ideology_C MoS_Prince_Of_Terror }
		}
	# Aleksandr I. Krivitsky
		BLR_MoS_Aleksandr_I_Krivitsky = {
			picture = Aleksandr_I_Krivitsky
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Aleksandr_I_Krivitsky_unavailable }
			}
			
			
			traits = { ideology_C MoS_Compassionate_Gentleman }
		}
	# Nikolay F. Gikalo
		BLR_MoS_Nikolay_F_Gikalo = {
			picture = Nikolay_F_Gikalo
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Nikolay_F_Gikalo_unavailable }
			}
			
			
			traits = { ideology_C MoS_Back_Stabber }
		}
	# Mikhail Bonch-Bruevich
		BLR_MoS_Mikhail_BonchBruevich = {
			picture = Mikhail_BonchBruevich
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Mikhail_BonchBruevich_unavailable }
			}
			
			
			traits = { ideology_C MoS_Man_Of_The_People }
		}
}
#################################################
### Armaments Minister
#################################################
	Armaments_Minister = {
	# Radion Survilla
		BLR_AM_Radion_Survilla = {
			picture = Radion_Survilla
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Radion_Survilla_unavailable }
			}
			
			
			traits = { ideology_F AM_Laissez-faire_Capitalist }
		}
	# Karl Lander
		BLR_AM_Karl_Lander = {
			picture = Karl_Lander
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Karl_Lander_unavailable }
			}
			
			
			traits = { ideology_F AM_Military_Entrepreneur }
		}
	# Jan Sierada
		BLR_AM_Jan_Sierada = {
			picture = Jan_Sierada
			allowed = { tag = BLR }
			available = {
				date > 1914.1.1
				date < 1944.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Jan_Sierada_unavailable }
			}
			
			
			traits = { ideology_A AM_Planned_Economy_Proponent }
		}
	# Kanstantzin Barisavich Yezavitaw
		BLR_AM_Kanstantzin_Barisavich_Yezavitaw = {
			picture = Kanstantzin_Barisavich_Yezavitaw
			allowed = { tag = BLR }
			available = {
				date > 1914.1.1
				date < 1947.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Kanstantzin_Barisavich_Yezavitaw_unavailable }
			}
			
			
			traits = { ideology_A AM_Infantry_Proponent }
		}
	# Mikola Abramchyk
		BLR_AM_Mikola_Abramchyk = {
			picture = Mikola_Abramchyk
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mikola_Abramchyk_unavailable }
			}
			
			
			traits = { ideology_D AM_Theoretical_Scientist }
		}
	# Rascislaw Zavistovic
		BLR_AM_Rascislaw_Zavistovic = {
			picture = Rascislaw_Zavistovic
			allowed = { tag = BLR }
			available = {
				date > 1985.1.1
				date < 2000.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Rascislaw_Zavistovic_unavailable }
			}
			
			
			traits = { ideology_D AM_Military_Entrepreneur }
		}
	# Wasyl Zacharka
		BLR_AM_Wasyl_Zacharka = {
			picture = Wasyl_Zacharka
			allowed = { tag = BLR }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Wasyl_Zacharka_unavailable }
			}
			
			
			traits = { ideology_D AM_Infantry_Proponent }
		}
	# E. Bialewicz
		BLR_AM_E_Bialewicz = {
			picture = E_Bialewicz
			allowed = { tag = BLR }
			available = {
				date > 1919.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = E_Bialewicz_unavailable }
			}
			
			
			traits = { ideology_D AM_Infantry_Proponent }
		}
	# Mikhail Chigir
		BLR_AM_Mikhail_Chigir = {
			picture = Mikhail_Chigir
			allowed = { tag = BLR }
			available = {
				date > 1985.1.1
				date < 2013.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mikhail_Chigir_unavailable }
			}
			
			
			traits = { ideology_D AM_Resource_Industrialist }
		}
	# Mikhail Bonch-Bruevich
		BLR_AM_Mikhail_BonchBruevich = {
			picture = Mikhail_BonchBruevich
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Mikhail_BonchBruevich_unavailable }
			}
			
			
			traits = { ideology_D AM_Tank_Proponent }
		}
	# Aleksander Osatkin-Vladimirsky
		BLR_AM_Aleksander_OsatkinVladimirsky = {
			picture = Aleksander_OsatkinVladimirsky
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Aleksander_OsatkinVladimirsky_unavailable }
			}
			
			
			traits = { ideology_C AM_Infantry_Proponent }
		}
	# Vasiliy F. Sharangovich
		BLR_AM_Vasiliy_F_Sharangovich = {
			picture = Vasiliy_F_Sharangovich
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Vasiliy_F_Sharangovich_unavailable }
			}
			
			
			traits = { ideology_C AM_Resource_Industrialist }
		}
}
#################################################
### Head of Intelligence
#################################################
	Head_of_Intelligence = {
	# Mikhail Bonch-Bruevich
		BLR_HoI_Mikhail_BonchBruevich = {
			picture = Mikhail_BonchBruevich
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Mikhail_BonchBruevich_unavailable }
			}
			
			
			traits = { ideology_F HoI_Technical_Specialist }
		}
	# Yurij B. Gamarnik
		BLR_HoI_Yurij_B_Gamarnik = {
			picture = Yurij_B_Gamarnik
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Yurij_B_Gamarnik_unavailable }
			}
			
			
			traits = { ideology_F HoI_Political_Specialist }
		}
	# Walter Darré
		BLR_HoI_Walter_Darre = {
			picture = Walter_Darre
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Walter_Darre_unavailable }
			}
			
			
			traits = { ideology_F HoI_Logistics_Specialist }
		}
	# Kanstantzin Barisavich Yezavitaw
		BLR_HoI_Kanstantzin_Barisavich_Yezavitaw = {
			picture = Kanstantzin_Barisavich_Yezavitaw
			allowed = { tag = BLR }
			available = {
				date > 1914.1.1
				date < 1947.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Kanstantzin_Barisavich_Yezavitaw_unavailable }
			}
			
			
			traits = { ideology_A HoI_Logistics_Specialist }
		}
	# Iuri Shurevich
		BLR_HoI_Iuri_Shurevich = {
			picture = Iuri_Shurevich
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Iuri_Shurevich_unavailable }
			}
			
			
			traits = { ideology_D HoI_Technical_Specialist }
		}
	# Zianon Pazniak
		BLR_HoI_Zianon_Pazniak = {
			picture = Zianon_Pazniak
			allowed = { tag = BLR }
			available = {
				date > 1985.1.1
				date < 2020.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Zianon_Pazniak_unavailable }
			}
			
			
			traits = { ideology_D HoI_Political_Specialist }
		}
	# Eduard Shirkovsky
		BLR_HoI_Eduard_Shirkovsky = {
			picture = Eduard_Shirkovsky
			allowed = { tag = BLR }
			available = {
				date > 1985.1.1
				date < 2013.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Eduard_Shirkovsky_unavailable }
			}
			
			
			traits = { ideology_D HoI_Dismal_Enigma }
		}
	# Aleksandr I. Krivitsky
		BLR_HoI_Aleksandr_I_Krivitsky = {
			picture = Aleksandr_I_Krivitsky
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Aleksandr_I_Krivitsky_unavailable }
			}
			
			
			traits = { ideology_D HoI_Political_Specialist }
		}
	# Karl Lander
		BLR_HoI_Karl_Lander = {
			picture = Karl_Lander
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Karl_Lander_unavailable }
			}
			
			
			traits = { ideology_D HoI_Dismal_Enigma }
		}
	# Yazip Losik
		BLR_HoI_Yazip_Losik = {
			picture = Yazip_Losik
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Yazip_Losik_unavailable }
			}
			
			
			traits = { ideology_C HoI_Naval_Intelligence_Specialist }
		}
	# Panteleion Ponomarenko
		BLR_HoI_Panteleion_Ponomarenko = {
			picture = Panteleion_Ponomarenko
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Panteleion_Ponomarenko_unavailable }
			}
			
			
			traits = { ideology_C HoI_Industrial_Specialist }
		}
}
#################################################
### Chief of Staff
#################################################
	Chief_of_Staff = {
	# Vassilyi Suerata
		BLR_CoStaff_Vassilyi_Suerata = {
			picture = Vassilyi_Suerata
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Vassilyi_Suerata_unavailable }
			}
			
			
			traits = { ideology_F CoStaff_School_Of_Defence }
		}
	# Stanislaw Bulak-Balachowicz
		BLR_CoStaff_Stanislaw_BulakBalachowicz = {
			picture = Stanislaw_BulakBalachowicz
			allowed = { tag = BLR }
			available = {
				date > 1914.1.1
				date < 1941.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Stanislaw_BulakBalachowicz_unavailable }
			}
			
			
			traits = { ideology_A CoStaff_School_Of_Psychology }
		}
	# Kanstantzin Barisavich Yezavitaw
		BLR_CoStaff_Kanstantzin_Barisavich_Yezavitaw = {
			picture = Kanstantzin_Barisavich_Yezavitaw
			allowed = { tag = BLR }
			available = {
				date > 1914.1.1
				date < 1947.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Kanstantzin_Barisavich_Yezavitaw_unavailable }
			}
			
			
			traits = { ideology_A CoStaff_School_Of_Defence }
		}
	# Yurij B. Gamarnik
		BLR_CoStaff_Yurij_B_Gamarnik = {
			picture = Yurij_B_Gamarnik
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Yurij_B_Gamarnik_unavailable }
			}
			
			
			traits = { ideology_D CoStaff_School_Of_Psychology }
		}
	# Michal Vituska
		BLR_CoStaff_Michal_Vituska = {
			picture = Michal_Vituska
			allowed = { tag = BLR }
			available = {
				date > 1970.1.1
				date < 2006.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Michal_Vituska_unavailable }
			}
			
			
			traits = { ideology_D CoStaff_School_Of_Psychology }
		}
	# Kanstantyn Jezawitaw
		BLR_CoStaff_Kanstantyn_Jezawitaw = {
			picture = Kanstantyn_Jezawitaw
			allowed = { tag = BLR }
			available = {
				date > 1914.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Kanstantyn_Jezawitaw_unavailable }
			}
			
			
			traits = { ideology_D CoStaff_School_Of_Psychology }
		}
	# Wasyl Zacharka
		BLR_CoStaff_Wasyl_Zacharka = {
			picture = Wasyl_Zacharka
			allowed = { tag = BLR }
			available = {
				date > 1918.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Wasyl_Zacharka_unavailable }
			}
			
			
			traits = { ideology_D CoStaff_School_Of_Psychology }
		}
	# Lavon Zajac
		BLR_CoStaff_Lavon_Zajac = {
			picture = Lavon_Zajac
			allowed = { tag = BLR }
			available = {
				date > 1918.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Lavon_Zajac_unavailable }
			}
			
			
			traits = { ideology_D CoStaff_School_Of_Psychology }
		}
	# Leonid Maltsev
		BLR_CoStaff_Leonid_Maltsev = {
			picture = Leonid_Maltsev
			allowed = { tag = BLR }
			available = {
				date > 1985.1.1
				date < 2013.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Leonid_Maltsev_unavailable }
			}
			
			
			traits = { ideology_D CoStaff_School_Of_Fire_Support }
		}
	# Vladimir I. Cheryakov
		BLR_CoStaff_Vladimir_I_Cheryakov = {
			picture = Vladimir_I_Cheryakov
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Vladimir_I_Cheryakov_unavailable }
			}
			
			
			traits = { ideology_D CoStaff_School_Of_Fire_Support }
		}
	# Anton Sokól-Kutylowski
		BLR_CoStaff_Anton_SokolKutylowski = {
			picture = Anton_SokolKutylowski
			allowed = { tag = BLR }
			available = {
				date > 1920.1.1
				date < 1933.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Anton_SokolKutylowski_unavailable }
			}
			
			
			traits = { ideology_C CoStaff_School_Of_Psychology }
		}
	# Serafim Pokrovski
		BLR_CoStaff_Serafim_Pokrovski = {
			picture = Serafim_Pokrovski
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Serafim_Pokrovski_unavailable }
			}
			
			
			traits = { ideology_C CoStaff_School_Of_Psychology }
		}
}
#################################################
### Chief of Army
#################################################
	Chief_of_Army = {
	# Bronislav Kaminski
		BLR_CoArmy_Bronislav_Kaminski = {
			picture = Bronislav_Kaminski
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Bronislav_Kaminski_unavailable }
			}
			
			
			traits = { ideology_F CoArmy_Guns_And_Butter_Doctrine }
		}
	# Stanislaw Bulak-Balachowicz
		BLR_CoArmy_Stanislaw_BulakBalachowicz = {
			picture = Stanislaw_BulakBalachowicz
			allowed = { tag = BLR }
			available = {
				date > 1914.1.1
				date < 1941.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Stanislaw_BulakBalachowicz_unavailable }
			}
			
			
			traits = { ideology_A CoArmy_Armoured_Spearhead_Doctrine }
		}
	# Guasan Kanapatzki
		BLR_CoArmy_Guasan_Kanapatzki = {
			picture = Guasan_Kanapatzki
			allowed = { tag = BLR }
			available = {
				date > 1914.1.1
				date < 1954.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Guasan_Kanapatzki_unavailable }
			}
			
			
			traits = { ideology_A CoArmy_Decisive_Battle_Doctrine }
		}
	# Cyprian Kondratowicz
		BLR_CoArmy_Cyprian_Kondratowicz = {
			picture = Cyprian_Kondratowicz
			allowed = { tag = BLR }
			available = {
				date > 1920.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Cyprian_Kondratowicz_unavailable }
			}
			
			
			traits = { ideology_A CoArmy_Armoured_Spearhead_Doctrine }
		}
	# Jozef Dowbor-Musnicki
		BLR_CoArmy_Jozef_DowborMusnicki = {
			picture = Jozef_DowborMusnicki
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jozef_DowborMusnicki_unavailable }
			}
			
			
			traits = { ideology_D CoArmy_Decisive_Battle_Doctrine }
		}
	# Michal Vituska
		BLR_CoArmy_Michal_Vituska = {
			picture = Michal_Vituska
			allowed = { tag = BLR }
			available = {
				date > 1970.1.1
				date < 2006.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Michal_Vituska_unavailable }
			}
			
			
			traits = { ideology_D CoArmy_Commando_Doctrine_Army }
		}
	# Leonid Maltsev
		BLR_CoArmy_Leonid_Maltsev = {
			picture = Leonid_Maltsev
			allowed = { tag = BLR }
			available = {
				date > 1985.1.1
				date < 2013.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Leonid_Maltsev_unavailable }
			}
			
			
			traits = { ideology_D CoArmy_Decisive_Battle_Doctrine }
		}
	# Serafim Pokrovski
		BLR_CoArmy_Serafim_Pokrovski = {
			picture = Serafim_Pokrovski
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Serafim_Pokrovski_unavailable }
			}
			
			
			traits = { ideology_D CoArmy_Static_Defence_Doctrine }
		}
	# Anton Sokól-Kutylowski
		BLR_CoArmy_Anton_SokolKutylowski = {
			picture = Anton_SokolKutylowski
			allowed = { tag = BLR }
			available = {
				date > 1920.1.1
				date < 1933.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Anton_SokolKutylowski_unavailable }
			}
			
			
			traits = { ideology_C CoArmy_Armoured_Spearhead_Doctrine }
		}
	# Pavel Ponomarenko
		BLR_CoArmy_Pavel_Ponomarenko = {
			picture = Pavel_Ponomarenko
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Pavel_Ponomarenko_unavailable }
			}
			
			
			traits = { ideology_C CoArmy_Armoured_Spearhead_Doctrine }
		}
}
#################################################
### Chief of Navy
#################################################
	Chief_of_Navy = {
	# Erhard Märtens
		BLR_CoNavy_Erhard_Martens = {
			picture = Erhard_Martens
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Erhard_Martens_unavailable }
			}
			
			
			traits = { ideology_F CoNavy_Base_Control_Doctrine }
		}
	# Friedrich Wilhelm Kurzr
		BLR_CoNavy_Friedrich_Wilhelm_Kurzr = {
			picture = Friedrich_Wilhelm_Kurzr
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Friedrich_Wilhelm_Kurzr_unavailable }
			}
			
			
			traits = { ideology_D CoNavy_Open_Seas_Doctrine }
		}
	# Michal Vituska
		BLR_CoNavy_Michal_Vituska = {
			picture = Michal_Vituska
			allowed = { tag = BLR }
			available = {
				date > 1970.1.1
				date < 2006.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Michal_Vituska_unavailable }
			}
			
			
			traits = { ideology_D CoNavy_Base_Control_Doctrine }
		}
	# Leonid Maltsev
		BLR_CoNavy_Leonid_Maltsev = {
			picture = Leonid_Maltsev
			allowed = { tag = BLR }
			available = {
				date > 1985.1.1
				date < 2013.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Leonid_Maltsev_unavailable }
			}
			
			
			traits = { ideology_D CoNavy_Indirect_Approach_Doctrine }
		}
	# Aleksandr Iziumov
		BLR_CoNavy_Aleksandr_Iziumov = {
			picture = Aleksandr_Iziumov
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Aleksandr_Iziumov_unavailable }
			}
			
			
			traits = { ideology_D CoNavy_Base_Control_Doctrine }
		}
	# Valerik Lesik
		BLR_CoNavy_Valerik_Lesik = {
			picture = Valerik_Lesik
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Valerik_Lesik_unavailable }
			}
			
			
			traits = { ideology_C CoNavy_Indirect_Approach_Doctrine }
		}
}
#################################################
### Chief of Airforce
#################################################
	Chief_of_Airforce = {
	# Hans Baur
		BLR_CoAir_Hans_Baur = {
			picture = Hans_Baur
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Hans_Baur_unavailable }
			}
			
			
			traits = { ideology_F CoAir_Air_Superiority_Doctrine }
		}
	# Aleksandr Iziumov
		BLR_CoAir_Aleksandr_Iziumov = {
			picture = Aleksandr_Iziumov
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Aleksandr_Iziumov_unavailable }
			}
			
			
			traits = { ideology_D CoAir_Vertical_Envelopment_Doctrine }
		}
	# Michal Vituska
		BLR_CoAir_Michal_Vituska = {
			picture = Michal_Vituska
			allowed = { tag = BLR }
			available = {
				date > 1970.1.1
				date < 2006.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Michal_Vituska_unavailable }
			}
			
			
			traits = { ideology_D CoAir_Army_Aviation_Doctrine }
		}
	# Leonid Maltsev
		BLR_CoAir_Leonid_Maltsev = {
			picture = Leonid_Maltsev
			allowed = { tag = BLR }
			available = {
				date > 1985.1.1
				date < 2013.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Leonid_Maltsev_unavailable }
			}
			
			
			traits = { ideology_D CoAir_Army_Aviation_Doctrine }
		}
	# Nikolay F. Gikalo
		BLR_CoAir_Nikolay_F_Gikalo = {
			picture = Nikolay_F_Gikalo
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Nikolay_F_Gikalo_unavailable }
			}
			
			
			traits = { ideology_D CoAir_Army_Aviation_Doctrine }
		}
	# Vladimir I. Cheryakov
		BLR_CoAir_Vladimir_I_Cheryakov = {
			picture = Vladimir_I_Cheryakov
			allowed = { tag = BLR }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Vladimir_I_Cheryakov_unavailable }
			}
			
			
			traits = { ideology_C CoAir_Carpet_Bombing_Doctrine }
		}
	}
}
