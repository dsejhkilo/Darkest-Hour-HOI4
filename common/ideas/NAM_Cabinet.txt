ideas = {
#################################################
### Head of Government
#################################################
	Head_of_Government = {
	# C.J. Langenhooven
		NAM_HoG_CJ_Langenhooven = {
			picture = CJ_Langenhooven
			allowed = { tag = NAM }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = CJ_Langenhooven_unavailable }
			}
			
			
			traits = { ideology_A HoG_Silent_Workhorse }
		}
	# Johannes G. H. van der Wath
		NAM_HoG_Johannes_G_H_van_der_Wath = {
			picture = Johannes_G_H_van_der_Wath
			allowed = { tag = NAM }
			available = {
				date > 1968.1.1
				date < 1987.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Johannes_G_H_van_der_Wath_unavailable }
			}
			
			
			traits = { ideology_A HoG_Backroom_Backstabber }
		}
	# Barend J. van der Walt
		NAM_HoG_Barend_J_van_der_Walt = {
			picture = Barend_J_van_der_Walt
			allowed = { tag = NAM }
			available = {
				date > 1971.1.1
				date < 1980.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Barend_J_van_der_Walt_unavailable }
			}
			
			
			traits = { ideology_A HoG_Smiling_Oilman }
		}
	# Marthinus T. Steyn
		NAM_HoG_Marthinus_T_Steyn = {
			picture = Marthinus_T_Steyn
			allowed = { tag = NAM }
			available = {
				date > 1977.1.1
				date < 1985.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Marthinus_T_Steyn_unavailable }
			}
			
			
			traits = { ideology_A HoG_Happy_Amateur }
		}
	# Gerrit Viljoen
		NAM_HoG_Gerrit_Viljoen = {
			picture = Gerrit_Viljoen
			allowed = { tag = NAM }
			available = {
				date > 1979.1.1
				date < 2010.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Gerrit_Viljoen_unavailable }
			}
			
			
			traits = { ideology_A HoG_Political_Protege }
		}
	# Danie Hough
		NAM_HoG_Danie_Hough = {
			picture = Danie_Hough
			allowed = { tag = NAM }
			available = {
				date > 1980.1.1
				date < 1990.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Danie_Hough_unavailable }
			}
			
			
			traits = { ideology_A HoG_Political_Protege }
		}
	# Willie van Niekerk
		NAM_HoG_Willie_van_Niekerk = {
			picture = Willie_van_Niekerk
			allowed = { tag = NAM }
			available = {
				date > 1983.1.1
				date < 2010.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Willie_van_Niekerk_unavailable }
			}
			
			
			traits = { ideology_A HoG_Naive_Optimist }
		}
	# Wentzel Christoffel du Plessis
		NAM_HoG_Wentzel_Christoffel_du_Plessis = {
			picture = Wentzel_Christoffel_du_Plessis
			allowed = { tag = NAM }
			available = {
				date > 1964.1.1
				date < 1975.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Wentzel_Christoffel_du_Plessis_unavailable }
			}
			
			
			traits = { ideology_D HoG_Political_Protege }
		}
	# Petrus I. Hoogenhout
		NAM_HoG_Petrus_I_Hoogenhout = {
			picture = Petrus_I_Hoogenhout
			allowed = { tag = NAM }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Petrus_I_Hoogenhout_unavailable }
			}
			
			
			traits = { ideology_D HoG_Ambitious_Union_Boss }
		}
	# David G. Conradie
		NAM_HoG_David_G_Conradie = {
			picture = David_G_Conradie
			allowed = { tag = NAM }
			available = {
				date > 1936.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = David_G_Conradie_unavailable }
			}
			
			
			traits = { ideology_D HoG_Political_Protege }
		}
	# Albertus Roux van Rhijn
		NAM_HoG_Albertus_Roux_van_Rhijn = {
			picture = Albertus_Roux_van_Rhijn
			allowed = { tag = NAM }
			available = {
				date > 1951.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Albertus_Roux_van_Rhijn_unavailable }
			}
			
			
			traits = { ideology_D HoG_Corporate_Suit }
		}
	# Daniel du Plessis Viljoen
		NAM_HoG_Daniel_du_Plessis_Viljoen = {
			picture = Daniel_du_Plessis_Viljoen
			allowed = { tag = NAM }
			available = {
				date > 1953.1.1
				date < 1970.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Daniel_du_Plessis_Viljoen_unavailable }
			}
			
			
			traits = { ideology_D HoG_Smiling_Oilman }
		}
	# Louis Pienaar
		NAM_HoG_Louis_Pienaar = {
			picture = Louis_Pienaar
			allowed = { tag = NAM }
			available = {
				date > 1985.1.1
				date < 2013.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Louis_Pienaar_unavailable }
			}
			
			
			traits = { ideology_D HoG_Naive_Optimist }
		}
	# Hage Geingob
		NAM_HoG_Hage_Geingob = {
			picture = Hage_Geingob
			allowed = { tag = NAM }
			available = {
				date > 1990.1.1
				date < 2020.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Hage_Geingob_unavailable }
			}
			
			
			traits = { ideology_D HoG_Ambitious_Union_Boss }
		}
}
#################################################
### Foreign Minister
#################################################
	Foreign_Minister = {
	# Bettie Conradie
		NAM_FM_Bettie_Conradie = {
			picture = Bettie_Conradie
			allowed = { tag = NAM }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Bettie_Conradie_unavailable }
			}
			
			
			traits = { ideology_A FM_Iron_Fisted_Brute }
		}
	# Tielman J. de Villiers Roos
		NAM_FM_Tielman_J_de_Villiers_Roos = {
			picture = Tielman_J_de_Villiers_Roos
			allowed = { tag = NAM }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Tielman_J_de_Villiers_Roos_unavailable }
			}
			
			
			traits = { ideology_D FM_The_Cloak_N_Dagger_Schemer }
		}
}
#################################################
### Minister of Security
#################################################
	Minister_of_Security = {
	# Hendrijk de Tottenberg
		NAM_MoS_Hendrijk_de_Tottenberg = {
			picture = Hendrijk_de_Tottenberg
			allowed = { tag = NAM }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Hendrijk_de_Tottenberg_unavailable }
			}
			
			
			traits = { ideology_A MoS_Back_Stabber }
		}
	# Nikolaus J. de Wet
		NAM_MoS_Nikolaus_J_de_Wet = {
			picture = Nikolaus_J_de_Wet
			allowed = { tag = NAM }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Nikolaus_J_de_Wet_unavailable }
			}
			
			
			traits = { ideology_D MoS_Man_Of_The_People }
		}
}
#################################################
### Armaments Minister
#################################################
	Armaments_Minister = {
	# Christiaan van den Heever
		NAM_AM_Christiaan_van_den_Heever = {
			picture = Christiaan_van_den_Heever
			allowed = { tag = NAM }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Christiaan_van_den_Heever_unavailable }
			}
			
			
			traits = { ideology_A AM_Military_Entrepreneur }
		}
	# Cornelius Boers
		NAM_AM_Cornelius_Boers = {
			picture = Cornelius_Boers
			allowed = { tag = NAM }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Cornelius_Boers_unavailable }
			}
			
			
			traits = { ideology_D AM_Laissez-faire_Capitalist }
		}
}
#################################################
### Head of Intelligence
#################################################
	Head_of_Intelligence = {
	# Adriaan Gewelten
		NAM_HoI_Adriaan_Gewelten = {
			picture = Adriaan_Gewelten
			allowed = { tag = NAM }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Adriaan_Gewelten_unavailable }
			}
			
			
			traits = { ideology_A HoI_Political_Specialist }
		}
	# Pieter Roerloof
		NAM_HoI_Pieter_Roerloof = {
			picture = Pieter_Roerloof
			allowed = { tag = NAM }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Pieter_Roerloof_unavailable }
			}
			
			
			traits = { ideology_D HoI_Industrial_Specialist }
		}
}
#################################################
### Chief of Staff
#################################################
	Chief_of_Staff = {
	# Nils Knobel
		NAM_CoStaff_Nils_Knobel = {
			picture = Nils_Knobel
			allowed = { tag = NAM }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Nils_Knobel_unavailable }
			}
			
			
			traits = { ideology_A CoStaff_School_Of_Fire_Support }
		}
	# John E.J. Wanstone
		NAM_CoStaff_John_EJ_Wanstone = {
			picture = John_EJ_Wanstone
			allowed = { tag = NAM }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = John_EJ_Wanstone_unavailable }
			}
			
			
			traits = { ideology_D CoStaff_School_Of_Manoeuvre }
		}
}
#################################################
### Chief of Army
#################################################
	Chief_of_Army = {
	# Hendrijk van Immen
		NAM_CoArmy_Hendrijk_van_Immen = {
			picture = Hendrijk_van_Immen
			allowed = { tag = NAM }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Hendrijk_van_Immen_unavailable }
			}
			
			
			traits = { ideology_A CoArmy_Decisive_Battle_Doctrine }
		}
	# Rijkard van Deffels
		NAM_CoArmy_Rijkard_van_Deffels = {
			picture = Rijkard_van_Deffels
			allowed = { tag = NAM }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Rijkard_van_Deffels_unavailable }
			}
			
			
			traits = { ideology_D CoArmy_Static_Defence_Doctrine }
		}
}
#################################################
### Chief of Navy
#################################################
	Chief_of_Navy = {
	# Hendrijk Bours
		NAM_CoNavy_Hendrijk_Bours = {
			picture = Hendrijk_Bours
			allowed = { tag = NAM }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Hendrijk_Bours_unavailable }
			}
			
			
			traits = { ideology_A CoNavy_Base_Control_Doctrine }
		}
	# Rijnhaard de Beers
		NAM_CoNavy_Rijnhaard_de_Beers = {
			picture = Rijnhaard_de_Beers
			allowed = { tag = NAM }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Rijnhaard_de_Beers_unavailable }
			}
			
			
			traits = { ideology_D CoNavy_Decisive_Naval_Battle_Doctrine }
		}
}
#################################################
### Chief of Airforce
#################################################
	Chief_of_Airforce = {
	# Michel de Groot
		NAM_CoAir_Michel_de_Groot = {
			picture = Michel_de_Groot
			allowed = { tag = NAM }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Michel_de_Groot_unavailable }
			}
			
			
			traits = { ideology_A CoAir_Naval_Aviation_Doctrine }
		}
	# Francis Dalton
		NAM_CoAir_Francis_Dalton = {
			picture = Francis_Dalton
			allowed = { tag = NAM }
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Francis_Dalton_unavailable }
			}
			
			
			traits = { ideology_D CoAir_Vertical_Envelopment_Doctrine }
		}
	}
}
