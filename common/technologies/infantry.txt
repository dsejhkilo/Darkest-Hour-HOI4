technologies = {
	@1900 = 0
	@1916 = 2
	@1935 = 4
	@1939 = 6
	@1942 = 8
	@1945 = 10
	@1952 = 12
	@1962 = 14
	@1980 = 16
	@1995 = 18
	
	@Uniform_line = -3
	@Small_Arms_line = 0
	@Support_Weapons_line = 3
	@Heavy_Weapons_line = 6
	@AT_Weapons_line = 9
	
	@Night_Vision_line = 16
	
#################################################################
## Infantry Technologies
##################################################################
	# 1900 Small Arms (Gewehr 98)
	Small_Arms_1900 = {
		
		enable_equipments = {
			Small_Arms_equipment_1900
		}
		
		enable_subunits = {
			infantry
			militia
			garrison
		}
		
		path = {
			leads_to_tech = Small_Arms_1916
			research_cost_coeff = 1
		}		
		path = {
			leads_to_tech = Uniform_1914
			research_cost_coeff = 1
		}		
		path = {
			leads_to_tech = Support_Weapons_1914
			research_cost_coeff = 1
		}
		
		research_cost = 1.5
		start_year = 1900
		folder = {
			name = infantry_folder
			position = { x = @Small_Arms_line y = @1900 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			training
		}
		
		ai_will_do = {
			factor = 1
		}
	}
	# 1916 Small Arms (Gewehr 98)
	Small_Arms_1916 = {
		
		enable_equipments = {
			Small_Arms_equipment_1916
		}
		
		path = {
			leads_to_tech = Small_Arms_1935
			research_cost_coeff = 1
		}		
		path = {
			leads_to_tech = Uniform_1918
			research_cost_coeff = 1
		}		
		path = {
			leads_to_tech = Support_Weapons_1918
			research_cost_coeff = 1
		}		
		
		research_cost = 1.5
		start_year = 1916
		folder = {
			name = infantry_folder
			position = { x = @Small_Arms_line y = @1916 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			training
		}
		
		ai_will_do = {
			factor = 1
		}
	}
	
	# 1936 Small Arms (Kar98k)	
	Small_Arms_1935 = {
		
		enable_equipments = {
			Small_Arms_equipment_1935
		}
		
		path = {
			leads_to_tech = Small_Arms_1939
			research_cost_coeff = 1
		}		
		path = {
			leads_to_tech = Uniform_1935
			research_cost_coeff = 1
		}
		path = {
			leads_to_tech = Support_Weapons_1935
			research_cost_coeff = 1
		}
		
		research_cost = 1.5
		start_year = 1936
		folder = {
			name = infantry_folder
			position = { x = @Small_Arms_line y = @1935 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			training
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 10
				tag = CHI
				
				OR = {
					date > "1937.1.1"
					JAP = { is_ai = no }
				}
			}
			
			modifier = {
				factor = 10
				date > "1936.1.3"
			}
		}
	}
	
	# 1939 Small Arms (MP-40)
	Small_Arms_1939 = {
		
		enable_equipments = {
			Small_Arms_equipment_1939
		}
		
		path = {
			leads_to_tech = Small_Arms_1942
			research_cost_coeff = 1
		}
		
		research_cost = 1.5
		start_year = 1939
		folder = {
			name = infantry_folder
			position = { x = @Small_Arms_line y = @1939 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			training
		}
		
		ai_will_do = {
			factor = 2
		}
	}
	# 1942 Small Arms (G43)
	Small_Arms_1942 = {
		
		enable_equipments = {
			Small_Arms_equipment_1942
		}	
		
		path = {
			leads_to_tech = Small_Arms_1945
			research_cost_coeff = 1
		}
		path = {
			leads_to_tech = Uniform_1942
			research_cost_coeff = 1
		}		
		path = {
			leads_to_tech = Support_Weapons_1942
			research_cost_coeff = 1
		}		
		research_cost = 2
		start_year = 1942
		folder = {
			name = infantry_folder
			position = { x = @Small_Arms_line y = @1942 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			training
		}
		
		ai_will_do = {
			factor = 0.8
		}
	}
	# 1945 Small Arms
	Small_Arms_1945 = {
		
		enable_equipments = {
			Small_Arms_equipment_1945
		}	
		
		path = {
			leads_to_tech = Small_Arms_1952
			research_cost_coeff = 1
		}
		path = {
			leads_to_tech = Uniform_1945
			research_cost_coeff = 1
		}	
		path = {
			leads_to_tech = Support_Weapons_1945
			research_cost_coeff = 1
		}	
		research_cost = 2
		start_year = 1945
		folder = {
			name = infantry_folder
			position = { x = @Small_Arms_line y = @1945 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			training
		}
		
		ai_will_do = {
			factor = 0.8
		}
	}
	# 1952 Small Arms
	Small_Arms_1952 = {
		
		enable_equipments = {
			Small_Arms_equipment_1952
		}	
		
		path = {
			leads_to_tech = Small_Arms_1962
			research_cost_coeff = 1
		}
		path = {
			leads_to_tech = Uniform_1952
			research_cost_coeff = 1
		}
		
		research_cost = 2
		start_year = 1952
		folder = {
			name = infantry_folder
			position = { x = @Small_Arms_line y = @1952 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			training
		}
		
		ai_will_do = {
			factor = 0.8
		}
	}	
	
	
	
	# 1962 Small Arms
	Small_Arms_1962 = {
		
		enable_equipments = {
			Small_Arms_equipment_1962
		}	
		
		path = {
			leads_to_tech = Small_Arms_1980
			research_cost_coeff = 1
		}
		path = {
			leads_to_tech = Uniform_1962
			research_cost_coeff = 1
		}
		
		research_cost = 2
		start_year = 1962
		folder = {
			name = infantry_folder
			position = { x = @Small_Arms_line y = @1962 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			training
		}
		
		ai_will_do = {
			factor = 0.8
		}
	}	
	# 1980 Small Arms
	Small_Arms_1980 = {

		enable_equipments = {
			Small_Arms_equipment_1980
		}	
		
		path = {
			leads_to_tech = Small_Arms_1995
			research_cost_coeff = 1
		}
		path = {
			leads_to_tech = Uniform_1980
			research_cost_coeff = 1
		}
		
		research_cost = 2
		start_year = 1980
		folder = {
			name = infantry_folder
			position = { x = @Small_Arms_line y = @1980 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			training
		}
		
		ai_will_do = {
			factor = 0.8
		}
	}
	# 1995 Small Arms
	Small_Arms_1995 = {
		
		enable_equipments = {
			Small_Arms_equipment_1995
		}	
		
		path = {
			leads_to_tech = Small_Arms_1995
			research_cost_coeff = 1
		}
		path = {
			leads_to_tech = Uniform_1995
			research_cost_coeff = 1
		}
		
		research_cost = 2
		start_year = 1995
		folder = {
			name = infantry_folder
			position = { x = @Small_Arms_line y = @1995 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			training
		}
		
		ai_will_do = {
			factor = 0.8
		}
	}
#################################################################
## Uniforms & Protection Technologies
##################################################################		
	# 1915 Uniform
	Uniform_1914 = {
		category_all_infantry = {
			defense = 0.03
		}
		
		path = {
			leads_to_tech = Uniform_1918
			research_cost_coeff = 1
		}


		research_cost = 1.5
		start_year = 1915
		folder = {
			name = infantry_folder
			position = { x = @Uniform_line y = @1900 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			training
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1942.1.6"
			}
			
			modifier = {
				factor = 2
				date > "1943.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1944.1.1"
			}
		}
	}
	# 1918 Uniform
	Uniform_1918 = {
		category_all_infantry = {
			defense = 0.03
		}
		
		path = {
			leads_to_tech = Uniform_1935
			research_cost_coeff = 1
		}
		
		research_cost = 1.5
		start_year = 1918
		folder = {
			name = infantry_folder
			position = { x = @Uniform_line y = @1916 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			training
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1942.1.6"
			}
			
			modifier = {
				factor = 2
				date > "1943.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1944.1.1"
			}
		}
	}
	# 1939 Uniform
	Uniform_1935 = {
		category_all_infantry = {
			defense = 0.03
		}
		
		path = {
			leads_to_tech = Uniform_1942
			research_cost_coeff = 1
		}
		
		research_cost = 1.5
		start_year = 1935
		folder = {
			name = infantry_folder
			position = { x = @Uniform_line y = @1935 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			training
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1942.1.6"
			}
			
			modifier = {
				factor = 2
				date > "1943.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1944.1.1"
			}
		}
	}
	# 1942 Uniform	
	Uniform_1942 = {
		category_all_infantry = {
			defense = 0.03
		}
		
		path = {
			leads_to_tech = Uniform_1945
			research_cost_coeff = 1
		}	
		
		research_cost = 1.5
		start_year = 1942
		folder = {
			name = infantry_folder
			position = { x = @Uniform_line y = @1942 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			training
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1943.1.6"
			}
			
			modifier = {
				factor = 2
				date > "1944.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1945.1.1"
			}
		}
	}
	
	# 1944 Uniform
	Uniform_1945 = {
		category_all_infantry = {
			defense = 0.03
		}
		
		path = {
			leads_to_tech = Uniform_1952
			research_cost_coeff = 1
		}	
		
		research_cost = 1.5
		start_year = 1944
		folder = {
			name = infantry_folder
			position = { x = @Uniform_line y = @1945 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			training
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1943.1.6"
			}
			
			modifier = {
				factor = 2
				date > "1944.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1945.1.1"
			}
		}
	}	
	
	# 1952 Uniform	
	Uniform_1952 = {
		category_all_infantry = {
			defense = 0.03
		}
		
		path = {
			leads_to_tech = Uniform_1962
			research_cost_coeff = 1
		}	
		
		research_cost = 1.5
		start_year = 1952
		folder = {
			name = infantry_folder
			position = { x = @Uniform_line y = @1952 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			training
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1943.1.6"
			}
			
			modifier = {
				factor = 2
				date > "1944.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1945.1.1"
			}
		}
	}
	
	# 1962 Uniform	
	Uniform_1962 = {
		category_all_infantry = {
			defense = 0.03
		}
		path = {
			leads_to_tech = Uniform_1980
			research_cost_coeff = 1
		}		
		research_cost = 1.5
		start_year = 1962
		folder = {
			name = infantry_folder
			position = { x = @Uniform_line y = @1962 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			training
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1943.1.6"
			}
			
			modifier = {
				factor = 2
				date > "1944.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1945.1.1"
			}
		}
	}
	# 1980 Uniform	
	Uniform_1980 = {
		category_all_infantry = {
			defense = 0.03
		}
		
		path = {
			leads_to_tech = Uniform_1995
			research_cost_coeff = 1
		}			
		research_cost = 1.5
		start_year = 1980
		folder = {
			name = infantry_folder
			position = { x = @Uniform_line y = @1980 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			training
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1943.1.6"
			}
			
			modifier = {
				factor = 2
				date > "1944.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1945.1.1"
			}
		}
	}
	# 1995 Uniform	
	Uniform_1995 = {
		category_all_infantry = {
			defense = 0.03
		}
		
		path = {
			leads_to_tech = Uniform_1995
			research_cost_coeff = 1
		}			
		research_cost = 1.5
		start_year = 1995
		folder = {
			name = infantry_folder
			position = { x = @Uniform_line y = @1995 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			training
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1943.1.6"
			}
			
			modifier = {
				factor = 2
				date > "1944.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1945.1.1"
			}
		}
	}
#################################################################
## Squad Auto Weapons Technologies
##################################################################		
	# 1915 Support_Weapons
	Support_Weapons_1914 = {
		category_all_infantry = {
        			defense = 0.05
        			soft_attack = 0.02
        		}
		
		path = {
			leads_to_tech = Support_Weapons_1918
			research_cost_coeff = 1
		}
		
		research_cost = 1.5
		start_year = 1915
		folder = {
			name = infantry_folder
			position = { x = @Support_Weapons_line y = @1900 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			training
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1942.1.6"
			}
			
			modifier = {
				factor = 2
				date > "1943.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1944.1.1"
			}
		}
	}
	# 1918 Support_Weapons
	Support_Weapons_1918 = {
		category_all_infantry = {
            defense = 0.05
            soft_attack = 0.02
        }
		
		path = {
			leads_to_tech = Support_Weapons_1935
			research_cost_coeff = 1
		}
		
		research_cost = 1.5
		start_year = 1918
		folder = {
			name = infantry_folder
			position = { x = @Support_Weapons_line y = @1916 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			training
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1942.1.6"
			}
			
			modifier = {
				factor = 2
				date > "1943.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1944.1.1"
			}
		}
	}
	# 1939 Support_Weapons
	Support_Weapons_1935 = {
		category_all_infantry = {
            defense = 0.05
            soft_attack = 0.02
        }
		
		path = {
			leads_to_tech = Support_Weapons_1942
			research_cost_coeff = 1
		}
		
		research_cost = 1.5
		start_year = 1935
		folder = {
			name = infantry_folder
			position = { x = @Support_Weapons_line y = @1935 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			training
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1942.1.6"
			}
			
			modifier = {
				factor = 2
				date > "1943.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1944.1.1"
			}
		}
	}
	# 1942 Support_Weapons	
	Support_Weapons_1942 = {
		category_all_infantry = {
            defense = 0.05
            soft_attack = 0.02
        }
		
		category_all_infantry = {
			hard_attack = 0.25
			ap_attack = 1
		}
		
		path = {
			leads_to_tech = Support_Weapons_1945
			research_cost_coeff = 1
		}	
		
		research_cost = 1.5
		start_year = 1942
		folder = {
			name = infantry_folder
			position = { x = @Support_Weapons_line y = @1942 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			training
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1943.1.6"
			}
			
			modifier = {
				factor = 2
				date > "1944.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1945.1.1"
			}
		}
	}
	
	# 1944 Support_Weapons
	Support_Weapons_1945 = {
		category_all_infantry = {
            defense = 0.05
            soft_attack = 0.02
        }
		
		path = {
			leads_to_tech = Support_Weapons_1952
			research_cost_coeff = 1
		}	
		
		research_cost = 1.5
		start_year = 1944
		folder = {
			name = infantry_folder
			position = { x = @Support_Weapons_line y = @1945 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			training
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1943.1.6"
			}
			
			modifier = {
				factor = 2
				date > "1944.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1945.1.1"
			}
		}
	}	
	
	# 1952 Support_Weapons	
	Support_Weapons_1952 = {
		category_all_infantry = {
            defense = 0.05
            soft_attack = 0.02
        }
		
		path = {
			leads_to_tech = Support_Weapons_1962
			research_cost_coeff = 1
		}	
		
		research_cost = 1.5
		start_year = 1952
		folder = {
			name = infantry_folder
			position = { x = @Support_Weapons_line y = @1952 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			training
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1943.1.6"
			}
			
			modifier = {
				factor = 2
				date > "1944.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1945.1.1"
			}
		}
	}
	
	# 1962 Support_Weapons	
	Support_Weapons_1962 = {
		category_all_infantry = {
            defense = 0.05
            soft_attack = 0.02
        }
		path = {
			leads_to_tech = Support_Weapons_1980
			research_cost_coeff = 1
		}		
		research_cost = 1.5
		start_year = 1962
		folder = {
			name = infantry_folder
			position = { x = @Support_Weapons_line y = @1962 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			training
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1943.1.6"
			}
			
			modifier = {
				factor = 2
				date > "1944.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1945.1.1"
			}
		}
	}
	# 1980 Support_Weapons	
	Support_Weapons_1980 = {
		category_all_infantry = {
            defense = 0.05
            soft_attack = 0.02
        }
		path = {
			leads_to_tech = Support_Weapons_1995
			research_cost_coeff = 1
		}			
		research_cost = 1.5
		start_year = 1980
		folder = {
			name = infantry_folder
			position = { x = @Support_Weapons_line y = @1980 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			training
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1943.1.6"
			}
			
			modifier = {
				factor = 2
				date > "1944.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1945.1.1"
			}
		}
	}
	# 1995 Support_Weapons	
	Support_Weapons_1995 = {
		category_all_infantry = {
            defense = 0.05
            soft_attack = 0.02
        }
		path = {
			leads_to_tech = Support_Weapons_1995
			research_cost_coeff = 1
		}			
		research_cost = 1.5
		start_year = 1995
		folder = {
			name = infantry_folder
			position = { x = @Support_Weapons_line y = @1995 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			training
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1943.1.6"
			}
			
			modifier = {
				factor = 2
				date > "1944.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1945.1.1"
			}
		}
	}
	
	#################################################################
	## Support Weapons
	##################################################################		
	# 1900 Support Weapons
	Heavy_Weapons_1905 = {
		
		path = {
			leads_to_tech = Heavy_Weapons_1916
			research_cost_coeff = 1
		}
		path = {
			leads_to_tech = Support_Weapons_1914
			research_cost_coeff = 1
		}
		
		enable_equipments = {
			Heavy_Weapons_equipment_1905
		}
		### Units will start using Support Weapons		
		infantry = { 
			need = {
				Heavy_Weapons_equipment = 3
			}
		}
		SS_infantry = { 
			need = {
				Heavy_Weapons_equipment = 3
			}
		}
		
		garrison = { 
			need = {
				Heavy_Weapons_equipment = 5
			}
		}		
		cavalry = { 
			need = {
				Heavy_Weapons_equipment = 2
			}
		}
		SS_cavalry = { 
			need = {
				Heavy_Weapons_equipment = 2
			}
		}
		motorized = { 
			need = {
				Heavy_Weapons_equipment = 3
			}
		}
		SS_motorized = { 
			need = {
				Heavy_Weapons_equipment = 3
			}
		}
		mechanized = { 
			need = {
				Heavy_Weapons_equipment = 4
			}
		}
		SS_mechanized = { 
			need = {
				Heavy_Weapons_equipment = 4
			}
		}
		
		heavy_mechanized = { 
			need = {
				Heavy_Weapons_equipment = 4
			}
		}
		paratrooper = { 
			need = {
				Heavy_Weapons_equipment = 4
			}
		}
		marine = { 
			need = {
				Heavy_Weapons_equipment = 4
			}
		}
		mountaineers = { 
			need = {
				Heavy_Weapons_equipment = 4
			}
		}
		
		
		research_cost = 1.5
		start_year = 1900
		folder = {
			name = infantry_folder
			position = { x = @Heavy_Weapons_line y = @1900 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			artillery
			training
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1937.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1938.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1939.1.1"
			}
		}
	}
	# 1916 Support Weapons
	Heavy_Weapons_1916 = {
		
		path = {
			leads_to_tech = Heavy_Weapons_1936
			research_cost_coeff = 1
		}
		path = {
			leads_to_tech = Support_Weapons_1918
			research_cost_coeff = 1
		}
		enable_equipments = {
			Heavy_Weapons_equipment_1916
		}
		
		research_cost = 1.5
		start_year = 1916
		folder = {
			name = infantry_folder
			position = { x = @Heavy_Weapons_line y = @1916 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			artillery
			training
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1937.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1938.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1939.1.1"
			}
		}
	}
	
	# 1936 Support Weapons	
	Heavy_Weapons_1936 = {
		
		path = {
			leads_to_tech = Heavy_Weapons_1942
			research_cost_coeff = 1
		}
		path = {
			leads_to_tech = Support_Weapons_1935
			research_cost_coeff = 1
		}
		path = {
			leads_to_tech = MANPATS_MANPADS_1936
			research_cost_coeff = 1
		}
		
		enable_equipments = {
			Heavy_Weapons_equipment_1936
		}		
		
		research_cost = 1.5
		start_year = 1936
		folder = {
			name = infantry_folder
			position = { x = @Heavy_Weapons_line y = @1935 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			artillery
			training
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1938.1.6"
			}
			
			modifier = {
				factor = 2
				date > "1939.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1940.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1941.1.1"
			}
		}
	}
	# 1942 Support Weapons	
	Heavy_Weapons_1942 = {
		
		path = {
			leads_to_tech = Heavy_Weapons_1945
			research_cost_coeff = 1
		}
		path = {
			leads_to_tech = Support_Weapons_1942
			research_cost_coeff = 1
		}	
		path = {
			leads_to_tech = MANPATS_MANPADS_1942
			research_cost_coeff = 1
		}
		
		enable_equipments = {
			Heavy_Weapons_equipment_1942
		}		
		
		research_cost = 1.5
		start_year = 1942
		folder = {
			name = infantry_folder
			position = { x = @Heavy_Weapons_line y = @1942 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			artillery
			training
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1940.1.6"
			}
			
			modifier = {
				factor = 2
				date > "1941.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1942.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1943.1.1"
			}
		}
	}
	# 1945 Support Weapons	
	Heavy_Weapons_1945 = {
		
		path = {
			leads_to_tech = Heavy_Weapons_1952
			research_cost_coeff = 1
		}
		path = {
			leads_to_tech = Support_Weapons_1945
			research_cost_coeff = 1
		}
		path = {
			leads_to_tech = MANPATS_MANPADS_1944
			research_cost_coeff = 1
		}
		enable_equipments = {
			Heavy_Weapons_equipment_1945
		}		
		
		research_cost = 1.5
		start_year = 1945
		folder = {
			name = infantry_folder
			position = { x = @Heavy_Weapons_line y = @1945 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			artillery
			training
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1940.1.6"
			}
			
			modifier = {
				factor = 2
				date > "1941.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1942.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1943.1.1"
			}
		}
	}
	# 1948 Support Weapons
	Heavy_Weapons_1952 = {
		
		path = {
			leads_to_tech = Heavy_Weapons_1962
			research_cost_coeff = 1
		}		
		path = {
			leads_to_tech = MANPATS_MANPADS_1952
			research_cost_coeff = 1
		}
		path = {
			leads_to_tech = Support_Weapons_1952
			research_cost_coeff = 1
		}
		enable_equipments = {
			Heavy_Weapons_equipment_1952
		}		
		
		research_cost = 1.5
		start_year = 1948
		folder = {
			name = infantry_folder
			position = { x = @Heavy_Weapons_line y = @1952 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			artillery
			training
		}
		
		ai_will_do = {
			factor = 1
		}
	}
	
	# 1962 Support Weapons
	Heavy_Weapons_1962 = {
		
		enable_equipments = {
			Heavy_Weapons_equipment_1962
		}
		
		path = {
			leads_to_tech = Heavy_Weapons_1980
			research_cost_coeff = 1
		}
		path = {
			leads_to_tech = MANPATS_MANPADS_1962
			research_cost_coeff = 1
		}
		path = {
			leads_to_tech = Support_Weapons_1962
			research_cost_coeff = 1
		}		
		research_cost = 1.5
		start_year = 1962
		folder = {
			name = infantry_folder
			position = { x = @Heavy_Weapons_line y = @1962 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			artillery
			training
		}
		
		ai_will_do = {
			factor = 1
		}
	}	
	# 1980 Support Weapons
	Heavy_Weapons_1980 = {
		
		enable_equipments = {
			Heavy_Weapons_equipment_1980
		}
		
		path = {
			leads_to_tech = Heavy_Weapons_1995
			research_cost_coeff = 1
		}
		path = {
			leads_to_tech = MANPATS_MANPADS_1980
			research_cost_coeff = 1
		}
		path = {
			leads_to_tech = Support_Weapons_1980
			research_cost_coeff = 1
		}		
		research_cost = 1.5
		start_year = 1980
		folder = {
			name = infantry_folder
			position = { x = @Heavy_Weapons_line y = @1980 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			artillery
			training
		}
		
		ai_will_do = {
			factor = 1
		}
	}
	# 1995 Support Weapons
	Heavy_Weapons_1995 = {
		
		enable_equipments = {
			Heavy_Weapons_equipment_1995
		}
		path = {
			leads_to_tech = MANPATS_MANPADS_1995
			research_cost_coeff = 1
		}
		path = {
			leads_to_tech = Support_Weapons_1995
			research_cost_coeff = 1
		}		
		research_cost = 1.5
		start_year = 1995
		folder = {
			name = infantry_folder
			position = { x = @Heavy_Weapons_line y = @1995 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			artillery
			training
		}
		
		ai_will_do = {
			factor = 1
		}
	}
	#################################################################
	## AT Infantry Technologies
	##################################################################		
	# 1936 AT Rifles
	MANPATS_MANPADS_1936 = {

		path = {
			leads_to_tech = MANPATS_MANPADS_1942
			research_cost_coeff = 1
		}
		on_research_complete = {
			custom_effect_tooltip = MANPATS_MANPADS_1936_tt
			custom_effect_tooltip = MANPATS_MANPADS_1936_tt2
			hidden_effect = {
				add_ideas = {
					MANPATS_MANPADS_1936
				}
			}
		}
		research_cost = 1.5
		start_year = 1936
		folder = {
			name = infantry_folder
			position = { x = @AT_Weapons_line y = @1935 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			training
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1942.1.6"
			}
			
			modifier = {
				factor = 2
				date > "1943.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1944.1.1"
			}
		}
	}
	# 1942 AT	
	MANPATS_MANPADS_1942 = {		
		
		path = {
			leads_to_tech = MANPATS_MANPADS_1944
			research_cost_coeff = 1
		}	
		on_research_complete = {
			custom_effect_tooltip = MANPATS_MANPADS_1942_tt
			custom_effect_tooltip = MANPATS_MANPADS_1942_tt2
			hidden_effect = {
				swap_ideas = {
					remove_idea = MANPATS_MANPADS_1936
					add_idea = MANPATS_MANPADS_1942
				}				
			}
		}		
		research_cost = 1.5
		start_year = 1942
		folder = {
			name = infantry_folder
			position = { x = @AT_Weapons_line y = @1942 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			training
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1943.1.6"
			}
			
			modifier = {
				factor = 2
				date > "1944.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1945.1.1"
			}
		}
	}
	
	# 1944 AT	
	MANPATS_MANPADS_1944 = {
		
		path = {
			leads_to_tech = MANPATS_MANPADS_1952
			research_cost_coeff = 1
		}	
		on_research_complete = {
			custom_effect_tooltip = MANPATS_MANPADS_1944_tt
			custom_effect_tooltip = MANPATS_MANPADS_1944_tt2
			hidden_effect = {
				swap_ideas = {
					remove_idea = MANPATS_MANPADS_1942
					add_idea = MANPATS_MANPADS_1944
				}				
			}
		}		
		research_cost = 1.5
		start_year = 1944
		folder = {
			name = infantry_folder
			position = { x = @AT_Weapons_line y = @1945 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			training
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1943.1.6"
			}
			
			modifier = {
				factor = 2
				date > "1944.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1945.1.1"
			}
		}
	}	
	
	# 1952 AT	
	MANPATS_MANPADS_1952 = {

		
		path = {
			leads_to_tech = MANPATS_MANPADS_1962
			research_cost_coeff = 1
		}	
		on_research_complete = {
			custom_effect_tooltip = MANPATS_MANPADS_1952_tt
			custom_effect_tooltip = MANPATS_MANPADS_1952_tt2
			hidden_effect = {
				swap_ideas = {
					remove_idea = MANPATS_MANPADS_1944
					add_idea = MANPATS_MANPADS_1952
				}				
			}
		}		
		research_cost = 1.5
		start_year = 1952
		folder = {
			name = infantry_folder
			position = { x = @AT_Weapons_line y = @1952 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			training
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1943.1.6"
			}
			
			modifier = {
				factor = 2
				date > "1944.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1945.1.1"
			}
		}
	}
	
	# 1962 AT	
	MANPATS_MANPADS_1962 = {

		path = {
			leads_to_tech = MANPATS_MANPADS_1980
			research_cost_coeff = 1
		}
		on_research_complete = {
			custom_effect_tooltip = MANPATS_MANPADS_1962_tt
			custom_effect_tooltip = MANPATS_MANPADS_1962_tt2
			hidden_effect = {
				swap_ideas = {
					remove_idea = MANPATS_MANPADS_1952
					add_idea = MANPATS_MANPADS_1962
				}				
			}
		}			
		research_cost = 1.5
		start_year = 1962
		folder = {
			name = infantry_folder
			position = { x = @AT_Weapons_line y = @1962 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			training
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1943.1.6"
			}
			
			modifier = {
				factor = 2
				date > "1944.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1945.1.1"
			}
		}
	}
	# 1980 AT	
	MANPATS_MANPADS_1980 = {

		path = {
			leads_to_tech = MANPATS_MANPADS_1995
			research_cost_coeff = 1
		}
		on_research_complete = {
			custom_effect_tooltip = MANPATS_MANPADS_1980_tt
			custom_effect_tooltip = MANPATS_MANPADS_1980_tt2
			hidden_effect = {
				swap_ideas = {
					remove_idea = MANPATS_MANPADS_1962
					add_idea = MANPATS_MANPADS_1980
				}				
			}
		}

		research_cost = 1.5
		start_year = 1980
		folder = {
			name = infantry_folder
			position = { x = @AT_Weapons_line y = @1980 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			training
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1943.1.6"
			}
			
			modifier = {
				factor = 2
				date > "1944.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1945.1.1"
			}
		}
	}
	# 1995 AT	
	MANPATS_MANPADS_1995 = {

		path = {
			leads_to_tech = MANPATS_MANPADS_1995
			research_cost_coeff = 1
		}
		on_research_complete = {
			custom_effect_tooltip = MANPATS_MANPADS_1995_tt
			custom_effect_tooltip = MANPATS_MANPADS_1995_tt2
			hidden_effect = {
				swap_ideas = {
					remove_idea = MANPATS_MANPADS_1980
					add_idea = MANPATS_MANPADS_1995
				}				
			}
		}				
		research_cost = 1.5
		start_year = 1995
		folder = {
			name = infantry_folder
			position = { x = @AT_Weapons_line y = @1995 }
		}
		
		categories = {
			infantry_weapons
			# Specialities
			general_equipment
			munitions
			training
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 2
				date > "1943.1.6"
			}
			
			modifier = {
				factor = 2
				date > "1944.1.1"
			}
			
			modifier = {
				factor = 2
				date > "1945.1.1"
			}
		}
	}
	
	#################################################################
	## Night Vision
	##################################################################	
	night_vision = {
		
		land_night_attack = 0.1
		
		path = {
			leads_to_tech = night_vision2
			research_cost_coeff = 1
		}
		
		research_cost = 2.5
		start_year = 1943
		folder = {
			name = infantry_folder
			position = { x = @Night_Vision_line y = @1942 }
		}
		
		categories = {
			night_vision
			# Specialities
			infantry_focus
			electronics
			training
			small_unit_tactics
			industrial_engineering
		}
		
		ai_will_do = {
			factor = 4
			
			modifier = {
				factor = 2
				date > "1942.1.9"
			}
			
			modifier = {
				factor = 2
				date > "1943.1.1"
			}
			
			modifier = {
				factor = 5
				date > "1943.1.6"
			}
			
			modifier = {
				factor = 5
				date > "1946.1.1"
			}
		}
	}
	
	night_vision2 = {
		
		land_night_attack = 0.15	
		
		research_cost = 2
		start_year = 1946
		folder = {
			name = infantry_folder
			position = { x = @Night_Vision_line y = @1945 }
		}
		
		categories = {
			night_vision
			# Specialities
			infantry_focus
			electronics
			training
			small_unit_tactics
			industrial_engineering
		}
		
		ai_will_do = {
			factor = 4
			
			modifier = {
				factor = 2
				date > "1945.1.9"
			}
			
			modifier = {
				factor = 2
				date > "1946.1.1"
			}
			
			modifier = {
				factor = 5
				date > "1946.1.6"
			}
		}
	}

}
