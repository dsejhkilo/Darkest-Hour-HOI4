technologies = {
	@1918 = 0
	@1936 = 2
	@1939 = 4
	@1942 = 6
	@1945 = 8
	@1950 = 10
	@1955 = 12
	@1960 = 14
	@1965 = 16

	@Mountaineer_line = 0
	@Marine_line = 2
	@Airborne_line = 4
	@left_elite_line = 6
	@middle_elite_line = 7
	@right_elite_line = 8

#################################################################
## Special Forces
##################################################################		
	paratroopers = {

		enable_subunits = {
			paratrooper
		}
		
		path = {
			leads_to_tech = paratroopers2
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1936
		folder = {
			name = spec_ops_folder
			position = { x = @Airborne_line y = @1936 }
		}
		
		on_research_complete = {
			limit = {
				NOT = {
					has_template_containing_unit = paratrooper
				}
			}
			hidden_effect = {
				load_oob = "unlock_paratroopers"
			}
			custom_effect_tooltip = UNLOCK_DIVISION_TEMPLATE
		}
		
		categories = {
			para_tech
		}
		
		ai_will_do = {
			factor = 0.5
			
			modifier = {
				factor = 2
				tag = USA
			}
			
			modifier = {
				factor = 2
				tag = GER
			}
			
			modifier = {
				factor = 2
				tag = ENG
			}
		}
	}
	
	paratroopers2 = {

		paratrooper = {
			max_organisation = 5
			soft_attack = 0.05
		}
		
		path = {
			leads_to_tech = paratroopers3
			research_cost_coeff = 1
		}

		research_cost = 1.5
		start_year = 1939
		folder = {
			name = spec_ops_folder
			position = { x = @Airborne_line y = @1939 }
		}
		
		categories = {
			para_tech
		}
		
		ai_will_do = {
			factor = 0.5
			
			modifier = {
				factor = 2
				tag = USA
			}
			
			modifier = {
				factor = 2
				tag = GER
			}
			
			modifier = {
				factor = 2
				tag = ENG
			}
		}
	}

	paratroopers3 = {

		paratrooper = {
			max_organisation = 5
		}
		

		research_cost = 2.0
		start_year = 1943
		folder = {
			name = spec_ops_folder
			position = { x = @Airborne_line y = @1942 }
		}
		
		categories = {
			para_tech
		}
		
		ai_will_do = {
			factor = 0.5
			
			modifier = {
				factor = 2
				tag = USA
			}
			
			modifier = {
				factor = 2
				tag = GER
			}
			
			modifier = {
				factor = 2
				tag = ENG
			}
		}
	}
	
	marines = {

		enable_subunits = {
			marine
		}
		
		on_research_complete = {
			limit = {
				NOT = {
					has_template_containing_unit = marine
				}
			}
			hidden_effect = {
				load_oob = "unlock_marines"
			}			
			custom_effect_tooltip = UNLOCK_DIVISION_TEMPLATE
		}

		path = {
			leads_to_tech = marines2
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1936
		folder = {
			name = spec_ops_folder
			position = { x = @Marine_line y = @1936 }
		}
		
		categories = {
			marine_tech
		}
		
		ai_will_do = {
			factor = 0.7
			
			modifier = {
				factor = 3
				tag = USA
			}
			
			modifier = {
				factor = 3
				tag = JAP
			}
		}
	}
	
	marines2 = {

		marine = {
			max_organisation = 5
			soft_attack = 0.05
		}
		path = {
			leads_to_tech = marines3
			research_cost_coeff = 1
		}
		

		research_cost = 1.5
		start_year = 1939
		folder = {
			name = spec_ops_folder
			position = { x = @Marine_line y = @1939 }
		}
		
		categories = {
			marine_tech
		}
		
		ai_will_do = {
			factor = 0.7
			
			modifier = {
				factor = 3
				tag = USA
			}
			
			modifier = {
				factor = 3
				tag = JAP
			}
		}
	}
	
	marines3 = {

		marine = {
			max_organisation = 5
		}

		research_cost = 2.0
		start_year = 1943
		folder = {
			name = spec_ops_folder
			position = { x = @Marine_line y = @1942 }
		}
		
		categories = {
			marine_tech
		}
		
		ai_will_do = {
			factor = 0.7
			
			modifier = {
				factor = 3
				tag = USA
			}
			
			modifier = {
				factor = 3
				tag = JAP
			}
		}
	}	
	
	tech_mountaineers = {

		enable_subunits = {
			mountaineers
		}

		on_research_complete = {
			limit = {
				NOT = {
					has_template_containing_unit = mountaineers
				}
			}
			hidden_effect = {
				load_oob = "unlock_mountineers"
			}
			custom_effect_tooltip = UNLOCK_DIVISION_TEMPLATE
		}
		
		path = {
			leads_to_tech = tech_mountaineers2
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1936
		folder = {
			name = spec_ops_folder
			position = { x = @Mountaineer_line y = @1936 }
		}
		
		categories = {
			infantry_tech
			mountaineers_tech
		}
		
		ai_will_do = {
			factor = 0.7
		}
	}
	
	tech_mountaineers2 = {

		mountaineers = {
			max_organisation = 5
			soft_attack = 0.05
		}
		path = {
			leads_to_tech = tech_mountaineers3
			research_cost_coeff = 1
		}


		research_cost = 1.5
		start_year = 1939
		folder = {
			name = spec_ops_folder
			position = { x = @Mountaineer_line y = @1939 }
		}
		
		categories = {
			infantry_tech
			mountaineers_tech
		}
		
		ai_will_do = {
			factor = 0.7
		}
	}
	
	tech_mountaineers3 = {

		mountaineers = {
			max_organisation = 5
		}

		research_cost = 2.0
		start_year = 1943
		folder = {
			name = spec_ops_folder
			position = { x = @Mountaineer_line y = @1942 }
		}
		
		categories = {
			infantry_tech
			mountaineers_tech
		}
		
		ai_will_do = {
			factor = 0.7
		}
	}
#################################################################
## Spec OpsTechnologies
##################################################################	
	tech_special_forces = {
		category_special_forces = {
			acclimatization_hot_climate_gain_factor = 0.05
			acclimatization_cold_climate_gain_factor = 0.05
		}	

		path = {
			leads_to_tech = advanced_special_forces
			research_cost_coeff = 1
		}

		path = {
			leads_to_tech = improved_special_forces
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1938
		folder = {
			name = spec_ops_folder
			position = { x = @middle_elite_line y = @1939 }
		}
		
		categories = {
			mountaineers_tech
			marine_tech
			para_tech
		}
		
		ai_will_do = {
			factor = 0.7
		}
	}


	advanced_special_forces = {
		xor={
			improved_special_forces
		}

		category_special_forces = {
			max_organisation = 5
			defense = 0.05
		}

		special_forces_training_time_factor = 0.1
		
		path = {
			leads_to_tech = extreme_environment_training
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1940
		folder = {
			name = spec_ops_folder
			position = { x = @left_elite_line y = @1942 }
		}
		
		categories = {
			mountaineers_tech
			marine_tech
			para_tech
		}
		
		ai_will_do = {
			factor = 0.7
		}
	}

	improved_special_forces = {
		xor={
			advanced_special_forces
		}

		special_forces_cap = 0.05
		special_forces_training_time_factor = -0.1
		
		path = {
			leads_to_tech = survival_training
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1940
		folder = {
			name = spec_ops_folder
			position = { x = @right_elite_line y = @1942 }
		}
		
		categories = {
			mountaineers_tech
			marine_tech
			para_tech
		}
		
		ai_will_do = {
			factor = 0.7
		}
	}


	extreme_environment_training = {

		category_special_forces = {
			acclimatization_hot_climate_gain_factor = 0.50
			acclimatization_cold_climate_gain_factor = 0.50
		}
		
		special_forces_no_supply_grace = 48
		special_forces_training_time_factor = 0.1
		
		path = {
			leads_to_tech = elite_forces
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1942
		folder = {
			name = spec_ops_folder
			position = { x = @left_elite_line y = @1945 }
		}
		
		categories = {
			mountaineers_tech
			marine_tech
			para_tech
		}
		
		ai_will_do = {
			factor = 0.7
		}
	}


	survival_training = {

		category_special_forces = {
			acclimatization_hot_climate_gain_factor = 0.20
			acclimatization_cold_climate_gain_factor = 0.20
		}
		
		special_forces_out_of_supply_factor = -0.1
		
		path = {
			leads_to_tech = elite_forces
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1942
		folder = {
			name = spec_ops_folder
			position = { x = @right_elite_line y = @1945 }
		}
		
		categories = {
			mountaineers_tech
			marine_tech
			para_tech
		}
		
		ai_will_do = {
			factor = 0.7
		}
	}


	elite_forces = {
		category_special_forces = {
			acclimatization_hot_climate_gain_factor = 0.20
			acclimatization_cold_climate_gain_factor = 0.20
			max_organisation = 5
			soft_attack = 0.05
		}
		
		special_forces_no_supply_grace = 48

		research_cost = 2
		start_year = 1944
		folder = {
			name = spec_ops_folder
			position = { x = @middle_elite_line y = @1950 }
		}
		
		categories = {
			mountaineers_tech
			marine_tech
			para_tech
		}
		
		ai_will_do = {
			factor = 0.7
		}
	}
}