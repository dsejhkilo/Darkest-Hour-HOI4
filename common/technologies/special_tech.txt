technologies = {

#########################################################################
#  Special Technologies
#########################################################################
#### Jungle Warfare
	jungle_warfare = {

		research_cost = 1

		allow = {
			always = no
		}

		marine = {
			jungle = { attack = 0.05 movement = 0.1 }
		}
		mountaineers = {
			jungle = { attack = 0.05 movement = 0.1 }
		}
		paratrooper = {
			jungle = { attack = 0.05 movement = 0.1 }
		}
	}
	### Japanese Focus Tree Bicycles
	bicycle_infantry = {
		
		allow = {
			always = no
		}
		
		research_cost = 1
		start_year = 1936
		
		enable_subunits = {
			bicycle_battalion
		}		
	}
#### Hungarian Light Infantry
	HUN_light_infantry_tech = {

		research_cost = 1

		allow = {
			always = no
		}

		infantry = {
			forest = { attack = 0.02 movement = 0.05 }
			urban = { attack = 0.05 movement = 0.05 }
		}
	}
	
#### Waffen SS
	GER_SS_VT = {

		allow = {
			always = no
		}

		enable_subunits = {
			SS_infantry
			SS_cavalry
			SS_motorized
		}

		SS_infantry = { max_organisation = -0.10 max_strength = -0.05 suppression = -0.05 }
		SS_cavalry = { max_organisation = -0.10 max_strength = -0.05 suppression = -0.05 }
		SS_motorized = { max_organisation = -0.10 max_strength = -0.05 suppression = -0.05 }

		allow = {
			always = no
		}

	}
#### Waffen SS
	GER_Waffen_SS = {

		allow = {
			always = no
		}

		SS_infantry = { max_organisation = 0.10 max_strength = 0.05 suppression = 0.05 }
		SS_cavalry = { max_organisation = 0.10 max_strength = 0.05 suppression = 0.05 }
		SS_motorized = { max_organisation = 0.10 max_strength = 0.05 suppression = 0.05 }

		allow = {
			always = no
		}

	}
#### Panzertruppen
	GER_SS_Panzertruppen = {

		allow = {
			always = no
		}

		enable_subunits = {
			SS_mechanized
			SS_armor
			SS_heavy_armor
		}

		SS_mechanized = { max_organisation = 0.10 max_strength = 0.05 suppression = 0.05 }
		SS_armor= { max_organisation = 0.10 max_strength = 0.05 suppression = 0.05 }
		SS_heavy_armor = { max_organisation = 0.10 max_strength = 0.05 suppression = 0.05 }

		allow = {
			always = no
		}

	}
#### Spezialkrafte
	GER_SS_Spezialkrafte = {

		allow = {
			always = no
		}

		enable_subunits = {
			SS_paratrooper
			SS_marines
		}

		SS_paratrooper = { max_organisation = 0.10 max_strength = 0.05 suppression = 0.05 }
		SS_marines= { max_organisation = 0.10 max_strength = 0.05 suppression = 0.05 }

		allow = {
			always = no
		}

	}
### Japanese Focus Tree Bicycles
	bicycle_infantry = {
		
		allow = {
			always = no
		}

		research_cost = 1
		start_year = 1936

		enable_subunits = {
			bicycle_battalion
		}		
	}
### Finnish Ski Infantry
	FIN_ski_infantry = {
		allow = {
			always = no
		}
		
		research_cost = 1
		start_year = 1933
		
		category_army = { acclimatization_cold_climate_gain_factor = 0.1 }
		modifier = { winter_attrition_factor = -0.05 }
		
	}
### Finnish Improvised AT
	FIN_improvised_at = {
		allow = {
			always = no
		}
		
		category_all_infantry = {
			hard_attack = 0.25
			ap_attack = 1
		}

		research_cost = 1.0
		start_year = 1933
		
	}
### Finnish Concentrated Fire
	FIN_concentrated_fire = {
		allow = {
			always = no
		}
		
		category_line_artillery = {
			soft_attack = 0.10
		}
		
		research_cost = 1.0
		start_year = 1933
		
	}
################################################################################################
}