####################################################
## Darkest Hour Civil War AI Strategies (Send Volunteers/ Lend Lease)
#####################################################

# air_factory_balance
# alliance
# antagonize
# area_priority
# befriend
# build_army
# build_building
# build_ship
# conquer
# contain
# equipment_production_factor
# equipment_stockpile
# garrison
# ignore
# influence
# invade
# pp_spend_priority
# protect
# research_tech
# role_ratio
# send_volunteers_desire
# support
# template_prio
# template_xp_reserve 
# unit_ratio

Spanish_Civil_War_No_Intervention = {
	enable = {
		SPR = { has_war_with = SPA }
		NOT = { 
			has_country_flag = SCW_SPA_Full_support
			has_country_flag = SCW_SPA_Limited_support
			has_country_flag = SCW_SPR_Full_support
			has_country_flag = SCW_SPR_Limited_support
		}
	}
	abort = {
		OR = {
			has_country_flag = SCW_SPA_Full_support
			has_country_flag = SCW_SPA_Limited_support
			has_country_flag = SCW_SPR_Full_support
			has_country_flag = SCW_SPR_Limited_support	
		}
	}
	ai_strategy = {
		type = send_volunteers_desire
		id = "SPA"
		value = -1000
	}
	ai_strategy = {
		type = support
		id = "SPA"
		value = -1000
	}
	ai_strategy = {
		type = send_volunteers_desire
		id = "SPR"
		value = -1000
	}
	ai_strategy = {
		type = support
		id = "SPR"
		value = -1000
	}	
}
# Spanish Civil War : Support the Nationalist s
Spanish_Civil_War_Full_Support_SPA = {
	enable = {
		SPR = { has_war_with = SPA }
		has_war = no
		is_subject = no
		has_country_flag = SCW_SPA_Full_support
	}
	abort = {
		OR = {
			has_war = yes
			is_subject = yes
			NOT = {SPR = { has_war_with = SPA }}
		}		
	}
	
	ai_strategy = {
		type = send_volunteers_desire
		id = "SPA"
		value = 1000
	}
	ai_strategy = {
		type = support
		id = "SPA"
		value = 1000
	}
	ai_strategy = {
		type = send_volunteers_desire
		id = "SPR"
		value = -1000
	}
	ai_strategy = {
		type = support
		id = "SPR"
		value = -1000
	}		
}

Spanish_Civil_War_Limited_Support_SPA = {
	enable = {
		SPR = { has_war_with = SPA }
		has_war = no
		is_subject = no
		has_country_flag = SCW_SPA_Limited_support
	}
	abort = {
		OR = {
			has_war = yes
			is_subject = yes
			NOT = {SPR = { has_war_with = SPA }}
		}		
	}
	
	ai_strategy = {
		type = send_volunteers_desire
		id = "SPA"
		value = -1000
	}
	ai_strategy = {
		type = support
		id = "SPA"
		value = 1000
	}
	ai_strategy = {
		type = send_volunteers_desire
		id = "SPR"
		value = -1000
	}
	ai_strategy = {
		type = support
		id = "SPR"
		value = -1000
	}		
}

Spanish_Civil_War_No_Support_SPA = {
	enable = {
		SPR = { has_war_with = SPA }
		has_war = no
		is_subject = no
		has_country_flag = SCW_SPA_Limited_support
	}
	abort = {
		OR = {
			has_war = yes
			is_subject = yes
			NOT = {SPR = { has_war_with = SPA }}
		}		
	}
	
	ai_strategy = {
		type = send_volunteers_desire
		id = "SPA"
		value = -1000
	}
	ai_strategy = {
		type = support
		id = "SPA"
		value = -1000
	}
	ai_strategy = {
		type = send_volunteers_desire
		id = "SPR"
		value = -1000
	}
	ai_strategy = {
		type = support
		id = "SPR"
		value = -1000
	}		
}

# Spanish Civil War : Support the Republicans
Spanish_Civil_War_Full_Support_SPR = {
	enable = {
		SPR = { has_war_with = SPA }
		has_war = no
		is_subject = no
		has_country_flag = SCW_SPR_Full_support
		NOT = { has_government = fascist }
	}
	abort = {
		OR = {
			has_war = yes
			is_subject = yes
			NOT = {SPR = { has_war_with = SPA }}
		}		
	}
	
	ai_strategy = {
		type = send_volunteers_desire
		id = "SPR"
		value = 1000
	}
	ai_strategy = {
		type = support
		id = "SPR"
		value = 1000
	}
	ai_strategy = {
		type = send_volunteers_desire
		id = "SPA"
		value = -1000
	}
	ai_strategy = {
		type = support
		id = "SPA"
		value = -1000
	}		
}

Spanish_Civil_War_Limited_Support_SPR = {
	enable = {
		SPR = { has_war_with = SPA }
		has_war = no
		is_subject = no
		has_country_flag = SCW_SPR_Limited_support
		NOT = { has_government = fascist }
	}
	abort = {
		OR = {
			has_war = yes
			is_subject = yes
			NOT = {SPR = { has_war_with = SPA }}
		}		
	}
	
	ai_strategy = {
		type = send_volunteers_desire
		id = "SPR"
		value = -1000
	}
	ai_strategy = {
		type = support
		id = "SPR"
		value = 1000
	}
	ai_strategy = {
		type = send_volunteers_desire
		id = "SPA"
		value = -1000
	}
	ai_strategy = {
		type = support
		id = "SPA"
		value = -1000
	}		
}

Spanish_Civil_War_No_Support_SPR = {
	enable = {
		SPR = { has_war_with = SPA }
		has_war = no
		is_subject = no
		has_country_flag = SCW_SPR_No_support
		NOT = { has_government = fascist }
	}
	abort = {
		OR = {
			has_war = yes
			is_subject = yes
			NOT = {SPR = { has_war_with = SPA }}
		}		
	}
	
	ai_strategy = {
		type = send_volunteers_desire
		id = "SPR"
		value = -1000
	}
	ai_strategy = {
		type = support
		id = "SPR"
		value = -1000
	}
	ai_strategy = {
		type = send_volunteers_desire
		id = "SPA"
		value = -1000
	}
	ai_strategy = {
		type = support
		id = "SPA"
		value = -1000
	}	
	
}