on_actions = {
    on_startup = {
        effect = {
            ENG = {
                set_variable = {
                    var = eng_power_in_iraq
                    value = 22
                }
            }
			IRQ = {
                set_variable = {
                    var = irq_power_in_iraq
                    value = 28
                }
            }
        }
    }
}