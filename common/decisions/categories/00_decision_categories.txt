#########################################################################
#  Debug Mode
#########################################################################
Debug_Decisions = {
	icon = generic_propaganda
	priority = 1000
	visible = {
		is_ai = no
		# always = no
	}
}

#########################################################################
#  Political Decision Categories
#########################################################################
### Internal Factions Tab
parliament_decision_category = {
	icon = usa_congress
	priority = 101
	visible = {
		country_has_parliament = yes
	}
	scripted_gui = parliament_decision_category_scripted_gui
	visible_when_empty = yes
}
factions_decision_category = {
	icon = generic_crisis
	priority = 100
	visible = {
		country_has_factions = yes
	}
	scripted_gui = factions_decision_category_scripted_gui
	visible_when_empty = yes
}
SOV_The_Kirov_Question_category = {
	icon = generic_communism
	priority = 50

	allowed = {
		original_tag = SOV
	}
	visible = {
		always = yes
	}
}
SOV_The_Great_Purge_category = {
	icon = generic_communism
	priority = 50

	allowed = {
		original_tag = SOV
	}
	visible = {
		always = yes
	}
	visible_when_empty = yes
}
GER_Positive_Christianity_category = {
	icon = eng_move_to_secure_the_dominions
	priority = 50

	allowed = {
		original_tag = GER
	}
	visible = {
		has_completed_focus = GER_Adopt_Positive_Christianity
	}
}
political_actions = {
	icon = generic_political_actions
	allowed = {
		always = yes
	}
}

propaganda_efforts = {
	icon = generic_propaganda
	visible = {
	}
}

crisis = {
	icon = generic_crisis
	# priority = 100
	visible = {
		has_country_flag = crisis_in_progress
	}
}

formable_nations_category = {
	icon = generic_formable_nations
	picture = GFX_decision_cat_generic_hre
	allowed = {
		always = yes
	}
}
BUL_Consolidate_Our_Power = {
	icon = generic_formable_nations
	picture = GFX_decision_cat_generic_hre
	allowed = {
		original_tag = BUL
	}
}
#########################################################################
#  Ideological Decision Categories
#########################################################################
fascist_on_the_rise = {
	icon = generic_fascism
	allowed = {
		always = yes
	}
}

authoritarian_on_the_rise = {
	icon = generic_fascism
	allowed = {
		always = yes
	}
}

democratic_on_the_rise = {
	icon = generic_democracy
	allowed = {
		always = yes
	}
}

communist_on_the_rise = {
	icon = generic_communism
	allowed = {
		always = yes
	}
}

socialist_on_the_rise = {
	icon = generic_communism
	allowed = {
		always = yes
	}
}

#########################################################################
#  Economic Decision Categories
#########################################################################
economy_decisions = {
	icon = generic_economy
}

dh_inflation_category = {
	icon = decision_category_ger_mefo_bills
	visible = {
		always = yes
	}
}

dh_unemployment_category = {
	icon = generic_economy
	visible = {
		set_temp_variable = { unemployment_value = 0.001 }
		has_at_least_unemployment_value = yes
	}
	visible_when_empty = yes
}

GER_Reichswerke_category = {
	icon = generic_industry

	allowed = {
		original_tag = GER
	}
	visible = {
		has_completed_focus = GER_Reichswerke_Hermann_Goering
	}
}
GER_Reichsautobahn_category = {
	icon = generic_industry

	on_map_area = {
		state = 64
		name = GER_Reichsautobahn_category_zoom
		zoom = 250
	}

	allowed = {
		original_tag = GER
	}
}
GER_MEFO_Bills_category = {
	icon = ger_mefo_bills

	allowed = {
		original_tag = GER
	}
}
ENG_Economic_Decisions_Category = {
	icon = placeholder

	picture = placeholder

	visible = {
	    has_completed_focus = ENG_Dealing_With_Unemployment
	}

	allowed = {
		original_tag = ENG
	}

	on_map_area = {
		state = 126
		name = ENG_Recovery_of_the_Depression
		zoom = 126
	}

	visible_when_empty = yes
}
ENG_Baldwin_Rearmament_Category = {
	icon = placeholder

	picture = placeholder

	visible = {
	    has_completed_focus = ENG_Baldwins_Rearmament
	}

	allowed = {
		original_tag = ENG
	}
}
ENG_Churchill_Rearmament_Category = {
	icon = placeholder

	picture = placeholder

	visible = {
	    has_completed_focus = ENG_Churchills_Rearmament
	}

	allowed = {
		original_tag = ENG
	}
}
fortification_projects = {
	icon = border_war
	visible = {
	}
}
GER_Westwall_category = {
	icon = border_war
	visible = {

	}
}
GER_Atlantikwall_category = {
	icon = border_war
	visible = {

	}
}
FRA_Economical_Reform_category = {
	icon = generic_economy
	visible = {
		has_completed_focus = FRA_Sanitize_Finances
	}
}
FRA_Reform_of_the_State_category = {
	icon = political_actions
	visible = {
		has_completed_focus = FRA_Launch_the_Reform_of_the_State
		NOT = {
			OR = {
				has_country_flag = FRA_Reform_of_the_State_Success
				has_country_flag = FRA_Reform_of_the_State_Rejected
			}
		}
	}
	visible_when_empty = yes
	scripted_gui = fra_reform_of_the_state_decision_gui
}
FRA_Negotiations_with_Socialists = {
	icon = generic_economy
	visible = {
		has_country_flag = FRA_Paul_Boncour_Completed
	}
}
FRA_Pacifism_category = {
	icon = generic_economy
	visible = {
		has_completed_focus = FRA_Position_About_Pacifism
	}
}
FRA_Nationalization_Program_category = {
	icon = generic_economy
	visible = {
		has_completed_focus = FRA_Large_Nationalizations
	}
}
FRA_Electrify_France_category = {
	icon = generic_economy
	visible = {
		has_completed_focus = FRA_Grand_Travaux_Policy
	}
}
FRA_Plan_Marquet_category = {
	icon = generic_economy
	visible = {
		has_country_flag = FRA_Plan_Marquet_flag
	}
}
ITA_Settle_the_Libyan_Coast = {
	icon = generic_political_actions
	visible = {
		has_completed_focus = ITA_Annex_la_Quarta_Sponda
	}
}
ITA_Pacify_Abyssinia_category = {
	icon = generic_political_actions
	visible = {
		OR = {
			ETH = { exists = no }
			ETH = { has_capitulated = yes }
		}
		NOT = {
			AND = {
				check_variable = { ITA_Ethiopian_Unrest_partisan = 0 }
				check_variable = { ITA_Ethiopian_Unrest_damage = 0 }
			}
		}
	}
	visible_when_empty = yes
	
}

prestigious_projects = {
	icon = generic_political_actions
}

prospect_for_resources = {
	icon = generic_prospect_for_resources
}

special_projects = {
	icon = generic_prospect_for_resources
}

#########################################################################
#  Foreign Policy Decision Categories
#########################################################################
foreign_politics = {
	icon = generic_political_actions
}
FRA_Barthou_Doctrine_category = {
	icon = generic_political_actions

	allowed = {
		original_tag = FRA
	}
}
GER_Heim_ins_Reich_category = {
	icon = GER_Heim_ins_Reich

	allowed = {
		original_tag = GER
	}

	visible = {
		has_government = fascist
		#has_completed_focus =GER_Heim_ins_Reich
	}
}
GER_Rule_over_the_Danube_category = {
	visible_when_empty = yes
	icon = eng_move_to_secure_the_dominions

	allowed = {
		original_tag = GER
	}

	visible = {
		has_government = fascist
		has_completed_focus = GER_Rule_over_the_Danube
	}
}
BUL_Foreign_Policy = {
	visible_when_empty = yes
	icon = eng_move_to_secure_the_dominions

	allowed = {
		original_tag = BUL
	}

	visible = {

	}
}
BUL_Black_Tiger_Lazy = {
	visible_when_empty = yes
	icon = eng_move_to_secure_the_dominions

	allowed = {
		original_tag = BUL
	}

	visible = {

	}
}
Balkan_German_Influence = {
	visible_when_empty = yes
	icon = GER_Heim_ins_Reich

	allowed = {
		OR = {
			tag = HUN
			tag = BUL
			tag = ROM
			tag = YUG
		}
	}

	visible = {
		GER = {
			has_completed_focus = GER_Rule_over_the_Danube
		}
	}
}
HUN_Claim_Land = {
	allowed = {
		original_tag = HUN
	}
}
HUN_Appeasing_Our_Superiors = {

	allowed = {
		original_tag = HUN
	}
}
HUN_A_Supportive_Alliance = {

	allowed = {
		original_tag = HUN
	}
}
HUN_Claim_Greater_Hungary = {

	allowed = {
		original_tag = HUN
	}
}
Occupation_Policy = {
	icon = GFX_decision_category_military_operation
}
GER_Reichskommissariats = {

	allowed = {
		original_tag = GER
	}

	visible = {
		has_government = fascist
	}
}
foreign_support = {
	icon = generic_political_actions
}
### British Dominions
ENG_invest_dominions_category = {
	icon = placeholder
	picture = placeholder
	visible = {
		has_completed_focus = ENG_Invest_in_our_Dominions
	}
	allowed = {
		original_tag = ENG
	}
}

ENG_anglo_iraqi_treaty_category = {

	icon = hol_gateway_to_europe

	allowed = {
		OR = {
			AND = {
				original_tag = ENG
				IRQ = {
					has_idea = IRQ_Anglo_Iraqi_Treaty
				}
			}
			AND = {
				original_tag = IRQ
				has_idea = IRQ_Anglo_Iraqi_Treaty
			}
		}
	}

	picture = GFX_decision_cat_picture_gateway_to_europe
	priority = {
		base = 1
	}

	custom_icon = {
		tag = ENG
		value = eng_power_in_iraq
		desc = eng_power_in_iraq_breakdown

		visible = {
			country_exists = ENG
		}
	}

	custom_icon = {
		tag = IRQ
		value = irq_power_in_iraq
		desc = irq_power_in_iraq_breakdown

		visible = {
			country_exists = IRQ
		}
	}

	on_map_area = {
		state = 291
		name = ENG_IRQ_Battle_for_Iraq
		zoom = 291
	}

	visible_when_empty = yes

	visible = {
		always = yes
	}
}

#Expansion of the Allies
ENG_expansion_of_the_alliance = {
	allowed = {
		tag = ENG
	}
	visible = {
		has_completed_focus = ENG_Expansion_of_the_Alliance
	}
}

#########################################################################
#  Military Decision Categories
#########################################################################
operations = {
	icon = GFX_decision_category_military_operation
}

war_measures = {
	icon = generic_propaganda
	visible = {
		has_war = yes
	}
}
ITA_reform_military_category = {
	icon = army_reform

	allowed = {
		original_tag = ITA
	}
}
ITA_spanish_cw_intervention_category = {
	icon = spr_the_inevitable_civil_war

	allowed = {
		original_tag = ITA
	}
}
ITA_ETH_war = {
	icon = army_reform
	visible = {
		has_country_flag = ITA_ETH_first_decisions
	}
}
### ETH
ETH_Second_Italo_Ethiopian_War = {
	icon = generic_propaganda

	picture = GFX_decision_cat_generic_mefo_bills

	visible = {
		original_tag = ETH
		has_country_flag = eth_second_italo_ethiopian_war
	}

	allowed = {
		original_tag = ETH
		has_country_flag = eth_second_italo_ethiopian_war
	}
}

ETH_The_Dervish_Accord = {
	icon = generic_propaganda

	picture = GFX_decision_cat_generic_mefo_bills

	visible = {
		original_tag = ETH
		has_country_flag = eth_dervish_accord
	}

	allowed = {
		original_tag = ETH
		has_country_flag = eth_dervish_accord
	}
}
GER_Treaty_of_Versaille = {
	icon = decision_category_generic_economy

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = GER
	}
}
GER_Waffen_SS_category = {
	icon = decision_category_generic_economy

	allowed = {
		original_tag = GER
	}
	visible = {
		has_completed_focus = GER_SS_VT
	}
	visible_when_empty = yes
}
demobilization = {
	icon = generic_crisis
	priority = 100
	visible = {
		has_war = no
	}
}

SAF_anti_colonialist_crusade = {
	allowed = {
		original_tag = SAF
	}
}

#########################################################################
#  La Resistance
#########################################################################

lar_local_recruitment = {
	allowed = { has_dlc = "La Resistance" }

	visible = { has_done_agency_upgrade = upgrade_training_centers }

	visibility_type = map_and_decisions_view

	on_map_area = {
		state = 8
		name = LAR_recruitment_europe
		zoom = 650

		target_root_trigger = {
			OR = {
				has_country_flag = europe_recruitment_unlocked
				capital_scope = { is_on_continent = europe }
			}
		}
	}

	on_map_area = {
		state = 373
		name = LAR_recruitment_north_america
		zoom = 850

		target_root_trigger = {
			OR = {
				has_country_flag = north_america_recruitment_unlocked
				capital_scope = { is_on_continent = north_america }
			}
		}
	}

	on_map_area = {
		state = 487
		name = LAR_recruitment_south_america
		zoom = 850

		target_root_trigger = {
			OR = {
				has_country_flag = south_america_recruitment_unlocked
				capital_scope = { is_on_continent = south_america }
			}
		}
	}

	on_map_area = {
		state = 295
		name = LAR_recruitment_africa
		zoom = 850

		target_root_trigger = {
			OR = {
				has_country_flag = africa_recruitment_unlocked
				capital_scope = { is_on_continent = africa }
			}
		}
	}

	on_map_area = {
		state = 597
		name = LAR_recruitment_asia
		zoom = 850

		target_root_trigger = {
			OR = {
				has_country_flag = asia_recruitment_unlocked
				capital_scope = { is_on_continent = asia }
			}
			NOT = {
				tag = RAJ
				tag = PAK
				tag = BAN
				tag = NEP
				tag = BHU
				tag = SRL
			}
		}
	}

	on_map_area = {
		state = 521
		name = LAR_recruitment_oceania
		zoom = 850

		target_root_trigger = {
			OR = {
				has_country_flag = australia_recruitment_unlocked
				capital_scope = { is_on_continent = australia }
			}
		}
	}

	on_map_area = {
		state = 439
		name = LAR_recruitment_india
		zoom = 850

		target_root_trigger = {
			OR = {
				has_country_flag = india_recruitment_unlocked
				tag = RAJ
				tag = PAK
				tag = BAN
				tag = NEP
				tag = BHU
				tag = SRL
			}
		}
	}

	on_map_area = {
		state = 656
		name = LAR_recruitment_middle_east
		zoom = 850

		target_root_trigger = {
			OR = {
				has_country_flag = middle_east_recruitment_unlocked
				capital_scope = { is_on_continent = middle_east }
			}
		}
	}
}
