################
##### USA ######
################

USA_war_measures = {

	allowed = {
		original_tag = USA
	}
}

USA_aid_britain = {

	allowed = {
		original_tag = USA
	}
}
USA_WPA_Category = {

	allowed = {
		original_tag = USA
	}
	visible = {
		has_completed_focus = USA_Improve_Nation_Infrastructure
	}
}
USA_Federal_Project_1 = {
	icon = GFX_decision_category_USA_Federal_Project_One

	allowed = {
		original_tag = USA
	}
	visible = {
		has_completed_focus = USA_Support_American_Artisans
	}
}
USA_Defence_Against_Agression_Category = {

	allowed = {
		original_tag = USA
	}
	visible = {
		has_completed_focus = USA_A_Defense_Against_Agression
	}
}
USA_Military_Rearmament_Category = {

	allowed = {
		original_tag = USA
	}
	visible = {
		has_country_flag = USA_Military_Quotas_Completed
	}
}
USA_Mobilization_Category = {

	allowed = {
		original_tag = USA
	}
	visible = {
	}
}
USA_Reciprocal_Deals_Category = {

	allowed = {
		original_tag = USA
	}
	visible = {
		has_completed_focus = USA_Road_Goes_Both_Ways
	}
}
USA_the_Good_Neighbour_Category = {

	allowed = {
		original_tag = USA
	}
	visible = {
		has_country_flag = USA_the_Good_Neighbour_Category_Unlock
	}
}
USA_the_First_100_Days_Category = {

	allowed = {
		original_tag = USA
	}
	visible = {
		has_completed_focus = USA_Houdini_in_the_House
	}
}

USA_Cash_and_Carry_Category = {

	allowed = {
		original_tag = USA
	}
	visible = {
		has_country_flag = USA_Cash_and_Carry
	}
}
USA_USACE_Projects_Category = {
	
	allowed = {
		original_tag = USA
	}
	visible = {
		has_completed_focus = USA_USACE_Projects
	}
}
USA_Pro_War_Propaganda_Category = {
	allowed = {
		original_tag = USA
	}
	visible = {
		has_completed_focus = USA_Pro_War_Propaganda
	}
}
USA_Military_Rearmament_1940_Category = {
	allowed = {
		original_tag = USA
	}
	visible = {
		has_completed_focus = USA_Rearm_The_Military
	}
}
USA_The_Office_of_the_Coordinator_of_Inter_American_Affairs_Category = {
	allowed = {
		original_tag = USA
	}
	visible = {
		has_completed_focus = USA_Office_of_Coordinator_of_Inter_American_Affairs
	}
}
USA_Wartime_Economic_Recovery_Category = {

	allowed = {
		original_tag = USA
	}
	visible = {
		has_completed_focus = USA_the_Giant_Wakes
	}
}
USA_The_War_Production_Board_Category = {

	allowed = {
		original_tag = USA
	}
	visible = {
		has_completed_focus = USA_War_Production_Board
	}
}
USA_The_Reserve_Companies_Category = {

	allowed = {
		original_tag = USA
	}
	visible = {
		has_completed_focus = USA_Reserve_Companies
	}
}
USA_Occupied_Economic_Stabilisation_Category = {

	allowed = {
		original_tag = USA
	}
	visible = {
		has_completed_focus = USA_Stabilize_the_Occupied_Economy
	}

	scripted_gui = usa_economic_stabilisation_decision_gui
}
USA_Gradual_Occupied_De_Militarization_Category = {

	allowed = {
		original_tag = USA
	}
	visible = {
		has_completed_focus = USA_De_Militarization_of_Occupied_Society
	}

	scripted_gui = usa_demilitarization_decision_gui
}
USA_Expand_the_Pacific_Defenses_Category = {
	icon = generic_military
	allowed = {
		original_tag = USA
	}
	visible = {
		has_completed_focus = USA_Expand_the_Pacific_Defenses
	}
}
USA_Landon_Private_Investments_Category = {
	
	allowed = {
		original_tag = USA
	}
	visible = {
		has_completed_focus = USA_Encourage_Private_Investment
	}
}

USA_Privatization_CCC_Category = {
	
	allowed = {
		original_tag = USA
	}
	visible = {
		has_completed_focus = USA_Make_Publc_Works_Private
	}
}

USA_Air_War_Plans_Division_Category = {
	
	allowed = {
		original_tag = USA
	}
	visible = {
		has_completed_focus = USA_Air_War_Plans_Division
	}
}

USA_OSS_Category = {
	
	allowed = {
		original_tag = USA
	}
	visible = {
		has_country_flag = USA_OSS_Decisions
	}
}
USA_War_Plans_Category = {
	
	allowed = {
		original_tag = USA
	}
	visible = {
		has_completed_focus = USA_War_Plans_Division
	}
}
USA_The_Rainbow_Plans_Category = {
	icon = generic_propaganda
	allowed = {
		original_tag = USA
	}
	visible = {
		has_completed_focus = USA_Rainbow_Plans
	}
}